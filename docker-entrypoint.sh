#!/bin/bash
set -e

php composer.phar install -o
php public/index.php generate-filters
chown :www-data . -R

exec "$@"