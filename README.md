#LajiStore-API

## Setting up development environment
 
 * You need the following software installed:
     * [virtualbox](https://www.virtualbox.org/wiki/Downloads)
     * git
     * [vagrant](https://www.vagrantup.com/downloads.html)
 * Clone this repository
 * Go to created directory
 * Add the kotka dev box to vagrant with following:
      * ```vagrant box add kotkadev <location of kotkadev.box>``` 
        e.g. ```vagrant box add kotkadev /home/user/kotkadev.box```
      *  If you don't have access to kotkadev.box then you'll need to use some of the existing vagrant boxes and
 make sure that it has all the needed components setup correctly (php>5.5, apache, etc.)
 * Issue command `vagrant up`
 * Login to the new box by issuing command `vagrant ssh`
 * Go to directory `/var/www` and issue commands 
 
```bash
   # update env
   sudo apt-get update
   sudo apt-get dist-upgrade
   
   # install package that is missing from kotka box (needed in the next step)
   sudo apt-get install apache2-utils
   
   # install dependencies and configure lajistore
   php composer.phar install
   cp config/autoload/local.php.dist config/autoload/local.php
```
 * Edit the config/autoload/local.php file and fill in the blanks
      
 * Open browser to address [192.168.33.11](http://192.168.33.11)

## LajiStore update to clasess
Lajistore keeps track that the data is in correct format. To make changes first update the class specification in triplestore and the call following address to update lajistore classes
`https://<address for lajistore>/update-filters`
Changes will be visible and usable immediately.
 
## LajiStore CLI
You can get a list of available commands by giving following command in the root directory of the lajiStore

```
php public/index.php
```

## API specification

LajiStore uses HTTP BASIC Autentication. Each system has it's own identifier and password. Each system can only access it's own records. 
When doing requests to LajiStore-API, it is not necessary to give system's identifier as a parameter; API gets it from autentication credentials.

For example: GET|POST|PUT|DELETE KE.123:secretpassword@https://host:port/lajistore

API uses following standards to communicate with client:

* [Hypermedia Application Language](http://tools.ietf.org/html/draft-kelly-json-hal-07), aka HAL, used for creating JSON payloads with hypermedia controls.
* [Problem Details for HTTP APIs](http://tools.ietf.org/html/draft-nottingham-http-problem-07), aka API Problem, used for reporting API problems.

Currently API can be found at [fmnh-ws-prod2.it.helsinki.fi](http://fmnh-ws-prod2.it.helsinki.fi/), but this will change
in the near future. More detailed api documentation can also be found by accessing the url above with a browser. 

### General status codes
* on success api will return 2xx status code
* on client error api will return 4xx status code
* on server error api will return 5xx or something else

API requires http basic authentication and will return 403 if no authentication provided and 401 if invalid authentication provided


### Create

POST /lajistore/{type}

#### For example
    * POST /lajistore/documents
    * POST /lajistore/devices
			 
#### Parameters 
    * JSON in the request body without an id.
	
* Success: HTTP 201 - The same JSON with the added id.
	
### Update

PUT /lajistore/{type}/{id}

#### For example:
    * PUT /lajistore/documents/12345

#### Parameters
    * The new JSON in the body.
	
* Success: HTTP 200 - Updated JSON as content.

### Delete

DELETE /lajistore/{type}/{id}

#### For example 
    * DELETE /lajistore/documents/12345

#### Parameters
    * none
	
* Success: HTTP 204 - No content.


### Get single

GET /lajistore/{type}/{id}

#### For example
    * GET /lajistore/documents/12345

#### Parameters
    * none
	
* Success: HTTP 200 - The JSON as content.


### Get multiple

GET /lajistore/{type}?page={n}

#### For example
    * GET /lajistore/documents?page=10

#### Parameters 	
    * page: integer,
    * page_size: integer,
    * query: query string
	
* Success: HTTP 200 - The JSONs with additional metadata to help browsing.
Actual resources can be found under ["_embedded"][{type}] key as an array of resources


### Search

GET /lajistore/{type}?page={n}&q={query}

Query mimics the syntax specified by [elastic query string query](https://www.elastic.co/guide/en/elasticsearch/reference/2.0/query-dsl-query-string-query.html#query-string-syntax)
Currently we're using only small sub set of it (grouping, ranges and negation), but we'll add mre as needed.

Remember to encode the request! 

#### For example 
* GET `/lajistore/documents?q=gatherings.publicity:public AND documentId:A647FD6D-A4AB-53C4-84E5-0933C84B4F4F`
* GET `/lajistore/documents?q=gatherings.publicity:public AND (documentId:"MY.122" OR documentId:"MY.123")`
* GET `/lajistore/documents?q=gatherings.publicity:public AND documentId:("MY.122" "MY.123")`
* GET `/lajistore/documents?q=gatherings.dateTimeBegin:[2015-09-15T11:00:57+03:00 TO 2015-09-15T11:00:57+03:00]`
* GET `/lajistore/documents?q=gatherings.dateTimeBegin:[2015-09-15T00:00:00+03:00 TO *]`
* GET `/lajistore/documents?q=deviceId:"groovy device"`

#### Parameters: 	
    * page: integer
    * page_size: integer
    * q: query string
	
* Success: HTTP 200 - The JSONs as ordered collection.
	
## Example documents

Examples can be found by browsing to the front page of lajistore