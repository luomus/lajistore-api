<?php

namespace LajiStore\Resource;


use LajiStore\Mapper\MapperInterface;
use ZF\ApiProblem\ApiProblem;
use ZF\MvcAuth\Identity\AuthenticatedIdentity;
use ZF\Rest\AbstractResourceListener;
use ZF\Rest\Exception;
use ZF\Rest\ResourceEvent;

class AbstractResource extends AbstractResourceListener
{
    protected $mapper;
    protected $identity;

    public function __construct(MapperInterface $mapper, $identity)
    {
        $this->mapper   = $mapper;
        $this->identity = $identity;
    }

    /**
     * Dispatch an incoming event to the appropriate method
     *
     * Marshals arguments from the event parameters.
     *
     * @param  ResourceEvent $event
     * @return mixed
     */
    public function dispatch(ResourceEvent $event)
    {
        $this->event = $event;
        switch ($event->getName()) {
            case 'create':
                $data = $event->getParam('data', []);
                return $this->create($data);
            case 'delete':
                $id   = $event->getParam('id', null);
                return $this->delete($id);
            case 'deleteList':
                $data = $event->getParam('data', []);
                return $this->deleteList($data);
            case 'fetch':
                $id   = $event->getParam('id', null);
                $queryParams = $event->getRequest()->getQuery() ?: [];
                return $this->fetch($id, $queryParams);
            case 'fetchAll':
                $queryParams = $event->getRequest()->getQuery() ?: [];
                return $this->fetchAll($queryParams);
            case 'patch':
                $id   = $event->getParam('id', null);
                $data = $event->getParam('data', []);
                return $this->patch($id, $data);
            case 'patchList':
                $data = $event->getParam('data', []);
                return $this->patchList($data);
            case 'replaceList':
                $data = $event->getParam('data', []);
                return $this->replaceList($data);
            case 'update':
                $id   = $event->getParam('id', null);
                $data = $event->getParam('data', []);
                return $this->update($id, $data);
            default:
                throw new Exception\RuntimeException(sprintf(
                    '%s has not been setup to handle the event "%s"',
                    __METHOD__,
                    $event->getName()
                ));
        }
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return $this->mapper->create($data);
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return $this->mapper->delete($id);
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        if (!$this->identity instanceof AuthenticatedIdentity) {
            return false;
        }
        $source = $this->identity->getName();
        if (strpos($source, '-testing') === false) {
            throw new \DomainException('Only users with testing in their name are allowed to use this', 406);
        }
        return $this->mapper->deleteAll($data);
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetch($id, $params = [])
    {
        return $this->mapper->fetch($id, $params);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        return $this->mapper->fetchAll($params);
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return $this->mapper->update($id, $data);
    }
}