<?php

namespace LajiStore\Filter;


use Zend\Filter\Exception;
use Zend\Filter\FilterInterface;

class CurrentTime implements FilterInterface
{

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        $datetime = new \DateTime();
        return $datetime->format('c');
    }
}