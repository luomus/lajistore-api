<?php

namespace LajiStore\V1\Rest\NamedPlace;

use LajiStore\Db\Sql\AbstractWhere;
use LajiStore\Paginator\Adapter\DbSearchTableGatewayWithFeature;
use LajiStore\TableGateway\Feature\FieldFeature;
use LajiStore\TableGateway\Feature\FilterFeature;
use LajiStore\V1\Mapper\AbstractSearchTableMapper;

class NamedPlaceMapper extends AbstractSearchTableMapper
{
    const TYPE = 'namedPlace';

    protected $fieldTypes = [
        'id' => AbstractWhere::TYPE_ID,
        'editors' => AbstractWhere::TYPE_ARRAY,
        'owners' => AbstractWhere::TYPE_ARRAY,
        'alternativeIDs' => AbstractWhere::TYPE_ARRAY,
        'municipality' => AbstractWhere::TYPE_ARRAY,
        'tags' => AbstractWhere::TYPE_ARRAY,
        'birdAssociationArea' => AbstractWhere::TYPE_ARRAY,
        'taxonIDs' => AbstractWhere::TYPE_ARRAY,
        'collectionID' => AbstractWhere::TYPE_TEXT,
        'isPublic' => AbstractWhere::TYPE_BOOLEAN,
        'hasCollectionID' => AbstractWhere::TYPE_BOOLEAN
    ];

    /**
     * @var array Key is the field that can be searched with and value means the following:
     * true = result is unambiguous
     * false = result is ambiguous
     */
    protected $fieldsInstructions = [
        'id' => true,
        'name' => true,
        'tags' => true,
        'municipality' => true,
        'taxonIDs' => true,
        'taxonIDs[0]' => true,
        'alternativeIDs' => true,
        'alternativeIDs[0]' => true,
        'invasiveControlOpen' => true,
        'editors' => true,
        'owners' => true,
        'birdAssociationArea' => true,
        'collectionID' => true,
        'public' => true,
        'geometry' => true,
        'reserve' => true,
        'geometry.coordinateVerbatim' => true,
        'prepopulatedDocument.gatherings[0].invasiveControlOpen' => true,
        'prepopulatedDocument.gatheringEvent.dateBegin' => true,
        'prepopulatedDocument.gatheringEvent.dateEnd' => true
    ];

    /**
     * @param array $params
     * @return NamedPlaceCollection
     * @throws \Exception
     */
    public function fetchAll($params)
    {
        if (isset($params[self::PARAM_FILTER])) {
            $this->addFeature(new FilterFeature($params[self::PARAM_FILTER]));
        }
        if (isset($params[self::PARAM_FIELDS]) && !empty($params[self::PARAM_FIELDS])) {
            $this->addFeature(new FieldFeature($params[self::PARAM_FIELDS], $this->fieldsInstructions));
        }
        return new NamedPlaceCollection(new DbSearchTableGatewayWithFeature(
            $this->table,
            $this->searchTable,
            $this->whereFromParams($params, 'OracleFlat'),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }

    public function addSearchRow($id, $data)
    {
        $isPublic = $this->pickValue($data, 'public', false);
        $isPublic = $isPublic === 'true' ||$isPublic === true ? true : false;

        $collectoinID = $this->pickValue($data, 'collectionID', null);

        $rows = [];
        foreach ($this->fieldTypes as $field => $type) {
            if ($type !== AbstractWhere::TYPE_ARRAY) {
                continue;
            }
            $value = $this->pickValue($data, $field, []);
            if (count($value) === 0) {
                continue;
            }
            $rows[$field] = $value;
        }

        $rowData = $this->cartesian($rows);

        // fill in with null if no value exists
        if (count($rowData) === 0) {
            $rowData = [['editors' => null]];
        }

        foreach ($rowData as $idx => $row) {
            $this->searchTable->insert([
                'ID' => $id,
                'EDITORS' => isset($row['editors']) ? $row['editors'] : null,
                'OWNERS' => isset($row['owners']) ? $row['owners'] : null,
                'ALTERNATIVEIDS' => isset($row['alternativeIDs']) ? $row['alternativeIDs'] : null,
                'TAGS' => isset($row['tags']) ? $row['tags'] : null,
                'MUNICIPALITY' => isset($row['municipality']) ? $row['municipality'] : null,
                'BIRDASSOCIATIONAREA' => isset($row['birdAssociationArea']) ? $row['birdAssociationArea'] : null,
                'TAXONIDS' => isset($row['taxonIDs']) ? $row['taxonIDs'] : null,
                'COLLECTIONID' => $collectoinID,
                'ISPUBLIC' => $isPublic ? 1 : 0,
                'HASCOLLECTIONID' => empty($collectoinID) ? 0 : 1,
                'IDX' => $idx
            ]);
        }
    }
}