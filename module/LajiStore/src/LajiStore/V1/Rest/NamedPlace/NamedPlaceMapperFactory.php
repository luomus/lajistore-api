<?php
namespace LajiStore\V1\Rest\NamedPlace;

use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\Feature\FullIsolationFeature;
use LajiStore\TableGateway\Feature\JsonSerializeFeature;
use LajiStore\TableGateway\Feature\MetadataFeature;
use LajiStore\TableGateway\Feature\PopulateDataFeature;
use LajiStore\TableGateway\Feature\SequencePrefixFeature;
use LajiStore\TableGateway\Feature\SourceMetadataFeature;
use LajiStore\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ObjectProperty as ObjectPropertyHydrator;

/**
 * Class NamedPlaceMapperFactory
 * @package LajiStore\V1\Rest\NamedPlace
 */
class NamedPlaceMapperFactory
{
    public function __invoke($services)
    {
        /** @var \ZF\MvcAuth\Identity\IdentityInterface $apiIdentity */
        $apiIdentity = $services->get('api-identity');
        $resultSet = new HydratingResultSet(new ObjectPropertyHydrator(), new NamedPlaceEntity());
        $queueManager = $services->get('SlmQueue\Queue\QueuePluginManager');
        $queue        = $queueManager->get('lajistore');
        return new NamedPlaceMapper(
            $services->get(ElasticService::class),
            $apiIdentity->getName(),
            new TableGateway(
                'LAJI_DOCUMENT',
                $services->get('laji-db'),
                [
                    new SequencePrefixFeature('id', 'NAMED_PLACE_SEQ', 'MNP.'),
                    new MetadataFeature($apiIdentity->getName(), NamedPlaceMapper::TYPE, 1),
                    new PopulateDataFeature([
                        '@type' => 'MNP.namedPlace'
                    ]),
                    new JsonSerializeFeature('DATA'),
                    new FullIsolationFeature('SOURCE', $apiIdentity->getName()),
                    new FullIsolationFeature('TYPE',   NamedPlaceMapper::TYPE),
                ],
                $resultSet,
                null,
                'LAJI_DOCUMENT_HISTORY'
            ),
            $queue,
            new TableGateway(
                'NAMEDPLACE_SEARCH',
                $services->get('laji-db'),
                [
                    new SourceMetadataFeature($apiIdentity->getName()),
                    new FullIsolationFeature('SOURCE', $apiIdentity->getName())
                ]
            ));
    }
}