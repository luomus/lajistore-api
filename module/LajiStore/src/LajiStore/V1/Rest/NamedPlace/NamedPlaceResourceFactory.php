<?php
namespace LajiStore\V1\Rest\NamedPlace;

class NamedPlaceResourceFactory
{
    public function __invoke($services)
    {
        return new NamedPlaceResource(
            $services->get('LajiStore\V1\Rest\NamedPlace\NamedPlaceMapper'),
            $services->get('api-identity')
        );
    }
}
