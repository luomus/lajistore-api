<?php
namespace LajiStore\V1\Rest\Annotation;

class AnnotationResourceFactory
{
    public function __invoke($services)
    {
        return new AnnotationResource(
            $services->get('LajiStore\V1\Rest\Annotation\AnnotationMapper'),
            $services->get('api-identity')
        );
    }
}
