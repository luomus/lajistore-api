<?php

namespace LajiStore\V1\Rest\Annotation;

use LajiStore\Db\Sql\AbstractWhere;
use LajiStore\Job\WarehouseAnnotation;
use LajiStore\Paginator\Adapter\DbTableGatewayWithFeature;
use LajiStore\TableGateway\TableGateway;
use LajiStore\V1\Mapper\AbstractMapper;
use SlmQueue\Queue\QueueInterface;

class AnnotationMapper extends AbstractMapper
{
    const TYPE = 'annotation';

    protected $fieldTypes = [
        'id' => 'id',
        'rootID' => AbstractWhere::TYPE_TEXT
    ];

    /**
     * @param array $params
     * @return AnnotationCollection
     */
    public function fetchAll($params)
    {
        return new AnnotationCollection(new DbTableGatewayWithFeature(
            $this->table,
            $this->whereFromParams($params),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }

    public function create($data) {
        return $this->addJobs(parent::create($data), WarehouseAnnotation::ACTION_ADD);
    }

    public function update($id, $data) {
        return $this->addJobs(parent::update($id, $data), WarehouseAnnotation::ACTION_UPDATE);
    }

    public function delete($id) {
        $annotation = new \stdClass();
        try {
            $annotation = parent::fetch($id);
        } catch (\Exception $e) {}

        if (parent::delete($id)) {
            $this->addJobs($annotation, WarehouseAnnotation::ACTION_DELETE);
            return true;
        }
        return false;
    }

    private function addJobs($data, $action) {
        if (isset($data->id) && isset($data->rootID)) {
            $this->addToWarehouseJob([$data->id], [$data->rootID], $action);
        }
        return $data;
    }

    private function addToWarehouseJob($id, $roots, $action) {
        $job = new WarehouseAnnotation();
        $job->setContent([
            'ids' => $id,
            'roots' => $roots,
            'action' => $action
        ]);
        $this->queue->push($job);
    }
}