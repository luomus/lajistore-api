<?php

namespace LajiStore\V1\Rest\DeviceIndividual;

use LajiStore\Paginator\Adapter\DbTableGatewayWithFeature;
use LajiStore\V1\Mapper\AbstractMapper;

class DeviceIndividualMapper extends AbstractMapper
{
    const TYPE = 'deviceIndividual';

    protected $fieldTypes = [
        'id' => 'id',
        'deviceID' => 'text',
        'individualID' => 'text',
    ];
    
    /**
     * @param array $params
     * @return DeviceIndividualCollection
     */
    public function fetchAll($params)
    {
        return new DeviceIndividualCollection(new DbTableGatewayWithFeature(
            $this->table,
            $this->whereFromParams($params),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }
}