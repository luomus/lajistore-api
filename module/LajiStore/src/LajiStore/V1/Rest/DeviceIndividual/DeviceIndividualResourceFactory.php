<?php
namespace LajiStore\V1\Rest\DeviceIndividual;

class DeviceIndividualResourceFactory
{
    public function __invoke($services)
    {
        return new DeviceIndividualResource(
            $services->get('LajiStore\V1\Rest\DeviceIndividual\DeviceIndividualMapper'),
            $services->get('api-identity')
        );
    }
}
