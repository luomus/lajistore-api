<?php
namespace LajiStore\V1\Rest\Document;

class DocumentResourceFactory
{
    public function __invoke($services)
    {
        return new DocumentResource(
            $services->get('LajiStore\V1\Rest\Document\DocumentMapper'),
            $services->get('api-identity')
        );
    }
}
