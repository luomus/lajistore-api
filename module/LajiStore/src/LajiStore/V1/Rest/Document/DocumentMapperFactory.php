<?php
namespace LajiStore\V1\Rest\Document;

use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\Feature\FullIsolationFeature;
use LajiStore\TableGateway\Feature\JsonSerializeFeature;
use LajiStore\TableGateway\Feature\MetadataFeature;
use LajiStore\TableGateway\Feature\PopulateDataFeature;
use LajiStore\TableGateway\Feature\SequencePrefixFeature;
use LajiStore\TableGateway\Feature\SourceMetadataFeature;
use LajiStore\TableGateway\Feature\WarehouseFeature;
use LajiStore\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ObjectProperty as ObjectPropertyHydrator;

/**
 * Class DocumentMapperFactory
 * @package LajiStore\V1\Rest\Device
 */
class DocumentMapperFactory
{
    public function __invoke($services)
    {
        /** @var \ZF\MvcAuth\Identity\IdentityInterface $apiIdentity */
        $apiIdentity  = $services->get('api-identity');
        $resultSet    = new HydratingResultSet(new ObjectPropertyHydrator(), new DocumentEntity());
        $queueManager = $services->get('SlmQueue\Queue\QueuePluginManager');
        $queue        = $queueManager->get('lajistore');
        return new DocumentMapper(
            $services->get(ElasticService::class),
            $apiIdentity->getName(),
            new TableGateway(
                'LAJI_DOCUMENT',
                $services->get('laji-db'),
                [
                    new SequencePrefixFeature('id', 'LAJISTORE_SEQ', 'JX.'),
                    new MetadataFeature($apiIdentity->getName(), DocumentMapper::TYPE,1),
                    new PopulateDataFeature([
                        '@type' => 'MY.document',
                        'gatheringEvent/id' => PopulateDataFeature::SUB_ID,
                        'gatheringEvent/@type' => 'MZ.gatheringEvent',
                        'gatherings/*/id' => PopulateDataFeature::SUB_ID,
                        'gatherings/*/@type' => 'MY.gathering',
                        'gatherings/*/units/*/unitGathering/id' => PopulateDataFeature::SUB_ID,
                        'gatherings/*/units/*/unitGathering/@type' => 'MZ.unitGathering',
                        'gatherings/*/units/*/id' => PopulateDataFeature::SUB_ID,
                        'gatherings/*/units/*/@type' => 'MY.unit',
                        'gatherings/*/units/*/identifications/*/id' => PopulateDataFeature::SUB_ID,
                        'gatherings/*/units/*/identifications/*/@type' => 'MY.identification',
                        'gatherings/*/units/*/typeSpecimens/*/id' => PopulateDataFeature::SUB_ID,
                        'gatherings/*/units/*/typeSpecimens/*/@type' => 'MY.typeSpecimen',
                    ], 'id', 'SEQUENCE'),
                    new JsonSerializeFeature('DATA'),
                    new FullIsolationFeature('SOURCE', $apiIdentity->getName()),
                    new FullIsolationFeature('TYPE',   DocumentMapper::TYPE)
                ],
                $resultSet,
                null,
                'LAJI_DOCUMENT_HISTORY'
            ),
            $queue,
            new TableGateway(
                'DOCUMENT_SEARCH',
                $services->get('laji-db'),
                [
                    new SourceMetadataFeature($apiIdentity->getName()),
                    new FullIsolationFeature('SOURCE', $apiIdentity->getName())
                ]
            )
        );
    }
}