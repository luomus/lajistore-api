<?php

namespace LajiStore\V1\Rest\Document;

use LajiStore\Db\Sql\AbstractWhere;
use LajiStore\Job\Warehouse;
use LajiStore\Paginator\Adapter\DbYearAggregateWithFeature;
use LajiStore\Paginator\Adapter\DbSearchTableGatewayWithFeature;
use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\Feature\FieldFeature;
use LajiStore\TableGateway\Feature\FilterFeature;
use LajiStore\TableGateway\TableGateway;
use LajiStore\V1\Mapper\AbstractSearchTableMapper;
use SlmQueue\Queue\QueueInterface;
use Zend\Db\Sql\Predicate\Expression;

class DocumentMapper extends AbstractSearchTableMapper
{
    const TYPE = 'document';

    protected $fieldTypes = [
        'id' => AbstractWhere::TYPE_ID,
        'creator' => AbstractWhere::TYPE_TEXT,
        'editor' => AbstractWhere::TYPE_TEXT,
        'editors' => AbstractWhere::TYPE_ARRAY,
        'owners' => AbstractWhere::TYPE_ARRAY,
        'deviceID' => AbstractWhere::TYPE_TEXT,
        'individualID' => AbstractWhere::TYPE_TEXT,
        'namedPlaceID' => AbstractWhere::TYPE_TEXT,
        'collectionID' => AbstractWhere::TYPE_TEXT,
        'publicityRestrictions' => AbstractWhere::TYPE_TEXT,
        'year' => AbstractWhere::TYPE_NUMBER,
        'date' => AbstractWhere::TYPE_NUMBER,
        'isTemplate' => AbstractWhere::TYPE_BOOLEAN,
        AbstractWhere::SEARCH_TABLE => []
    ];

    /**
     * @var array Key is the field that can be searched with and value means the following:
     * true = result is unambiguous
     * false = result is ambiguous
     */
    protected $fieldsInstructions = [
        'id' => true,
        'formID' => true,
        'creator' => true,
        'locked' => true,
        'templateName' => true,
        'templateDescription' => true,
        'publicityRestrictions' => true,
        'dateEdited' => true,
        'gatheringEvent.leg' => true,
        'namedPlaceID' => true,
        'gatheringEvent.dateBegin' => true,
        'gatheringEvent.dateEnd' => true,
        'gatherings[*].id' => false,
        'gatherings[*].locality' => false,
        'gatherings[*].namedPlaceID' => false,
        'gatherings[*].dateEnd' => false,
        'gatherings[*].dateBegin' => false,
        'gatherings[*].units[*]' => false,
        'gatherings[*].units[*].identifications[*]' => false,
        'gatherings[*].units[*].count' => false,
        'gatherings[*].units[*].individualCount' => false,
        'gatherings[*].units[*].pairCount' => false,
        'gatherings[*].units[*].abundanceString' => false,
        'gatherings[*].units[*].maleIndividualCount' => false,
        'gatherings[*].units[*].femaleIndividualCount' => false,
        'gatherings[*].units[*].informalNameString' => false,
        'gatherings[*].units[*].identifications[*].taxon' => false,
        'gatherings[*].units[*].identifications[*].taxonVerbatim' => false,
        'gatherings[*].units[*].identifications[*].taxonID' => false
    ];

    public function __construct(ElasticService $elasticService, $apiIdentity, TableGateway $table, QueueInterface $queue, TableGateway $searchTable)
    {
        $this->queue = $queue;
        $this->fieldTypes[AbstractWhere::SEARCH_TABLE]['year'] = function(AbstractWhere $where, $field, $value, $operator) {
            $sql = 'YEAREND >= ? AND YEARSTART <= ?';
            if ($value == '0') {
                $sql = 'YEAREND is null';
                return new Expression($sql);
            } else {
                $value = is_array($value) ? $value : [$value, $value];
                return new Expression($sql, $value);
            }
        };
        $this->fieldTypes[AbstractWhere::SEARCH_TABLE]['date'] = function(AbstractWhere $where, $field, $value, $operator) {
            $sql = 'DATEEND >= ? AND DATESTART <= ?';
            if (is_string($value)) {
                $parts = explode('/', $value);
                if (count($parts) === 2) {
                    $value = [
                        preg_replace("/[^0-9]/", "", $parts[0]),
                        preg_replace("/[^0-9]/", "", $parts[1])
                    ];
                }
            }
            return new Expression($sql, is_array($value) ? $value : [$value, $value]);
        };
        parent::__construct($elasticService, $apiIdentity, $table, $queue, $searchTable);
    }

    public function fetch($id, $queryParams = [])
    {
        if (isset($queryParams[self::PARAM_FILTER])) {
            $this->addFeature(new FilterFeature($queryParams[self::PARAM_FILTER]));
        }
        return parent::fetch($id, $queryParams);
    }

    /**
     * @param array $params
     * @return DocumentCollection
     * @throws \Exception
     */
    public function fetchAll($params)
    {
        if (isset($params[self::PARAM_FILTER])) {
            $this->addFeature(new FilterFeature($params[self::PARAM_FILTER]));
        }
        if (isset($params[self::PARAM_YEARS])) {
            return new DocumentCollection(new DbYearAggregateWithFeature(
                $this->table,
                $this->searchTable,
                $this->whereFromParams($params, 'OracleFlat')
            ));
        }
        if (isset($params[self::PARAM_FIELDS]) && !empty($params[self::PARAM_FIELDS])) {
            $this->addFeature(new FieldFeature($params[self::PARAM_FIELDS], $this->fieldsInstructions));
        }
        return new DocumentCollection(new DbSearchTableGatewayWithFeature(
            $this->table,
            $this->searchTable,
            $this->whereFromParams($params, 'OracleFlat'),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }

    public function create($data) {
        return $this->addJobs(parent::create($data), Warehouse::ACTION_ADD);
    }

    public function update($id, $data) {
        return $this->addJobs(parent::update($id, $data), Warehouse::ACTION_UPDATE);
    }

    public function addSearchRow($id, $data) {
        $days = $this->pickObservationDays($data);
        $isTemplate = $this->pickValue($data, 'isTemplate', false);
        $isTemplate = $isTemplate === 'true' ||$isTemplate === true ? true : false;

        $yearStart = null;
        $yearEnd = null;
        $dayStart = null;
        $dayEnd = null;

        if (!empty($days)) {
            $yearStart = floor($days[0] / 10000);
            $yearEnd = floor($days[1] / 10000);
            $dayStart = $days[0];
            $dayEnd = $days[1];
        }
        $owners = [];
        $creator = $this->pickValue($data, 'creator', null);
        if ($creator !== null) {
            array_push($owners, $creator);
        }
        $editors = $this->pickValue($data, 'editors', []);
        foreach ($editors as $editor) {
            if (!in_array($editor, $owners)) {
                array_push($owners, $editor);
            }
        }
        if (count($owners) === 0) {
            $owners = [null];
        }
        foreach ($owners as $idx => $owner) {
            $this->searchTable->insert([
                'ID' => $id,
                'YEARSTART' => $yearStart,
                'YEAREND' => $yearEnd,
                'DATESTART' => $dayStart,
                'DATEEND' => $dayEnd,
                'CREATOR' => $creator,
                'EDITOR' => $this->pickValue($data, 'editor', null),
                'EDITORS' => in_array($owner, $editors) ? $owner : null,
                'OWNERS' => $owner,
                'DEVICEID' => $this->pickValue($data, 'deviceID'),
                'INDIVIDUALID' => $this->pickValue($data, 'individualID'),
                'NAMEDPLACEID' => $this->pickValue($data, 'namedPlaceID'),
                'COLLECTIONID' => $this->pickValue($data, 'collectionID'),
                'ISTEMPLATE' => $isTemplate ? 1 : 0,
                'PUBLICITYRESTRICTIONS' => $this->pickValue($data, 'publicityRestrictions'),
                'IDX' => $idx
            ]);
        }
    }

    public function delete($id) {
        if (parent::delete($id)) {
            $this->addToWarehouseJob([$id], Warehouse::ACTION_DELETE);
            return true;
        }
        return false;
    }

    public function deleteAll($data = null)
    {
        if (!empty($data)) {
            throw new \Exception(
                'Deleting all with parameters or data in the body is not supported!',
                406
            );
        }
        parent::deleteAll($data);
        $this->searchTable->delete(new Expression('1 = 1'));
        return true;
    }

    private function pickObservationDays($data, &$days = []) {
        $data = (array)$data;
        $dateFields = ['dateBegin', 'dateEnd'];
        foreach ($dateFields as $field) {
            if (isset($data[$field])) {
                $date = $this->dateToInt($data[$field], $field === 'dateBegin');
                if ($date) {
                    if (!isset($days[0]) || $days[0] > $date) {
                        $days[0] = $date;
                    }
                    if (!isset($days[1]) || $days[1] < $date) {
                        $days[1] = $date;
                    }
                }
            }
        }
        $subArrays = ['gatherings', 'units'];
        foreach ($subArrays as $arrField) {
            if (isset($data[$arrField])) {
                foreach ($data[$arrField] as $sub) {
                    $this->pickObservationDays($sub, $days);
                }
            }
        }
        $subObj = ['gatheringEvent', 'unitGathering'];
        foreach ($subObj as $objField) {
            if (isset($data[$objField])) {
                $this->pickObservationDays($data[$objField], $days);
            }
        }
        return $days;
    }

    private function addJobs($data, $action) {
        if (!empty($data) && isset($data->id)) {
            $this->addToWarehouseJob([$data->id], $action);
        }
        return $data;
    }

    private function addToWarehouseJob($id, $action) {
        $job = new Warehouse();
        $job->setContent([
            'ids' => $id,
            'action' => $action
        ]);
        $this->queue->push($job);
    }
}