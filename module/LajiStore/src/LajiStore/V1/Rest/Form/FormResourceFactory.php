<?php
namespace LajiStore\V1\Rest\Form;

class FormResourceFactory
{
    public function __invoke($services)
    {
        return new FormResource(
            $services->get('LajiStore\V1\Rest\Form\FormMapper'),
            $services->get('api-identity')
        );
    }
}
