<?php
namespace LajiStore\V1\Rest\Form;

use LajiStore\Paginator\Adapter\DbTableGatewayWithFeature;
use LajiStore\V1\Mapper\AbstractMapper;

class FormMapper extends AbstractMapper
{
    const TYPE = 'form';

    protected $fieldTypes = [
        'id' => 'id',
    ];

    /**
     * @param array $params
     * @return FormCollection
     */
    public function fetchAll($params)
    {
        return new FormCollection(new DbTableGatewayWithFeature(
            $this->table,
            $this->whereFromParams($params),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }
}