<?php
namespace LajiStore\V1\Rest\Individual;

class IndividualResourceFactory
{
    public function __invoke($services)
    {
        return new IndividualResource(
            $services->get('LajiStore\V1\Rest\Individual\IndividualMapper'),
            $services->get('api-identity')
        );
    }
}
