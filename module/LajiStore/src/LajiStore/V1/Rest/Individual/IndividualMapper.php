<?php

namespace LajiStore\V1\Rest\Individual;

use LajiStore\Paginator\Adapter\DbTableGatewayWithFeature;
use LajiStore\V1\Mapper\AbstractMapper;

class IndividualMapper extends AbstractMapper
{
    const TYPE = 'individual';

    protected $fieldTypes = [
        'id' => 'id',
        'individualID' => 'text',
        'deleted' => 'boolean'
    ];

    /**
     * @param array $params
     * @return IndividualCollection
     */
    public function fetchAll($params)
    {
        return new IndividualCollection(new DbTableGatewayWithFeature(
            $this->table,
            $this->whereFromParams($params),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }
}