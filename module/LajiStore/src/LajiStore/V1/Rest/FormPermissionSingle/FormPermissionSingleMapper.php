<?php

namespace LajiStore\V1\Rest\FormPermissionSingle;

use LajiStore\Db\Sql\AbstractWhere;
use LajiStore\Paginator\Adapter\DbTableGatewayWithFeature;
use LajiStore\V1\Mapper\AbstractMapper;

class FormPermissionSingleMapper extends AbstractMapper
{
    const TYPE = 'formPermissionSingle';

    protected $fieldTypes = [
        'id' => 'id',
        'collectionID' => AbstractWhere::TYPE_TEXT,
        'userID' => AbstractWhere::TYPE_TEXT
    ];
    
    /**
     * @param array $params
     * @return FormPermissionSingleCollection
     */
    public function fetchAll($params)
    {
        return new FormPermissionSingleCollection(new DbTableGatewayWithFeature(
            $this->table,
            $this->whereFromParams($params),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }
}