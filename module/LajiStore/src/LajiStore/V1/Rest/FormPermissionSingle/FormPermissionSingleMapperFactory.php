<?php
namespace LajiStore\V1\Rest\FormPermissionSingle;

use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\Feature\FullIsolationFeature;
use LajiStore\TableGateway\Feature\JsonSerializeFeature;
use LajiStore\TableGateway\Feature\MetadataFeature;
use LajiStore\TableGateway\Feature\PopulateDataFeature;
use LajiStore\TableGateway\Feature\SequencePrefixFeature;
use LajiStore\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ObjectProperty as ObjectPropertyHydrator;

/**
 * Class FormPermissionSingleMapperFactory
 * @package LajiStore\V1\Rest\FormPermissionSingle
 */
class FormPermissionSingleMapperFactory
{
    public function __invoke($services)
    {
        /** @var \ZF\MvcAuth\Identity\IdentityInterface $apiIdentity */
        $apiIdentity = $services->get('api-identity');
        $resultSet = new HydratingResultSet(new ObjectPropertyHydrator(), new FormPermissionSingleEntity());
        $queueManager = $services->get('SlmQueue\Queue\QueuePluginManager');
        $queue        = $queueManager->get('lajistore');
        return new FormPermissionSingleMapper(
            $services->get(ElasticService::class),
            $apiIdentity->getName(),
            new TableGateway(
            'LAJI_DOCUMENT',
            $services->get('laji-db'),
            [
                new SequencePrefixFeature('id', 'FORM_PERMISSION_SEQ', 'MFP.'),
                new MetadataFeature($apiIdentity->getName(), FormPermissionSingleMapper::TYPE, 1),
                new PopulateDataFeature([
                    '@type' => 'MFP.formPermissionSingle'
                ]),
                new JsonSerializeFeature('DATA'),
                new FullIsolationFeature('SOURCE', $apiIdentity->getName()),
                new FullIsolationFeature('TYPE',   FormPermissionSingleMapper::TYPE),
            ],
            $resultSet,
            null,
            'LAJI_DOCUMENT_HISTORY'
        ), $queue);
    }
}