<?php
namespace LajiStore\V1\Rest\FormPermissionSingle;

class FormPermissionSingleResourceFactory
{
    public function __invoke($services)
    {
        return new FormPermissionSingleResource(
            $services->get('LajiStore\V1\Rest\FormPermissionSingle\FormPermissionSingleMapper'),
            $services->get('api-identity')
        );
    }
}
