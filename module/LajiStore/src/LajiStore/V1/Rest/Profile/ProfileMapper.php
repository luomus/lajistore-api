<?php
namespace LajiStore\V1\Rest\Profile;

use LajiStore\Paginator\Adapter\DbTableGatewayWithFeature;
use LajiStore\V1\Mapper\AbstractMapper;

class ProfileMapper extends AbstractMapper
{
    const TYPE = 'profile';

    protected $fieldTypes = [
        'id' => 'id',
        'userID' => 'text',
        'profileKey' => 'text'
    ];

    /**
     * @param array $params
     * @return ProfileCollection
     */
    public function fetchAll($params)
    {
        return new ProfileCollection(new DbTableGatewayWithFeature(
            $this->table,
            $this->whereFromParams($params),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }
}