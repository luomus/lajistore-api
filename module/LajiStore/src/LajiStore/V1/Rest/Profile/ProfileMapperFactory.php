<?php
namespace LajiStore\V1\Rest\Profile;

use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\Feature\EditIsolationFeature;
use LajiStore\TableGateway\Feature\FullIsolationFeature;
use LajiStore\TableGateway\Feature\JsonSerializeFeature;
use LajiStore\TableGateway\Feature\MetadataFeature;
use LajiStore\TableGateway\Feature\PopulateDataFeature;
use LajiStore\TableGateway\Feature\SequencePrefixFeature;
use LajiStore\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ObjectProperty as ObjectPropertyHydrator;

/**
 * Class IndividualMapperFactory
 * @package LajiStore\V1\Rest\Profile
 */
class ProfileMapperFactory
{
    public function __invoke($services)
    {
        /** @var \ZF\MvcAuth\Identity\IdentityInterface $apiIdentity */
        $apiIdentity = $services->get('api-identity');
        $resultSet = new HydratingResultSet(new ObjectPropertyHydrator(), new ProfileEntity());
        $queueManager = $services->get('SlmQueue\Queue\QueuePluginManager');
        $queue        = $queueManager->get('lajistore');
        return new ProfileMapper(
            $services->get(ElasticService::class),
            $apiIdentity->getName(),
            new TableGateway(
            'LAJI_DOCUMENT',
            $services->get('laji-db'),
            [
                new SequencePrefixFeature('id', 'LAJISTORE_SEQ', 'JX.'),
                new MetadataFeature($apiIdentity->getName(), ProfileMapper::TYPE, 1),
                new PopulateDataFeature([
                    '@type' => 'MA.profile'
                ]),
                new JsonSerializeFeature('DATA'),
                new EditIsolationFeature('SOURCE', $apiIdentity->getName()),
                new FullIsolationFeature('TYPE',   ProfileMapper::TYPE),
            ],
            $resultSet,
            null,
            'LAJI_DOCUMENT_HISTORY'
        ), $queue);
    }
}