<?php
namespace LajiStore\V1\Rest\Profile;

class ProfileResourceFactory
{
    public function __invoke($services)
    {
        return new ProfileResource(
            $services->get('LajiStore\V1\Rest\Profile\ProfileMapper'),
            $services->get('api-identity')
        );
    }
}
