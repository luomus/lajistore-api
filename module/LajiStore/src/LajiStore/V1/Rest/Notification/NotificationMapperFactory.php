<?php
namespace LajiStore\V1\Rest\Notification;

use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\Feature\FullIsolationFeature;
use LajiStore\TableGateway\Feature\JsonSerializeFeature;
use LajiStore\TableGateway\Feature\MetadataFeature;
use LajiStore\TableGateway\Feature\PopulateDataFeature;
use LajiStore\TableGateway\Feature\SequencePrefixFeature;
use LajiStore\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ObjectProperty as ObjectPropertyHydrator;

/**
 * Class NotificationMapperFactory
 * @package LajiStore\V1\Rest\Notification
 */
class NotificationMapperFactory
{
    public function __invoke($services)
    {
        /** @var \ZF\MvcAuth\Identity\IdentityInterface $apiIdentity */
        $apiIdentity = $services->get('api-identity');
        $resultSet = new HydratingResultSet(new ObjectPropertyHydrator(), new NotificationEntity());
        $queueManager = $services->get('SlmQueue\Queue\QueuePluginManager');
        $queue        = $queueManager->get('lajistore');
        return new NotificationMapper(
            $services->get(ElasticService::class),
            $apiIdentity->getName(),
            new TableGateway(
            'LAJI_DOCUMENT',
            $services->get('laji-db'),
            [
                new SequencePrefixFeature('id', 'NOTIFICATION_SEQ', 'MHN.'),
                new MetadataFeature($apiIdentity->getName(), NotificationMapper::TYPE, 1),
                new PopulateDataFeature([
                    '@type' => 'MHN.notification'
                ]),
                new JsonSerializeFeature('DATA'),
                new FullIsolationFeature('SOURCE', $apiIdentity->getName()),
                new FullIsolationFeature('TYPE',   NotificationMapper::TYPE),
            ],
            $resultSet,
            null,
            'LAJI_DOCUMENT_HISTORY'
        ), $queue);
    }
}