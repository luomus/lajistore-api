<?php
namespace LajiStore\V1\Rest\Notification;

class NotificationResourceFactory
{
    public function __invoke($services)
    {
        return new NotificationResource(
            $services->get('LajiStore\V1\Rest\Notification\NotificationMapper'),
            $services->get('api-identity')
        );
    }
}
