<?php

namespace LajiStore\V1\Rest\Notification;

use LajiStore\Db\Sql\AbstractWhere;
use LajiStore\Paginator\Adapter\DbTableGatewayWithFeature;
use LajiStore\V1\Mapper\AbstractMapper;

class NotificationMapper extends AbstractMapper
{
    const TYPE = 'notification';

    protected $fieldTypes = [
        'id' => 'id',
        'seen' => AbstractWhere::TYPE_BOOLEAN,
        'toPerson' => AbstractWhere::TYPE_TEXT,
        'annotationID' => AbstractWhere::TYPE_TEXT
    ];
    
    /**
     * @param array $params
     * @return NotificationCollection
     */
    public function fetchAll($params)
    {
        return new NotificationCollection(new DbTableGatewayWithFeature(
            $this->table,
            $this->whereFromParams($params),
            ['CREATED' => 'DESC', 'ID' => 'DESC']
        ));
    }
}