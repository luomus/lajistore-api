<?php
namespace LajiStore\V1\Rest\Device;

class DeviceResourceFactory
{
    public function __invoke($services)
    {
        return new DeviceResource(
            $services->get('LajiStore\V1\Rest\Device\DeviceMapper'),
            $services->get('api-identity')
        );
    }
}
