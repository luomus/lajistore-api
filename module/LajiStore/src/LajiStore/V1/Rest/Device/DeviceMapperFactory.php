<?php
namespace LajiStore\V1\Rest\Device;

use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\Feature\FullIsolationFeature;
use LajiStore\TableGateway\Feature\JsonSerializeFeature;
use LajiStore\TableGateway\Feature\MetadataFeature;
use LajiStore\TableGateway\Feature\PopulateDataFeature;
use LajiStore\TableGateway\Feature\SequencePrefixFeature;
use LajiStore\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Stdlib\Hydrator\ObjectProperty as ObjectPropertyHydrator;

/**
 * Class DeviceMapperFactory
 * @package LajiStore\V1\Rest\Device
 */
class DeviceMapperFactory
{
    public function __invoke($services)
    {
        /** @var \ZF\MvcAuth\Identity\IdentityInterface $apiIdentity */
        $apiIdentity = $services->get('api-identity');
        $resultSet = new HydratingResultSet(new ObjectPropertyHydrator(), new DeviceEntity());
        $queueManager = $services->get('SlmQueue\Queue\QueuePluginManager');
        $queue        = $queueManager->get('lajistore');
        return new DeviceMapper(
            $services->get(ElasticService::class),
            $apiIdentity->getName(),
            new TableGateway(
            'LAJI_DOCUMENT',
            $services->get('laji-db'),
            [
                new SequencePrefixFeature('id', 'LAJISTORE_SEQ', 'JX.'),
                new MetadataFeature($apiIdentity->getName(),DeviceMapper::TYPE, 1),
                new PopulateDataFeature([
                    '@type' => 'MXC.device'
                ]),
                new JsonSerializeFeature('DATA'),
                new FullIsolationFeature('SOURCE', $apiIdentity->getName()),
                new FullIsolationFeature('TYPE',   DeviceMapper::TYPE),
            ],
            $resultSet,
            null,
            'LAJI_DOCUMENT_HISTORY'
        ), $queue);
    }
}