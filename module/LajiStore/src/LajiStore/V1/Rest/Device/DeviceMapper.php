<?php

namespace LajiStore\V1\Rest\Device;

use LajiStore\Paginator\Adapter\DbTableGatewayWithFeature;
use LajiStore\V1\Mapper\AbstractMapper;

class DeviceMapper extends AbstractMapper
{
    const TYPE = 'device';

    protected $fieldTypes = [
        'id' => 'id',
        'deviceManufacturerID' => 'text',
    ];
    
    /**
     * @param array $params
     * @return DeviceCollection
     */
    public function fetchAll($params)
    {
        return new DeviceCollection(new DbTableGatewayWithFeature(
            $this->table,
            $this->whereFromParams($params),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }
}