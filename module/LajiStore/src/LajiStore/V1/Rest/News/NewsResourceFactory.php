<?php
namespace LajiStore\V1\Rest\News;

class NewsResourceFactory
{
    public function __invoke($services)
    {
        return new NewsResource(
            $services->get('LajiStore\V1\Rest\News\NewsMapper'),
            $services->get('api-identity')
        );
    }
}
