<?php

namespace LajiStore\V1\Rest\News;

use LajiStore\Db\Sql\AbstractWhere;
use LajiStore\Paginator\Adapter\DbTableGatewayWithFeature;
use LajiStore\V1\Mapper\AbstractMapper;

class NewsMapper extends AbstractMapper
{
    const TYPE = 'news';

    protected $fieldTypes = [
        'id' => AbstractWhere::TYPE_ID,
        'targets' => AbstractWhere::TYPE_ARRAY,
        'targetIndividualIds' => AbstractWhere::TYPE_ARRAY,
        'language' => AbstractWhere::TYPE_TEXT
    ];
    
    /**
     * @param array $params
     * @return NewsCollection
     */
    public function fetchAll($params)
    {
        return new NewsCollection(new DbTableGatewayWithFeature(
            $this->table,
            $this->whereFromParams($params),
            ['EDITED' => 'DESC', 'ID' => 'DESC']
        ));
    }
}