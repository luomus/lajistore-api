<?php
namespace LajiStore\V1\Mapper;

use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\TableGateway;
use SlmQueue\Queue\QueueInterface;

abstract class AbstractSearchTableMapper extends AbstractMapper {

    protected $searchTable;

    public function __construct(ElasticService $elasticService, $apiIdentity, TableGateway $table, QueueInterface $queue, TableGateway $searchTable)
    {

        $this->searchTable = $searchTable;
        parent::__construct($elasticService, $apiIdentity, $table, $queue);
    }

    public function getSearchTableGateway() {
        return $this->searchTable;
    }

    public function create($data) {
        return parent::create($data, function($id) use($data) {
            $this->addSearchRow($id, $data);
        });
    }

    public function update($id, $data) {
        return parent::update($id, $data, function($id) use($data) {
            $this->deleteSearchRow($id);
            $this->addSearchRow($id, $data);
        });
    }

    public function delete($id) {
        return parent::delete($id, function($id) {
            $this->deleteSearchRow($id);
        });
    }

    public function deleteSearchRow($id) {
        return $this->searchTable->delete(['ID' => $id]);
    }

    protected function pickValue($src, $field, $empty = null) {
        if (is_object($src)) {
            return property_exists($src, $field) ? $src->{$field} : $empty;
        } else if (is_array($src)) {
            return array_key_exists($field, $src) ? $src[$field] : $empty;
        }
        return $empty;
    }

    protected function dateToInt($date, $toStart = true) {
        if (preg_match('/^[0-9]{4}$/', $date)) {
            $date .= $toStart ? '-01-01' : '-12-31';
        }
        $dateParts = date_parse($date);
        if (is_array($dateParts)) {
            $year = isset($dateParts['year']) ? $dateParts['year']: 0;
            $month = isset($dateParts['month']) ? $dateParts['month'] : ($toStart ? 1 : 12);
            $day = isset($dateParts['day']) ? $dateParts['day'] : ($toStart ? 1 : 31);
            return $year * 10000 + $month * 100 + $day;
        }
        return false;
    }

    protected function cartesian($input) {
        $result = array(array());
        foreach ($input as $key => $values) {
            $append = array();
            foreach($result as $product) {
                foreach($values as $item) {
                    $product[$key] = $item;
                    $append[] = $product;
                }
            }
            $result = $append;
        }

        return $result;
    }

    public abstract function addSearchRow($id, $data);
}
