<?php

namespace LajiStore\V1\Mapper;

use DomainException;
use InvalidArgumentException;
use LajiStore\ApiHistory\ApiHistory;
use LajiStore\ApiHistory\ApiHistoryResponse;
use LajiStore\Db\Sql\OracleFlatWhere;
use LajiStore\Db\Sql\OracleWhere;
use LajiStore\Exception\NotFoundException;
use LajiStore\Job\Elastic;
use LajiStore\Mapper\MapperInterface;
use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\TableGateway;
use SlmQueue\Queue\QueueInterface;
use Traversable;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\TableGateway\Feature\AbstractFeature;
use Zend\Stdlib\ArrayUtils;

abstract class AbstractMapper implements MapperInterface
{
    const PARAM_FILTER = 'filter';
    const PARAM_FIELDS = 'fields';
    const PARAM_YEARS = 'countByYear';
    const TYPE = '';

    protected $fieldTypes = [];

    /**
     * @var ElasticService
     */
    protected $elasticService;

    /** @var TableGateway */
    protected $table;

    /**
     * @var QueueInterface
     */
    protected $queue;

    /**
     * @var string
     */
    protected $apiIdentity;

    /**
     * AbstractMapper constructor.
     * @param ElasticService $elasticService
     * @param $apiIdentity
     * @param TableGateway $table
     * @param QueueInterface $queue
     */
    public function __construct(ElasticService $elasticService, $apiIdentity, TableGateway $table, QueueInterface $queue)
    {
        $this->elasticService = $elasticService;
        $this->apiIdentity = $apiIdentity;
        $this->table = $table;
        $this->queue = $queue;
    }

    /**
     * @param array|Traversable|\stdClass $data
     * @param callable $inTransaction
     * @return mixed
     */
    public function create($data, $inTransaction = null)
    {
        if ($data instanceof Traversable) {
            $data = ArrayUtils::iteratorToArray($data);
        }
        if (is_object($data)) {
            $data = (array) $data;
        }

        if (!is_array($data)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid data provided to %s; must be an array or Traversable',
                __METHOD__
            ));
        }

        if (isset($data['id'])) {
            try {
                $this->fetch($data['id']);
                throw new DomainException("Object with the ID {$data['id']} already exists in the database", 406);
            } catch (NotFoundException $e) {
            }
        }
        $connection = $this->table->getAdapter()->getDriver()->getConnection();
        $connection->beginTransaction();
        $this->table->insert($data);

        $id        = $this->table->lastInsertValue;
        $resultSet = $this->table->select(['ID' => $id]);
        $current   = $resultSet->current();
        if (is_callable($inTransaction)) {
            $inTransaction($id);
        }
        $connection->commit();

        if ($current === false) {
            throw new DomainException('Insert operation failed or did not result in new row', 500);
        }
        try {
            $this->elasticService->copyToElastic($id, true);
        } catch (\Exception $e) {}
        $this->addToElasticJob($id, Elastic::ACTION_ADD);
        return $current;
    }

    /**
     * @param string $id
     * @param array $queryParams
     * @return mixed
     */
    public function fetch($id, $queryParams = [])
    {
        if (isset($queryParams['version']) || isset($queryParams['diffTo'])) {
            return $this->fetchFromHistory($id, $queryParams);
        }
        return $this->_fetch(['ID' => $id]);
    }

    protected function fetchFromHistory($id, $queryParams) {
        if (isset($queryParams['diffTo'])) {
            $srcWhere = (isset($queryParams['version']) && is_numeric($queryParams['version'])) ?
                ['ID' => $id, 'VERSION' => $queryParams['version']] :
                ['ID' => $id];
            $destWhere = is_numeric($queryParams['diffTo']) ?
                ['ID' => $id, 'VERSION' => $queryParams['diffTo']] :
                ['ID' => $id];
            return $this->diffResult($this->_fetch($srcWhere), $this->_fetch($destWhere));
        }
        if (isset($queryParams['version']) && is_numeric($queryParams['version'])) {
            return $this->_fetch(['ID' => $id, 'VERSION' => $queryParams['version']]);
        }
        return new ApiHistoryResponse(new ApiHistory($this->getHistoryVersions($id)));
    }

    protected function getHistoryVersions($id) {
        $columns = [
            'VERSION',
            'EDITED' => new Expression('TO_CHAR(EDITED, \'YYYY-MM-DD"T"HH24:MI:SS\')'),
            'EDITOR' => new Expression('json_value(data, \'$.editor\')')
        ];
        $this->table->setUseHistory(true);
        $select = $this->table->getSql()->select();
        $select->where(['ID' => $id])->columns($columns);
        $resultSet = $this->table->selectWith($select);
        $this->table->setUseHistory(false);
        $versions = [];
        foreach ($resultSet as $result) {
            $versions[(int)$result->VERSION] = [
                'version' => $result->VERSION,
                'created' => $result->EDITED,
                'editor' => $result->EDITOR
            ];
        }
        $select = $this->table->getSql()->select();
        $select->where(['ID' => $id])->columns($columns);
        $resultSet = $this->table->selectWith($select);
        foreach ($resultSet as $result) {
            $versions[(int)$result->VERSION] = [
                'version' => $result->VERSION,
                'created' => $result->EDITED,
                'editor' => $result->EDITOR
            ];
        }
        ksort($versions);
        return array_values($versions);
    }

    protected function diffResult($src, $dest) {
        return new ApiHistoryResponse(new ApiHistory(
            null,
            $this->toArray($src),
            $this->toArray($dest)
        ));
    }

    protected function toArray($result) {
        if (is_array($result) || $result === null) {
            return $result;
        }
        $resultSetProto = $this->table->getResultSetPrototype();
        if ($resultSetProto instanceof HydratingResultSet) {
            $hydrator = $resultSetProto->getHydrator();
            return $hydrator->extract($result);
        }
        throw new DomainException('Extracting array data failed', 500);
    }


    protected function  _fetch($where) {
        if (isset($where['VERSION'])) {
            $this->table->setUseHistory(true);
            $resultSet = $this->table->select($where);
            $this->table->setUseHistory(false);
            $item = $resultSet->current();
            if ($item === false) {
                $resultSet = $this->table->select($where);
                $item = $resultSet->current();
            }
        } else {
            $resultSet = $this->table->select($where);
            $item = $resultSet->current();
        }

        if ($item === false) {
            throw new NotFoundException('Not found', 404);
        }
        return $item;
    }

    /**
     * @param array $params
     * @return mixed
     */
    abstract public function fetchAll($params);

    /**
     * @param string $id
     * @param array|Traversable|\stdClass $data
     * @param callable $inTransaction
     * @return mixed
     */
    public function update($id, $data, $inTransaction = null)
    {
        if (is_object($data)) {
            $data = (array) $data;
        }
        if ((isset($data['id']) && $data['id'] != $id)) {
            throw new DomainException('Id should be same in the data and in the url', 400);
        }
        if (!isset($data['id'])) {
            $data['id'] = $id;
        }
        $connection = $this->table->getAdapter()->getDriver()->getConnection();
        $connection->beginTransaction();
        $effected = $this->table->update(
            $data,
            ['ID' => $id]
        );
        if (is_callable($inTransaction)) {
            $inTransaction($id);
        }
        $connection->commit();

        $resultSet = $this->table->select(['ID' => $id]);
        $item      = $resultSet->current();
        if ($effected == 0 || $item === false) {
            throw new NotFoundException('Not found', 404);
        }
        try {
            $this->elasticService->copyToElastic($id, true);
        } catch (\Exception $e) {}
        $this->addToElasticJob($id, Elastic::ACTION_UPDATE);
        return $item;
    }

    /**
     * @param string $id
     * @param callable $inTransaction
     * @return bool
     */
    public function delete($id, $inTransaction = null)
    {
        $connection = $this->table->getAdapter()->getDriver()->getConnection();

        $connection->beginTransaction();
        $result = $this->table->delete(['ID' => $id]);
        if (is_callable($inTransaction)) {
            $inTransaction($id);
        }
        $connection->commit();

        if (!$result) {
            return false;
        }
        try {
            $this->elasticService->delete($this->apiIdentity, self::TYPE, [$id]);
        } catch (\Exception $e) {}
        $this->addToElasticJob($id, Elastic::ACTION_DELETE);
        return true;
    }

    public function deleteAll($data = null)
    {
        if (!empty($data)) {
            throw new \Exception(
                'Deleting all with parameters or data in the body is not supported!',
                406
            );
        }
        $this->table->setUseHistory(true);
        $this->table->delete(new Expression('1 = 1'));
        $this->table->setUseHistory(false);
        $this->table->delete(new Expression('1 = 1'));
        return true;
    }

    protected function addFeature(AbstractFeature $feature) {
        $this->table->getFeatureSet()->addFeature($feature);
    }

    protected function whereFromParams($params, $override = null)
    {
        if (!isset($params['q'])) {
            return null;
        }
        $query = $params['q'];
        $where = $this->platformWhere($override === null ? $this->table->getAdapter()->getPlatform()->getName() : $override);
        $c = get_called_class();
        return $where->parseQuery($c::TYPE, $query, $this->fieldTypes);
    }

    protected function platformWhere($platformName)
    {
        switch($platformName) {
            case 'Oracle':
                return new OracleWhere();
            case 'OracleFlat':
                return new OracleFlatWhere();
            default:
                throw new \Exception('Unsupported platform for complex queries!');
        }
    }

    protected function beginTransaction()
    {
        $this->table->getAdapter()->getDriver()->getConnection()->beginTransaction();
    }

    protected function commit()
    {
        $this->table->getAdapter()->getDriver()->getConnection()->commit();
    }

    protected function rollBack()
    {
        $this->table->getAdapter()->getDriver()->getConnection()->rollBack();
    }

    private function addToElasticJob($id, $action) {
        $job = new Elastic();
        $job->setContent([
            'ids' => $id,
            'type' => static::TYPE,
            'source' => $this->apiIdentity,
            'action' => $action
        ]);
        $this->queue->push($job,  ['delay' => 10]);
    }

}