<?php
namespace LajiStore\V1\Rpc\Truncate;

use Zend\Mvc\Controller\AbstractActionController;
use ZF\ContentNegotiation\ViewModel;

class TruncateController extends AbstractActionController
{
    protected $tables = [
        'laji_document',
        'laji_device',
        'laji_individual'
    ];

    public function truncateAction()
    {
        $apiIdentity = $this->getServiceLocator()->get('api-identity');
        $source      = $apiIdentity->getName();
        if (strpos($source, '-testing') === false) {
            throw new \DomainException('Only users with testing in their name are allowed to use this', 400);
        }
        /** @var \ZF\ContentNegotiation\Request $request */
        $request = $this->getRequest();
        $table = $request->getQuery('table', 'all');

        /** @var \Zend\Db\Adapter\Adapter $dbAdapter */
        $dbAdapter = $this->getServiceLocator()->get('laji-db');

        if ($table === 'all') {
            $tables = $this->tables;
        } elseif (in_array($table, $this->tables)) {
            $tables = [$table];
        } else {
            throw new \DomainException("Table '$table' is not known to us", 400);
        }
        $deleted = [];
        foreach($tables as $table) {
            $deleted[] = $table;
            $dbAdapter->query("delete from $table where source = :source", ['source' => $source]);
        }

        return new ViewModel([
            'truncated' => $deleted
        ]);
    }
}
