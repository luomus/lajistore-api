<?php
namespace LajiStore\V1\Rpc\Truncate;

class TruncateControllerFactory
{
    public function __invoke($controllers)
    {
        return new TruncateController();
    }
}
