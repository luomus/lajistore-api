<?php
namespace LajiStore\V1\Rpc\UpdateFilters;

use Luomus\InputFilter\Generator;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class UpdateFiltersController extends AbstractActionController
{
    /** @var Generator */
    private $generator;

    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }


    public function updateFiltersAction()
    {
        $this->generator->generateValidators();
        return new JsonModel(['acknowledged' => true]);
    }
}
