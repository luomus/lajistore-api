<?php
namespace LajiStore\V1\Rpc\UpdateFilters;

use LajiStore\Validator\LajistoreValidatorSpecProvider;
use Luomus\InputFilter\Generator;
use Zend\Mvc\Controller\ControllerManager;

class UpdateFiltersControllerFactory
{
    public function __invoke(ControllerManager $controllers)
    {
        $parentLocator = $controllers->getServiceLocator();
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $parentLocator->get('Triplestore\ObjectManager');
        $om->getCache()->flush();
        $config = $parentLocator->get('Config');
        $generatorConfig = isset($config['luomus']['input_filter']) ? $config['luomus']['input_filter'] : [];
        $metadataService = $om->getMetadataService();
        return new UpdateFiltersController(new Generator(
            $metadataService,
            $generatorConfig,
            new LajistoreValidatorSpecProvider($metadataService)
        ));
    }
}
