<?php
namespace LajiStore\V1\Rpc\Status;

use Zend\Mvc\Controller\AbstractActionController;
use ZF\ContentNegotiation\ViewModel;

class StatusController extends AbstractActionController
{

    /**
     * @var \Zend\Db\Adapter\Adapter
     */
    private $dbAdapter;

    public function __construct(\Zend\Db\Adapter\Adapter $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }


    public function statusAction()
    {
        $status = 'all good, nothing to see here';

        // Check database connection
        try {
            /** @var \Zend\Db\Adapter\Adapter $dbAdapter */
            $results = $this->dbAdapter->query("SELECT id, data, TO_CHAR(edited, 'YYYY-MM-DD HH24:MI') as last FROM LAJI_DOCUMENT ORDER BY edited DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY")->execute();
            $cnt = 0;
            $last = strtotime('2000-01-01 00:00');
            foreach ($results as $result) {
                $cnt++;
                $last = strtotime($result['LAST']);
            }
            if ($cnt === 0) {
                $status = 'Nothing was returned from the database!';
            }
            if ($last < strtotime('-4 hours')) {
                $status = 'Nothing was done in the database in the last 4h!';
            }
        } catch (\Exception $e) {
            $status = 'Database query failed! (SELECT id, data, TO_CHAR(edited, \'YYYY-MM-DD HH24:MI\') as last FROM LAJI_DOCUMENT ORDER BY edited DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY)';
        }

        // Check that background process is running
        exec('ps -auxw | grep "[q]ueue beanstalkd"', $pids);
        if (empty($pids)) {
           $status = 'Background jobs are not running! Please restart supervisord using instructions in the wiki https://wiki.helsinki.fi/pages/viewpage.action?pageId=208085444';
        }

        // Check job queuue process is running
        exec('ps -auxw | grep "[b]in/beanstalkd"', $pids);
        if (empty($pids)) {
            $status = 'Job queue is not up! Please start beanstalkd using instructions in the wiki https://wiki.helsinki.fi/pages/viewpage.action?pageId=208085444';
        }

        return new ViewModel([
            'status' => $status
        ]);
    }
}
