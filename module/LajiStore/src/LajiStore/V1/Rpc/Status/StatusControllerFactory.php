<?php
namespace LajiStore\V1\Rpc\Status;

class StatusControllerFactory
{
    public function __invoke($controllers)
    {
        return new StatusController(
            $controllers->getServiceLocator()->get('laji-db')
        );
    }
}
