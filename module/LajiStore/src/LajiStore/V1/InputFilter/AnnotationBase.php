<?php

namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\Annotation as GeneratedAnnotation;

class AnnotationBase extends GeneratedAnnotation
{

    public function init()
    {
        $this->initLajiMeta();
        parent::init();
    }

}