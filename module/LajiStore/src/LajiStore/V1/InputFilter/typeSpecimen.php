<?php
namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\TypeSpecimen as GeneratedTypeSpecimen;

class TypeSpecimen extends GeneratedTypeSpecimen
{

    public function init() {
        $this->initLajiMeta();
        parent::init();
    }
}