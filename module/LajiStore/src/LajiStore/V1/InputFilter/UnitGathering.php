<?php
namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\UnitGathering as GeneratedInputFilter;

class UnitGathering extends GeneratedInputFilter
{
    public function init() {
        $this->initLajiMeta();
        $this->add([
            'name' => 'geometry',
            'required' => false,
            'validators' => [
                [
                    'name' => 'Luomus\\InputFilter\\Validator\\Geometry',
                ]
            ],
            'inputType' => 'geometry',
            'description' => 'Geological information about gathering'
        ]);
        parent::init();
    }
}