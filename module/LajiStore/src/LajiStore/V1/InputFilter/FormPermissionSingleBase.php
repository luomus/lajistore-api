<?php

namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\FormPermissionSingle as GeneratedFormPermissionSingle;

class FormPermissionSingleBase extends GeneratedFormPermissionSingle
{

    public function init()
    {
        $this->initLajiMeta();
        parent::init();
    }

}