<?php
namespace LajiStore\V1\InputFilter;

use LajiStore\Documentation\DocumentedInputFilter;
use LajiStore\Documentation\DocumentedInputFilterTrait;
use Luomus\InputFilter\BaseInputFilter;

class Fact extends BaseInputFilter implements DocumentedInputFilter
{
    use DocumentedInputFilterTrait;

    public function init() {
        $this->add([
            'name' => 'name',
            'required' => true,
            'inputType' => 'string',
            'description' => 'Name for the fact'
        ]);
        $this->add([
            'name' => 'value',
            'required' => false,
            'inputType' => 'mixed',
            'description' => 'value for the fact'
        ]);
    }
}