<?php

namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\Device as GeneratedDevice;

class DeviceBase extends GeneratedDevice
{

    public function init()
    {
        $this->initLajiMeta();
        parent::init();
    }

}