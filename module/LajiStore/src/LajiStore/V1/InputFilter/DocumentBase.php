<?php

namespace LajiStore\V1\InputFilter;


use Luomus\V1\GeneratedInputFilter\Document as GeneratedDocument;
use Zend\InputFilter\CollectionInputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class DocumentBase extends GeneratedDocument
{
    protected $collections = ['gatherings'];

    public function init() {
        $this->initLajiMeta();

        /** @var ServiceLocatorAwareInterface $parentServiceLocator */
        $parentServiceLocator = $this->getServiceLocator()->getServiceLocator();

        $gatheringInputFilter = $parentServiceLocator
            ->get('InputFilterManager')
            ->get('LajiStore\\V1\\InputFilter\\Gathering');

        $collectionContainerInputFilter = new CollectionInputFilter();
        $collectionContainerInputFilter->setInputFilter($gatheringInputFilter);
        $this->add($collectionContainerInputFilter, 'gatherings');

        /** @var \LajiStore\V1\InputFilter\GatheringEvent $gatheringEvent */
        $gatheringEvent = $parentServiceLocator
            ->get('InputFilterManager')
            ->get('LajiStore\\V1\\InputFilter\\GatheringEvent');

        $this->add($gatheringEvent, 'gatheringEvent');
        parent::init();
    }

}