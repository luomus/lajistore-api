<?php
namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\GatheringEvent as GeneratedGatheringEvent;

class GatheringEvent extends GeneratedGatheringEvent
{
    public function init() {
        $this->initLajiMeta();
        parent::init();
    }
}