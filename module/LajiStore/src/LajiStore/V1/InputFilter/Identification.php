<?php
namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\Identification as GeneratedIdentification;

class Identification extends GeneratedIdentification
{
    public function init()
    {
        $this->initLajiMeta();
        parent::init();
    }
}