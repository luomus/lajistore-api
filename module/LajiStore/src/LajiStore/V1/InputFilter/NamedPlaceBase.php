<?php

namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\NamedPlace as GeneratedNamedPlace;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class NamedPlaceBase extends GeneratedNamedPlace
{

    public function init()
    {
        $this->initLajiMeta();

        /** @var ServiceLocatorAwareInterface $parentServiceLocator */
        $parentServiceLocator = $this->getServiceLocator()->getServiceLocator();

        /** @var \LajiStore\V1\InputFilter\GatheringEvent $prepopulatedDocument */
        $prepopulatedDocument = $parentServiceLocator
            ->get('InputFilterManager')
            ->get('LajiStore\\V1\\InputFilter\\DocumentBase');

        $this->add($prepopulatedDocument, 'prepopulatedDocument');

        parent::init();
    }

}