<?php

namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\News as GeneratedNewsFilter;

class News extends GeneratedNewsFilter
{

    public function init()
    {
        $this->initLajiMeta();
        parent::init();
    }

}