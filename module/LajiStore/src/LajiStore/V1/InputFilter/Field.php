<?php

namespace LajiStore\V1\InputFilter;

use LajiStore\Documentation\DocumentedInputFilter;
use LajiStore\Documentation\DocumentedInputFilterTrait;
use Luomus\V1\GeneratedInputFilter\Field as GeneratedFieldInputFilter;
use Zend\InputFilter\CollectionInputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Field extends GeneratedFieldInputFilter implements DocumentedInputFilter, ServiceLocatorAwareInterface
{
    use DocumentedInputFilterTrait,
        ServiceLocatorAwareTrait;

    protected $collections = ['fields'];

    public function init() {
        parent::init();
    }

    public function setDataWithoutPopulate($data)
    {
        $this->addNestedCollections($data, $this);
        parent::setDataWithoutPopulate($data);
    }

    public function setData($data)
    {
        $this->addNestedCollections($data, $this);
        return parent::setData($data);
    }

    protected function addNestedCollections($data, Field $inputFilter) {
        foreach($this->collections as $collection) {
            if (!isset($data[$collection])) {
                continue;
            }
            $nestedInputFilter = null;
            if ($inputFilter->has($collection)) {
                $nestedInputFilter = $inputFilter->get($collection);
                if (!$nestedInputFilter instanceof CollectionInputFilter) {
                    $inputFilter->remove($collection);
                }
            }
            if (!$nestedInputFilter instanceof CollectionInputFilter) {
                /** @var ServiceLocatorAwareInterface $parentServiceLocator */
                $parentServiceLocator = $this->getServiceLocator()->getServiceLocator();

                $nestedInputFilter = $parentServiceLocator
                    ->get('InputFilterManager')
                    ->get('LajiStore\\V1\\InputFilter\\Field');

                $collectionContainerInputFilter = new CollectionInputFilter();
                $collectionContainerInputFilter->setInputFilter($nestedInputFilter);
                $this->add($collectionContainerInputFilter, $collection);
            }

        }

    }

}