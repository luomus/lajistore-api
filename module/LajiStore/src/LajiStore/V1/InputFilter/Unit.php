<?php
namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\Unit as GeneratedUnit;
use Zend\InputFilter\CollectionInputFilter;

class Unit extends GeneratedUnit
{
    protected $collections = ['identifications', 'typeSpecimens'];

    public function init() {
        $this->initLajiMeta();

        $parentServiceLocator = $this
            ->getServiceLocator()
            ->getServiceLocator();

        $identificationInputFilter = $parentServiceLocator
            ->get('InputFilterManager')
            ->get('LajiStore\\V1\\InputFilter\\Identification');

        $collectionContainerInputFilter = new CollectionInputFilter();
        $collectionContainerInputFilter->setInputFilter($identificationInputFilter);
        $this->add($collectionContainerInputFilter, 'identifications');

        $typeSpecimenInputFilter = $parentServiceLocator
            ->get('InputFilterManager')
            ->get('LajiStore\\V1\\InputFilter\\TypeSpecimen');

        $collectionContainerInputFilter = new CollectionInputFilter();
        $collectionContainerInputFilter->setInputFilter($typeSpecimenInputFilter);
        $this->add($collectionContainerInputFilter, 'typeSpecimens');

        $gatheringEvent = $parentServiceLocator
            ->get('InputFilterManager')
            ->get('LajiStore\\V1\\InputFilter\\UnitGathering');

        $this->add($gatheringEvent, 'unitGathering');

        parent::init();
    }
}