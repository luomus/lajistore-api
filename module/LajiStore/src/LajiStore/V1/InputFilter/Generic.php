<?php
namespace LajiStore\V1\InputFilter;

use LajiStore\Documentation\DocumentedInputFilter;
use LajiStore\Documentation\DocumentedInputFilterTrait;
use Luomus\InputFilter\BaseInputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Validator\NotEmpty;

abstract class Generic extends BaseInputFilter implements ServiceLocatorAwareInterface, DocumentedInputFilter
{
    use ServiceLocatorAwareTrait,
        DocumentedInputFilterTrait,
        LajiMetaTrait;

    protected $required = false;
    protected $valid;
    protected $messages = [
        NotEmpty::IS_EMPTY => 'Object is required and cannot be empty'
    ];

    public function init() {
        $this->initLajiMeta();
    }

    public function isValid($context = null)
    {
        if (empty($this->data)) {
            $this->valid = !$this->isRequired();
            return $this->valid;
        }
        return parent::isValid($context);
    }

    public function getMessages()
    {
        if ($this->valid === false) {
            return $this->messages;
        }
        return parent::getMessages();
    }

    /**
     * @return bool
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @param $required
     * @return $this
     */
    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

}