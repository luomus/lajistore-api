<?php

namespace LajiStore\V1\InputFilter;


trait LajiMetaTrait
{
    protected function initLajiMeta() {
        $this->add([
            'name' => 'id',
            'required' => false,
            'validators' => [
                [
                    'name' => 'Luomus\InputFilter\Validator\Type',
                    'options' => [
                        'expecting' => 'string'
                    ]
                ]
            ],
            'inputType' => 'string',
            'description' => 'Unique ID for the object. (if none given id will be auto generated during insert)'
        ]);

        $this->add([
            'name' => '@context',
            'required' => false,
            'validators' => [
                [
                    'name' => 'Luomus\InputFilter\Validator\Type',
                    'options' => [
                        'expecting' => 'string'
                    ]
                ]
            ],
            'inputType' => 'string',
            'description' => 'Context for the given json'
        ]);

        $this->add([
            'name' => '@type',
            'required' => false,
            'inputType' => 'string',
            'description' => 'This field is automatically populated with the objects type and any user given value in here will be ignored!'
        ]);
    }

}