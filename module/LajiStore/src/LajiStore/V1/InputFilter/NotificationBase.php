<?php

namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\Notification as GeneratedFormPermissionSingle;

class NotificationBase extends GeneratedFormPermissionSingle
{

    public function init()
    {
        $this->initLajiMeta();
        parent::init();
    }

}