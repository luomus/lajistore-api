<?php
namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\Profile as GeneratedProfile;

class Profile extends GeneratedProfile
{

    public function init() {
        $this->initLajiMeta();
        parent::init();
    }
}