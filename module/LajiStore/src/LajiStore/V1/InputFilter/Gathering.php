<?php
namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\Gathering as GeneratedGathering;
use Zend\InputFilter\CollectionInputFilter;

class Gathering extends GeneratedGathering
{
    protected $collections = ['units'];

    public function init() {
        $this->initLajiMeta();

        $unitInputFilter = $this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get('InputFilterManager')
            ->get('LajiStore\\V1\\InputFilter\\Unit');

        $this->add([
            'name' => 'geometry',
            'required' => false,
            'validators' => [
                [
                    'name' =>  'Luomus\\InputFilter\\Validator\\Geometry',
                ]
            ],
            'inputType' => 'geometry',
            'description' => 'Geological information about gathering'
        ]);
        $this->add([
            'name' => 'wgs84Geometry',
            'required' => false,
            'validators' => [
                [
                    'name' =>  'Luomus\\InputFilter\\Validator\\Geometry',
                ]
            ],
            'inputType' => 'geometry',
            'description' => 'Geological information about gathering in wgs84 format'
        ]);

        $collectionContainerInputFilter = new CollectionInputFilter();
        $collectionContainerInputFilter->setInputFilter($unitInputFilter);
        $this->add($collectionContainerInputFilter, 'units');

        parent::init();
    }
}