<?php

namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\DeviceIndividual as GeneratedDeviceIndividual;

class DeviceIndividual extends GeneratedDeviceIndividual
{
    public function init()
    {
        $this->initLajiMeta();
        parent::init();
    }

}