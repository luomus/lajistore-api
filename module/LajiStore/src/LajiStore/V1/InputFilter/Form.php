<?php

namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\Form as GeneratedForm;
use Zend\InputFilter\CollectionInputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Form extends GeneratedForm
{

    protected $collections = ['fields'];

    public function init() {
        $this->initLajiMeta();
        parent::init();
        $this->remove('fields');
        $this->remove('prepopulatedDocument');
        /** @var ServiceLocatorAwareInterface $parentServiceLocator */
        $parentServiceLocator = $this->getServiceLocator()->getServiceLocator();

        $gatheringInputFilter = $parentServiceLocator
            ->get('InputFilterManager')
            ->get('LajiStore\\V1\\InputFilter\\Field');

        $collectionContainerInputFilter = new CollectionInputFilter();
        $collectionContainerInputFilter->setInputFilter($gatheringInputFilter);
        $this->add($collectionContainerInputFilter, 'fields');


        /** @var \LajiStore\V1\InputFilter\GatheringEvent $prepopulatedDocument */
        $prepopulatedDocument = $parentServiceLocator
            ->get('InputFilterManager')
            ->get('LajiStore\\V1\\InputFilter\\DocumentBase');
        $this->add($prepopulatedDocument, 'prepopulatedDocument');
    }
}