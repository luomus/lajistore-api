<?php

namespace LajiStore\V1\InputFilter;

use Luomus\V1\GeneratedInputFilter\Individual as GeneratedIndividual;

class Individual extends GeneratedIndividual
{
    public function init() {
        $this->initLajiMeta();
        parent::init();
    }
}