<?php

namespace LajiStore\Job;

use LajiStore\TableGateway\TableGateway;
use SlmQueue\Job\AbstractJob;
use SlmQueue\Queue\QueueAwareInterface;
use SlmQueue\Queue\QueueAwareTrait;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Http\Client;
use Zend\Http\Response;
use Zend\Log\Logger;
use Zend\View\Helper\Url;

abstract class WarehouseAbstract extends AbstractJob implements QueueAwareInterface
{
    use QueueAwareTrait;

    protected static $tokens;
    const MAX_RETRY = 100;
    const MAX_FORMS_TTL = 600;
    const ACTION_ADD = 'add';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * @var TableGateway
     */
    protected $documentTable;
    /**
     * @var array
     */
    protected $config;
    /**
     * @var Url
     */
    protected $urlHelper;
    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var array
     */
    protected $sources;

    /**
     * @var AdapterInterface
     */
    protected $lajiApi;

    public function __construct(
        TableGateway $documentTable = null,
        Url $urlHelper = null,
        $config = null,
        Logger $logger = null,
        $sources = null,
        AdapterInterface $lajiApi = null
    )
    {
        $this->config = $config;
        $this->logger = $logger;
        $this->sources = $sources;
        $this->urlHelper = $urlHelper;
        $this->documentTable = $documentTable;
        $this->lajiApi = $lajiApi;
    }

    public function execute()
    {
        $payload = $this->getContent();
        $ids = $payload['ids'];
        $action = isset($payload['action']) ? $payload['action'] : self::ACTION_ADD;
        $this->logger->crit('Starting to job', $payload);
        switch($action) {
            case self::ACTION_DELETE:
                $this->doDelete($ids, $payload);
                break;
            case self::ACTION_ADD:
            case self::ACTION_UPDATE:
                $this->doPost($ids, $payload);
        }
    }

    abstract protected function doDelete($ids, $payload);

    abstract protected function doPost($ids, $payload);

    protected function analyzeWarehouseResponse(Response $response, $payload) {
        if ($response->isOk()) {
            $this->logger->notice('warehouse success', $payload);
        } else {
            $this->logger->crit('warehouse error', [
                'response' => $response->getBody(),
                'code' => $response->getStatusCode()
            ]);
            if (!isset($payload['retry']) || $payload['retry'] < self::MAX_RETRY) {
                $payload['retry'] = isset($payload['retry']) ? $payload['retry'] + 1 : 0;
                $className = get_called_class();
                $warehouseJob = new $className();
                if ($warehouseJob instanceof AbstractJob) {
                    $warehouseJob->setContent($payload);
                    $this->getQueue()->push($warehouseJob, ['delay' => $payload['retry'] * $payload['retry']]);
                } else {
                    $this->logger->crit('Could not re create the job!', ['payload' => json_encode($payload)]);
                }
            } else {
                $this->logger->crit('Failed to send data to warehouse! ', ['payload' => json_encode($payload)]);
            }
        }
    }

    protected function openData(&$document) {
        if (is_a($document['DATA'], 'OCI-Lob')) {
            $document['DATA'] = json_decode($document['DATA']->load(), true);
        }
    }

    protected function getApiClient($sourceId = null) {
        $tokens = $this->getAllTokens();
        if ($sourceId === null || !isset($tokens[$sourceId])) {
            if ($sourceId !== null) {
                $this->logger->debug('Using default source in api.laji.fi for ' . $sourceId);
                $sourceId = $this->config['api_laji_fi']['defaultSystem'];
            } else {
                $this->logger->debug('Token refresh since no token for ' . $sourceId);
                $tokens = $this->getAllTokens(true);
                if (!isset($tokens[$sourceId])) {
                    $this->logger->debug('No token found for ' . $sourceId . '. Falling back to default');
                    $sourceId = $this->config['api_laji_fi']['defaultSystem'];
                }
            }
        }
        if (!isset($tokens[$sourceId])) {
            $this->logger->crit('Could not find api.laji.fi token for ' . $sourceId);
        }
        $client = new Client(
            $this->config['api_laji_fi']['url'] . $this->config['api_laji_fi']['endpoints']['warehousePush'],
            isset($this->config['api_laji_fi']['httpOptions']) ? $this->config['api_laji_fi']['httpOptions'] : null
        );
        $apiHeaders = [
            'Authorization' => $tokens[$sourceId],
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];
        $client->setHeaders($apiHeaders);
        return $client;
    }

    protected function getFormClient() {
        $tokens = $this->getAllTokens();
        $client = new Client(
            $this->config['api_laji_fi']['url'] . $this->config['api_laji_fi']['endpoints']['allForms'],
            isset($this->config['api_laji_fi']['httpOptions']) ? $this->config['api_laji_fi']['httpOptions'] : null
        );
        $apiHeaders = [
            'Authorization' => $tokens[$this->config['api_laji_fi']['defaultSystem']],
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];
        $client->setHeaders($apiHeaders);
        return $client;
    }

    protected function getAllTokens($refresh = false) {
        if ($refresh || !isset(self::$tokens)) {
            $conn = $this->lajiApi->getDriver()->getConnection();
            $results = $conn->execute('SELECT A.SYSTEMID, B.ID as TOKEN from APIUSER A
                                LEFT JOIN ACCESSTOKEN B on A.ID = B.USERID
                                WHERE A.SYSTEMID is not null');
            $tokens = [];
            foreach ($results as $result) {
                $tokens[$result['SYSTEMID']] = $result['TOKEN'];
            }
            self::$tokens = $tokens;
        }
        return self::$tokens;
    }

}