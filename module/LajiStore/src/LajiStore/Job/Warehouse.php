<?php
namespace LajiStore\Job;

use Common\Service\IdService;
use LajiStore\Db\Sql\AbstractWhere;
use LajiStore\Db\Sql\OracleWhere;
use LajiStore\TableGateway\TableGateway;
use LajiStore\V1\Rest\Device\DeviceMapper;
use LajiStore\V1\Rest\DeviceIndividual\DeviceIndividualMapper;
use LajiStore\V1\Rest\Document\DocumentMapper;
use LajiStore\V1\Rest\Individual\IndividualMapper;
use SlmQueue\Queue\QueueAwareTrait;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Log\Logger;
use Zend\View\Helper\Url;

class Warehouse extends WarehouseAbstract
{
    use QueueAwareTrait;

    const RECORD_BASIS = 'recordBasis';
    const DEFAULT_RECORD_BASIS = 'MY.recordBasisHumanObservation';

    private static $forms;
    private static $formsTtl;

    /**
     * @var AdapterInterface
     */
    protected $lajiApi;

    /**
     * @var TableGateway
     */
    protected $documentHistoryTable;

    public function __construct(
        TableGateway $documentTable = null,
        TableGateway $documentHistoryTable = null,
        Url $urlHelper = null,
        $config = null,
        Logger $logger = null,
        $sources = null,
        AdapterInterface $lajiApi = null
    )
    {
        parent::__construct($documentTable, $urlHelper, $config, $logger, $sources, $lajiApi);
        $this->documentHistoryTable = $documentHistoryTable;
    }

    private function shouldSend($row, $payload) {
        if (isset($row['DATA'])) {
            if (isset($row['DATA']['publicityRestrictions']) &&
                $row['DATA']['publicityRestrictions'] === 'MZ.publicityRestrictionsPrivate') {
                $payload['action'] = self::ACTION_DELETE;
                $this->doDelete($row['ID'], $payload);
                return false;
            }
            if (isset($row['DATA']['isTemplate']) &&
                ($row['DATA']['isTemplate'] === 'true' || $row['DATA']['isTemplate'] === true)) {
                return false;
            }
        }
        return true;
    }

    private function getSourceId($row) {
        if (isset($row['DATA']) && isset($row['DATA']['sourceID'])) {
            $systemID = $row['DATA']['sourceID'];
        } else {
            $systemID = isset($row['SOURCE']) ? $row['SOURCE'] : $this->config['api_laji_fi']['defaultSystem'];
        }
        return $systemID;
    }

    protected function doPost($ids, $payload) {
        $rowSet = $this->sources ?
            $this->documentTable->select(['ID' => $ids, 'TYPE' => DocumentMapper::TYPE, 'SOURCE' => $this->sources]) :
            $this->documentTable->select(['ID' => $ids, 'TYPE' => DocumentMapper::TYPE]);
        $results = [];
        foreach($rowSet as $row) {
            $this->openData($row);
            if (!$this->shouldSend($row, $payload)) {
                continue;
            }
            $sourceId = $this->getSourceId($row);
            $results[$sourceId][] = $this->prepareDocumentPackage($row);
        }
        foreach ($results as $systemID => $data) {
            if (!empty($data)) {
                $client = $this->getApiClient($systemID);
                $client->setMethod('POST');
                $client->setRawBody(json_encode([
                    'schema' => 'lajistore',
                    'roots' => $data
                ]));
                $this->analyzeWarehouseResponse($client->send(), $payload);
            }
        }
    }

    protected function doDelete($ids, $payload) {
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        foreach($ids as $id) {
            $select = new \Zend\Db\Sql\Select();
            $select->from($this->documentHistoryTable->table);
            $select->where($this->sources ?
                ['ID' => $id, 'TYPE' => DocumentMapper::TYPE, 'SOURCE' => $this->sources] :
                ['ID' => $id, 'TYPE' => DocumentMapper::TYPE]);
            $select->order('VERSION DESC');
            $select->limit(1);
            $rowSet = $this->documentHistoryTable->selectWith($select);
            foreach ($rowSet as $row) {
                $this->openData($row);
                $client = $this->getApiClient($this->getSourceId($row));
                $client->setMethod('DELETE');
                $payload['ids'] = $id;
                $this->analyzeWarehouseResponse($client->setParameterGet([
                    'documentId' => IdService::getUri($id)
                ])->send(), $payload);
            }
        }
    }

    private function prepareDocumentPackage($document) {
        $document = $document['DATA'];
        $options = isset($document['formID']) ? $this->getFormOptions($document['formID']) : [];
        $this->prepareDocument(
            $document,
            (isset($options['prepopulateWithInformalTaxonGroups']) && $options['prepopulateWithInformalTaxonGroups']) ||
            (isset($options['emptyOnNoCount']) && $options['emptyOnNoCount'])
        );
        $package = ['document' => $document];
        if (isset($document['deviceID'])) {
            $package['devices'] = $this->getDevice($document['deviceID']);
            $package['deviceIndividuals'] = $this->getIndividualsByDevices($package['devices']);
            $package['individuals'] = $this->getIndividualsByDeviceIndividuals($package['deviceIndividuals']);
        }
        return $package;
    }

    private function prepareDocument(&$data, $removeUnitIfNoCount) {
        if (isset($data['gatherings'])) {
            foreach ($data['gatherings'] as $gKey => &$gathering) {
                if (isset($gathering['units'])) {
                    $remove = [];
                    foreach ($gathering['units'] as $key => &$unit) {
                        $identification = isset($unit['identifications']) && isset($unit['identifications'][0]) ?
                            $unit['identifications'][0] : [];
                        if ((
                                $removeUnitIfNoCount &&
                                $this->isEmpty($unit, 'count') &&
                                $this->isEmpty($unit, 'individualCount') &&
                                $this->isEmpty($unit, 'pairCount') &&
                                $this->isEmpty($unit, 'abundanceString') &&
                                $this->isEmpty($unit, 'maleIndividualCount') &&
                                $this->isEmpty($unit, 'femaleIndividualCount') &&
                                $this->isEmpty($unit, 'areaInSquareMeters') &&
                                $this->isEmpty($unit, 'images')
                            ) ||
                            (
                                $this->isEmpty($unit, 'informalNameString') &&
                                $this->isEmpty($identification, 'taxon') &&
                                $this->isEmpty($identification, 'taxonID') &&
                                $this->isEmpty($identification, 'taxonVerbatim') &&
                                $this->isEmpty($unit, 'informalTaxonGroups') &&
                                $this->isEmpty($unit, 'images')
                            )
                        ) {
                            $remove[] = $key;
                        } else if (!isset($unit[self::RECORD_BASIS]) || empty($unit[self::RECORD_BASIS])){
                            $unit[self::RECORD_BASIS] = self::DEFAULT_RECORD_BASIS;
                        }
                    }
                    $gathering['units'] = $this->removeByKeys($gathering['units'], $remove);
                }
            }
        }
    }

    private function removeByKeys($data, $removeKeys) {
        $remCnt = count($removeKeys);
        if ($remCnt > 0) {
            for($i = $remCnt - 1; $i >= 0; $i--) {
                array_splice($data, $removeKeys[$i], 1);
            }
        }
        return $data;
    }

    private function isEmpty($obj, $key) {
        return !isset($obj[$key]) || (empty($obj[$key]) && $obj[$key] !== 0 && $obj[$key] !== '0');
    }

    private function getDevice($deviceID) {
        $rowSet = $this->documentTable->select(['ID' => $deviceID, 'TYPE' => DeviceMapper::TYPE]);
        $devices = [];
        foreach($rowSet as $row) {
            $this->openData($row);
            $devices[] = $row['DATA'];
        }
        return $devices;
    }

    private function getIndividualsByDevices($devices) {
        $deviceIndividuals = [];
        foreach($devices as $device) {
            $where = new OracleWhere();
            $where->equalTo('TYPE', DeviceIndividualMapper::TYPE);
            $where->selectByValue(DeviceIndividualMapper::TYPE, 'deviceID', $device['id'], PredicateSet::COMBINED_BY_AND, AbstractWhere::TYPE_TEXT);
            $where->selectByValue(DeviceIndividualMapper::TYPE, 'deviceID', $this->getUri($device['id'], DeviceMapper::TYPE), PredicateSet::COMBINED_BY_OR, AbstractWhere::TYPE_TEXT);

            $rowSet = $this->documentTable->select($where);
            foreach($rowSet as $row) {
                $this->openData($row);
                $deviceIndividuals[] = $row['DATA'];
            }
        }
        return $deviceIndividuals;
    }

    private function getIndividualsByDeviceIndividuals($deviceIndividuals) {
        $individuals = [];
        foreach($deviceIndividuals as $deviceIndividual) {
            $individualsRowSet = $this->documentTable->select([
                'ID' => $this->getIdFromUri($deviceIndividual['individualID']),
                'TYPE' => IndividualMapper::TYPE
            ]);
            foreach($individualsRowSet as $individual) {
                $this->openData($individual);
                $individuals[] = $individual['DATA'];
            }
        }
        return $individuals;
    }

    private function getUri($id, $type) {
        return $this->config['host'] . $this->urlHelper->__invoke('laji-store.rest.' . $type). '/' . $id;
    }

    private function getIdFromUri($uri) {
        $parts = explode('/', $uri);
        return array_pop($parts);
    }

    private function getFormOptions($formId) {
        $forms = $this->getAllForms();
        if (isset($forms[$formId]) && isset($forms[$formId]['options'])) {
            return $forms[$formId]['options'];
        }
        return [];
    }

    private function getAllForms() {
        $date = date_create();
        $now  = date_timestamp_get($date);
        if (isset(self::$forms) && isset(self::$formsTtl) && $now < self::$formsTtl) {
            return self::$forms;
        }
        $response = $this->getFormClient()->send();
        if (!$response->isOk()) {
            if (isset(self::$forms)) {
                self::$formsTtl = $now + self::MAX_FORMS_TTL;
                return self::$forms;
            } else {
                $this->logger->warn('Unable to fetch forms!!!');
                return [];
            }
        }
        $data = json_decode($response->getBody(), true);
        $response = [];
        if (isset($data['results']) && count($data['results']) > 0) {
            foreach ($data['results'] as $form) {
                $response[$form['id']] = $form;
            }
        } else if (isset(self::$forms)) {
            return self::$forms;
        }
        self::$formsTtl = $now + self::MAX_FORMS_TTL;
        self::$forms = $response;
        return self::$forms;
    }
}