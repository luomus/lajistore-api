<?php
namespace LajiStore\Job;

use LajiStore\V1\Rest\Annotation\AnnotationMapper;
use SlmQueue\Queue\QueueAwareTrait;

class WarehouseAnnotation extends WarehouseAbstract
{
    use QueueAwareTrait;

    protected function doDelete($ids, $payload) {
        $results = [];
        $client = $this->getApiClient();
        foreach ($ids as $idx => $id) {
            if (isset($payload['roots']) && isset($payload['roots'][$idx])) {
                $results[] = $this->prepareEmptyAnnotationPackage($id, $payload['roots'][$idx]);
            } else {
                $this->logger->err('No rootID in annotation job: ' . $id, $payload);
            }
        }
        if (!empty($results)) {
            $client->setMethod('POST');
            $client->setRawBody(json_encode([
                'schema' => 'lajistore',
                'roots' => $results
            ]));
            $this->analyzeWarehouseResponse($client->send(), $payload);
        }
    }

    protected function doPost($ids, $payload) {
        $rowSet = $this->documentTable->select(['ID' => $ids, 'TYPE' => AnnotationMapper::TYPE]);
        $results = [];
        $client = $this->getApiClient();
        foreach($rowSet as $row) {
            $this->openData($row);
            $results[] = $this->prepareAnnotationPackage($row);
        }
        if (!empty($results)) {
            $client->setMethod('POST');
            $client->setRawBody(json_encode([
                'schema' => 'lajistore',
                'roots' => $results
            ]));
            $this->analyzeWarehouseResponse($client->send(), $payload);
        }
    }

    private function prepareAnnotationPackage($annotation) {
        $annotation = $annotation['DATA'];
        $package = ['annotation' => $annotation];
        return $package;
    }

    private function prepareEmptyAnnotationPackage($annotationId, $root) {
        $package = ['annotation' => ['id' => $annotationId, 'rootID' => $root]];
        return $package;
    }

}