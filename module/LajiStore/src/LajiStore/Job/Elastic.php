<?php
namespace LajiStore\Job;

use LajiStore\Service\ElasticService;
use SlmQueue\Job\AbstractJob;

class Elastic extends AbstractJob
{
    const ACTION_ADD = 'add';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * @var ElasticService
     */
    private $elasticService;

    public function __construct(ElasticService $elasticService = null)
    {
        $this->elasticService = $elasticService;
    }

    /**
     * Execute the job
     *
     * @return void
     */
    public function execute()
    {
        $payload = $this->getContent();
        $action = isset($payload['action']) ? $payload['action'] : self::ACTION_ADD;
        switch($action) {
            case self::ACTION_DELETE:
                if ($this->elasticService->hasDocument($payload['source'], $payload['type'], $payload['ids'])) {
                    $this->elasticService->delete($payload['source'], $payload['type'], $payload['ids']);
                }
                break;
            case self::ACTION_ADD:
            case self::ACTION_UPDATE:
                if (!$this->elasticService->hasDocument($payload['source'], $payload['type'], $payload['ids'])) {
                    $this->elasticService->copyToElastic($payload['ids'], true);
                }
        }
    }
}