<?php
namespace LajiStore\Job;

use LajiStore\Service\ElasticService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ElasticFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $sl)
    {
        $container = $sl->getServiceLocator();
        return new Elastic(
            $container->get(ElasticService::class)
        );
    }

} 