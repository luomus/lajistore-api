<?php
namespace LajiStore\Job;

use LajiStore\TableGateway\TableGateway;
use Zend\Http\Client;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class WarehouseAnnotationFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $sl)
    {
        $container = $sl->getServiceLocator();
        $config = $container->get('Config');
        if (!isset($config['api_laji_fi'])) {
            throw new \Exception('Missing api_laji_fi configurations!!!');
        }
        if (!isset($config['api_laji_fi']['endpoints']) || !isset($config['api_laji_fi']['endpoints']['warehousePush'])) {
            throw new \Exception('Missing warehouse push endpoint!!!');
        }
        return new WarehouseAnnotation(
            new TableGateway(
                'LAJI_DOCUMENT',
                $container->get('laji-db')
            ),
            $container->get('ViewHelperManager')->get('Url'),
            $config,
            $container->get('logger'),
            null,
            $container->get('laji-api')
        );
    }

} 