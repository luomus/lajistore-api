<?php
namespace LajiStore;

use Zend\Console\Adapter\AdapterInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
use Zend\ModuleManager\Feature;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\ResponseSender\SendResponseEvent;
use ZF\Apigility\Provider\ApigilityProviderInterface;

class Module implements ApigilityProviderInterface,
    Feature\ConfigProviderInterface,
    Feature\ConsoleBannerProviderInterface,
    Feature\ConsoleUsageProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        $serviceManager = $e->getApplication()->getServiceManager();
        /** @var Adapter $adapter */
        $adapter = $serviceManager->get('laji-db');
        GlobalAdapterFeature::setStaticAdapter($adapter);

        $eventManager         = $e->getApplication()->getEventManager();
        $moduleRouteListener  = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $sendResponseListener = $serviceManager->get('SendResponseListener');
        $sendResponseListener->getEventManager()->attach(
            SendResponseEvent::EVENT_SEND_RESPONSE,
            $serviceManager->get('LajiStore\ApiHistory\Listener\SendApiHistoryResponseListener'),
            -600
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getConsoleBanner(AdapterInterface $console)
    {
        return 'LajiStore';
    }

    /**
     * {@inheritDoc}
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'reset-named-place-coordinates [--dry-run] <source> <id> [<version>]' => 'Reset named places coordinates',
            array('<source>', 'Source id for the system'),
            array('<id>', 'Named place id'),
            array('<version>', 'Version number from where to pick the coordinates from.'),
            array('--dry-run', 'Only show the resulting json and doesn\'t save anything'),
            'generate-filters' => 'Generate filters from triplestore',
            'copy-to-elastic [<id>]' => 'Make a secondary copy of the data to elasticsearch',
            array('<id>', 'Id of the item to be copied to search index'),
            'check-elastic-content' => 'Checks that everything is copied to elasticsearch',
            'send-annotation-to-warehouse [<id>]' => 'Send annotation to the warehouse',
            array('<id>', 'Annotation id or a list of ids separated by comma. If omitted will send all the annotations.'),
            'send-to-warehouse [<id>]' => 'Send document to the warehouse',
            array('<id>', 'Document id or a list of ids separated by comma. If omitted will send all the documents.'),
            'delete-from-warehouse <id>' => 'Delete document from warehouse',
            array('<id>', 'Document id or a list of ids separated by comma that will be deleted.'),
            'rebuild document search <source>' => 'Rebuilds the document search table for the given source',
            array('<source>', 'Source id for the system'),
            'rebuild namedPlace search ' => 'Rebuilds the named place search table for the given source',
            array('<source>', 'Source id for the system'),
            'link user <source> <target> [--dry-run]' => 'Links user id to MA id',
            array('<source>', 'The original id that the user had'),
            array('<target>', 'The MA person id for the users'),
            array('--dry-run', 'Only show the resulting json and doesn\'t save anything'),
        );
    }
}
