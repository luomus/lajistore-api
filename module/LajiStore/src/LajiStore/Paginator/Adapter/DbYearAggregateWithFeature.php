<?php

namespace LajiStore\Paginator\Adapter;

use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\Feature\EventFeature;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;

class DbYearAggregateWithFeature extends DbSelect
{

    protected $featureSet;

    /**
     * DbYearAggregateWithFeature constructor.
     * @param TableGateway $mainTable
     * @param TableGateway $searchTable
     * @param null|Where|\Closure|string|array $where
     * @param null $order
     * @param null $group
     * @param null $having
     * @throws \Exception
     */
    public function __construct(TableGateway $mainTable, TableGateway $searchTable, $where = null, $order = null, $group = null, $having = null)
    {
        $searchSql = $searchTable->getSql();
        $searchSelect = $searchSql->select();

        if ($where) {
            $searchSelect->where($where);
        }

        if ($group) {
            throw new \Exception('No handler for having by query');
        }
        if ($having) {
            throw new \Exception('No handler for having by query');
        }

        $this->featureSet   = $searchTable->getFeatureSet();
        $resultSetPrototype = $mainTable->getResultSetPrototype();

        $searchSelect->columns(['ID']);
        // echo $searchSelect->getSqlString($mainTable->getAdapter()->getPlatform()); exit();


        $this->featureSet->apply(EventFeature::EVENT_PRE_SELECT, array($searchSelect));

        $where = new Where();
        // When YEARSTART is coalesced yearstart limit will not be needed.
        $where->expression('IDX = 0 AND ID IN ?', [$searchSelect]);

        $wrapperSql = $searchTable->getSql();
        $wrapperSelect = $wrapperSql->select()
            ->columns([
                'id' => new Expression('case when YEARSTART is not null then TO_CHAR(YEARSTART) else \'null\' end'),
                'count' => new Expression('count(*)')
            ])
            ->group(['YEARSTART'])
            ->order(['YEARSTART'])
            ->where($where);

        // apply preSelect features
        $this->featureSet->apply(EventFeature::EVENT_PRE_SELECT, array($wrapperSelect));

        // echo $wrapperSelect->getSqlString($mainTable->getAdapter()->getPlatform()); exit();

        parent::__construct($wrapperSelect, $wrapperSql, $resultSetPrototype);
    }

    /**
     * Returns an array of items for a page.
     *
     * @param  int $offset           Page offset
     * @param  int $itemCountPerPage Number of items per page
     * @return array
     */
    public function getItems($offset, $itemCountPerPage)
    {
        $select = clone $this->select;

        $select->offset($offset);
        $select->limit($itemCountPerPage);

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        // apply postSelect features
        $this->featureSet->apply(EventFeature::EVENT_POST_SELECT, array($statement, $result, $resultSet));

        return iterator_to_array($resultSet);
    }

}