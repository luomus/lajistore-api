<?php

namespace LajiStore\Paginator\Adapter;

use Zend\Db\Sql\Having;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\Feature\EventFeature;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;

class DbTableGatewayWithFeature extends DbSelect
{

    protected $featureSet;

    /**
     * Constructs instance.
     *
     * @param TableGateway                      $tableGateway
     * @param null|Where|\Closure|string|array  $where
     * @param null|string|array                 $order
     * @param null|string|array                 $group
     * @param null|Having|\Closure|string|array $having
     */
    public function __construct(TableGateway $tableGateway, $where = null, $order = null, $group = null, $having = null)
    {
        $sql    = $tableGateway->getSql();
        $select = $sql->select();
        if ($where) {
            $select->where($where);
        }
        if ($order) {
            $select->order($order);
        }
        if ($group) {
            $select->group($group);
        }
        if ($having) {
            $select->having($having);
        }
        //echo $select->getSqlString($tableGateway->getAdapter()->getPlatform());exit();
        $this->featureSet = $tableGateway->getFeatureSet();
        $resultSetPrototype = $tableGateway->getResultSetPrototype();

        // apply preSelect features
        $this->featureSet->apply(EventFeature::EVENT_PRE_SELECT, array($select));
        //echo $select->getSqlString($tableGateway->getAdapter()->getPlatform());exit();

        parent::__construct($select, $sql, $resultSetPrototype);
    }

    /**
     * Returns an array of items for a page.
     *
     * @param  int $offset           Page offset
     * @param  int $itemCountPerPage Number of items per page
     * @return array
     */
    public function getItems($offset, $itemCountPerPage)
    {
        $select = clone $this->select;

        $select->offset($offset);
        $select->limit($itemCountPerPage);

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        // apply postSelect features
        $this->featureSet->apply(EventFeature::EVENT_POST_SELECT, array($statement, $result, $resultSet));

        return iterator_to_array($resultSet);
    }

}