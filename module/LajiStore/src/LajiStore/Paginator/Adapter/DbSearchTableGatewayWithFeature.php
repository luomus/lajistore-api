<?php

namespace LajiStore\Paginator\Adapter;

use Zend\Db\Sql\Having;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\Feature\EventFeature;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;

class DbSearchTableGatewayWithFeature extends DbSelect
{

    protected $featureSet;

    /**
     * DbSearchTableGatewayWithFeature constructor.
     * @param TableGateway $mainTable
     * @param TableGateway $searchTable
     * @param null $where
     * @param null $order
     * @param null $group
     * @param null $having
     * @throws \Exception
     */
    public function __construct(TableGateway $mainTable, TableGateway $searchTable, $where = null, $order = null, $group = null, $having = null)
    {
        $mainSql = $mainTable->getSql();
        $select = $mainSql->select();
        if ($order) {
            $select->order($order);
        }
        if ($group) {
            throw new \Exception('No handler for group by query');
        }
        if ($having) {
            throw new \Exception('No handler for having by query');
        }

        $searchSql = $searchTable->getSql();
        $searchSelect = $searchSql->select();

        if ($where) {
            $searchSelect->columns(['ID']);
            $searchSelect->where($where);

            $where = new Where();
            $where->expression('ID in (?)', [$searchSelect]);
            $select->where($where);
        }

        //echo $select->getSqlString($tableGateway->getAdapter()->getPlatform());exit();
        $this->featureSet = $mainTable->getFeatureSet();
        $resultSetPrototype = $mainTable->getResultSetPrototype();

        // apply preSelect features
        $this->featureSet->apply(EventFeature::EVENT_PRE_SELECT, array($select));
        //echo $select->getSqlString($mainTable->getAdapter()->getPlatform());exit();

        parent::__construct($select, $mainSql, $resultSetPrototype);
    }

    /**
     * Returns an array of items for a page.
     *
     * @param  int $offset           Page offset
     * @param  int $itemCountPerPage Number of items per page
     * @return array
     */
    public function getItems($offset, $itemCountPerPage)
    {
        $select = clone $this->select;

        $select->offset($offset);
        $select->limit($itemCountPerPage);

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        // apply postSelect features
        $this->featureSet->apply(EventFeature::EVENT_POST_SELECT, array($statement, $result, $resultSet));

        return iterator_to_array($resultSet);
    }

}