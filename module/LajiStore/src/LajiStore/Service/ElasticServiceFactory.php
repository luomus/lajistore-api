<?php


namespace LajiStore\Service;

use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ElasticServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new ElasticService(
            new TableGateway(
                'LAJI_DOCUMENT',
                $serviceLocator->get('laji-db')
            ),
            $serviceLocator->get('Elastic\Client\Index'),
            $serviceLocator->get('Elastic\Client\Search'),
            $serviceLocator->get('logger')
        );
    }
}