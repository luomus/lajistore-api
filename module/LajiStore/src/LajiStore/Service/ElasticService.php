<?php


namespace LajiStore\Service;

use Elastic\Client\Index;
use Elastic\Client\Search;
use Elastic\Query\Query;
use Exception;
use LajiStore\Db\Driver\Oci8\MetaResult;
use LajiStore\ElasticExtract\GenericExtract;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Log\Logger;

class ElasticService
{
    const MAX_SIZE = 500;

    /**
     * @var TableGateway
     */
    private $table;
    /**
     * @var Index
     */
    private $index;
    /**
     * @var Search
     */
    private $search;
    /**
     * @var Logger
     */
    private $logger;
    private $pid;

    private $extractors = [];

    public function __construct(TableGateway $table, Index $index, Search $search, Logger $logger)
    {
        $this->pid = getmypid();
        $this->table = $table;
        $this->index = $index;
        $this->logger = $logger;
        $this->search = $search;
    }

    public function copyToElastic($ids = null, $greedy = false)
    {
        $splitter = ':::';
        $rowSet = $this->getItemsToSend($ids, $greedy);
        $send = [];
        $cnt = 0;
        MetaResult::$disable = true;
        foreach ($rowSet as $row) {
            if (!isset($row['SOURCE'])) {
                $this->logger->crit('Missing source params from', $row);
                continue;
            }
            $type = $row['SOURCE'] . $splitter . $row['TYPE'];
            if (is_a($row['DATA'], 'OCI-Lob')) {
                $row['DATA'] = $row['DATA']->load();
            }
            $data = json_decode($row['DATA'], true);
            if (!isset($send[$type])) {
                $send[$type] = [];
            }
            array_push($send[$type], ['ID' => $row['ID'], 'SOURCE' => $row['SOURCE'], 'DATA' => $data]);
            $cnt++;
        }
        MetaResult::$disable = false;

        $indexes = [];
        foreach ($send as $type => $rows) {
            $parts = explode($splitter, $type);
            $extractor = $this->getExtractor($parts[0], $parts[1]);
            try {
                $this->index->indexResults(array_map(function($row) { return $row['DATA']; }, $rows), $extractor, ElasticService::MAX_SIZE);
                $this->markAsSend($rows);
                $indexes[] = $extractor->getIndex();
            } catch (Exception $e) {
                $this->logger->crit('ES Failed to index data', [$e->getCode(), $e->getMessage(), $e->getTraceAsString()]);
                $this->freeLock(array_map(function($row) { return $row['ID']; }, $rows));
            }
        }

        if (count($indexes) > 0) {
            $this->index->getClient()->indices()->refresh(['index' => implode(',', $indexes)]);
        }
        return $cnt;
    }

    public function hasDocument($source, $type, $id)
    {
        $extractor = $this->getExtractor($source, $type);
        $query = new Query($extractor->getIndex(), $extractor::getTypeName());
        $queryStr = 'id: ("' . $id . '")';
        $query->setQueryString($queryStr);
        $query->setSize(0);
        try {
            return $this->search->query($query)->execute()->count() > 0;
        } catch (Exception $e) {
            $this->logger->crit('ES Failed to check if document exists', [$e->getCode(), $e->getMessage(), $e->getTraceAsString()]);
            return false;
        }
    }

    public function delete($source, $type, $ids) {
        if (empty($ids)) {
            return;
        }
        if (!is_array($ids)) {
            $ids = [$ids];
        }
        $extractor = $this->getExtractor($source, $type);
        $index = $extractor->getIndex();

        $delete = new Query($index, $extractor::getTypeName());
        $queryStr = 'id: ("' . implode('" OR "', $ids) . '")';
        $delete->setQueryString($queryStr);
        $this->index->deleteByQuery($delete);
        $this->index->getClient()->indices()->refresh(['index' => $index]);
    }

    private function getExtractor($source, $type)
    {
        $source = mb_strtolower(str_replace('.', '', $source));
        $type = mb_strtolower($type);
        $index = $source . '_' . $type;
        if (!isset($this->extractors[$index])) {
            $this->extractors[$index] = new GenericExtract($source, $type);
        }
        return $this->extractors[$index];
    }

    private function markAsSend($rowSet)
    {
        foreach ($rowSet as $row) {
            $this->table->update(['PID' => null, 'ADMIN' => 1, 'IN_ES' => 1], ['ID' => $row['ID'], 'SOURCE' => $row['SOURCE']]);
        }
    }

    private function freeLock($ids) {
        $this->table->update(['PID' => null, 'ADMIN' => 1, 'IN_ES' => 0], ['ID' => $ids, 'PID' => $this->pid]);
    }


    private function getItemsToSend($ids, $greedy)
    {
        if ($ids) {
            $this->table->update(['PID' => $this->pid, 'ADMIN' => 1], $greedy ? ['ID' => $ids] : ['ID' => $ids, new Expression('PID is null')]);
        } else {
            $this->table->update(['PID' => $this->pid, 'ADMIN' => 1], ['IN_ES' => 0, new Expression('PID is null'), new Expression('ROWNUM <= ' . ElasticService::MAX_SIZE)]);
        }
        $select = new Select();
        $select->columns(['ID', 'SOURCE', 'TYPE', 'DATA'])
            ->from('LAJI_DOCUMENT')
            ->where(['PID' => $this->pid]);
        return $this->table->selectWith($select);
    }

}