<?php

namespace LajiStore\Service;

use Zend\Log\Filter\Priority;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class LoggerFactory is factory class for creating logger.
 *
 * @package Kotka\Service\Factory
 */
class LoggerFactory implements FactoryInterface
{
    const LOG_LEVEL = Logger::INFO;

    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return Logger
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $logger = new Logger();
        $writer = new Stream('./data/logs/' . date('Y-m-d') . '.log');

        $logger->addWriter($writer);
        $writer->addFilter(new Priority(self::LOG_LEVEL));

        Logger::registerErrorHandler($logger);

        return $logger;
    }
}
