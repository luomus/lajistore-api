<?php


namespace LajiStore\ElasticExtract;


use Elastic\Client\Index;
use Elastic\Extract\ExtractInterface;

class GenericExtract implements ExtractInterface
{

    private $type;
    private $source;

    public function __construct($source, $type)
    {
        $this->source = $source;
        $this->type = $type;
    }


    public function addToBulk($data, &$bulk)
    {
        $id = isset($data['id']) ? $data['id'] : null;
        if ($id === null) {
            return;
        }
        if (!isset($bulk['body'])) {
            $bulk['body'] = [];
        }

        $bulk['body'][] = ['index' => [
            '_index' => $this->getIndex(),
            '_type' => self::getTypeName(),
            '_id' => $id
        ]];
        $bulk['body'][] = $data;

    }

    public function getMapping()
    {
        return [
            "dynamic_templates" => [
                [
                    "string" => [
                        "match_mapping_type" => "string",
                        "mapping" => [
                            'type' => 'keyword',
                            "ignore_above" => 512
                        ]
                    ],
                ]
            ],
            "dynamic_date_formats" => ["strict_date_optional_time||yyyy-MM-dd HH:mm:ss Z||yyyy-MM-dd Z||dd.MM.yyyy HH:mm:ss||dd.MM.yyyy HH:mm||dd.MM.yyyy"]
        ];
    }

    public function getSettings()
    {
        return [
            'number_of_shards' => Index::SHARDS,
            'number_of_replicas' => Index::REPLICAS,
            'index.mapping.ignore_malformed' => true
        ];
    }

    /**
     * Returns the extracts that are related to this Extract.
     *
     * @return array
     */
    public function getRelatedExtracts()
    {
        return [];
    }

    public static function getTypeName()
    {
        return 'data';
    }

    public static function getIndexName()
    {
        return 'laji_deprecated';
    }

    /**
     * @return string
     */
    public function getIndex()
    {
        return 'laji_' . $this->source . '_' . $this->type;
    }
}