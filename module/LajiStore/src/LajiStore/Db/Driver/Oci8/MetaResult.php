<?php

namespace LajiStore\Db\Driver\Oci8;

use LajiStore\Db\Driver\MetaResultInterface;
use LajiStore\Db\Driver\MetaResultTrait;
use Zend\Db\Adapter\Driver\Oci8\Result;

class MetaResult extends Result implements MetaResultInterface
{
    use MetaResultTrait;

    public static $disable = false;

    protected $serializedField;
    protected $data;
    protected $dataInitialized = false;

    public function __construct($serializedField)
    {
        $this->serializedField = $serializedField;
    }

    protected function loadData()
    {
        if (!$this->dataInitialized) {
            $data = [];
            //$start = microtime(true);
            while ( $row = oci_fetch_array($this->resource, OCI_ASSOC+OCI_RETURN_LOBS) ) {
                $data[] = $row;
            }
            //$end = microtime(true);
            //$took = $end - $start;
            //if ($took > 0.001) {
            //    file_put_contents('./debug.txt', 'took ' . $took . "\n", FILE_APPEND);
            //}
            $this->data = $data;
            $this->dataInitialized = true;
        }
        $this->currentComplete = true;
        $this->currentData = isset($this->data[$this->position]) ? $this->data[$this->position] : false;
        if ($this->currentData !== false) {
            $this->position++;
            return true;
        }
        return false;
    }


    /**
     * Reads the actual data from the row and returns it decoded
     *
     * @return mixed
     */
    public function current()
    {
        if (MetaResult::$disable) {
            return parent::current();
        }
        $current =  parent::current();
        if (isset($current[$this->serializedField])) {
            if (is_a($current[$this->serializedField], 'OCI-Lob')) {
                $current[$this->serializedField] = $current[$this->serializedField]->load();
            }
            return $this->unSerializeData($current[$this->serializedField]);
        }
        return $current;
    }
}