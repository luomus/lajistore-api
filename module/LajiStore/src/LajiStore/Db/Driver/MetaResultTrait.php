<?php

namespace LajiStore\Db\Driver;


trait MetaResultTrait
{

    protected function unSerializeData($json)
    {
        return json_decode($json, true);
    }

}