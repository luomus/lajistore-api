<?php
namespace LajiStore\Db\Sql\Predicate;

use Zend\Db\Sql\Exception;
use Zend\Db\Sql\Predicate\Expression as ZendExpression;

class Expression extends ZendExpression
{
    const PLACEHOLDER = '??';

    /**
     * @return array
     * @throws Exception\RuntimeException
     */
    public function getExpressionData()
    {
        $parameters = (is_scalar($this->parameters)) ? [$this->parameters] : $this->parameters;
        $parametersCount = count($parameters);
        $expression = str_replace('%', '%%', $this->expression);

        if ($parametersCount == 0) {
            return [
                str_ireplace(static::PLACEHOLDER, '', $expression)
            ];
        }

        // assign locally, escaping % signs
        $expression = str_replace(static::PLACEHOLDER, '%s', $expression, $count);
        if ($count !== $parametersCount) {
            throw new Exception\RuntimeException('The number of replacements in the expression does not match the number of parameters');
        }
        foreach ($parameters as $parameter) {
            list($values[], $types[]) = $this->normalizeArgument($parameter, self::TYPE_VALUE);
        }
        return [[
            $expression,
            $values,
            $types
        ]];
    }
}