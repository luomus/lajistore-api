<?php

namespace LajiStore\Db\Sql;

use LajiStore\Db\Sql\Predicate\Expression;

class OracleWhere extends AbstractWhere
{
    const DB_TIME_FORMAT = 'YYYY-MM-DD"T"HH24:MI:SSTZH:TZM';

    public function selectByValue($docType, $field, $value, $combine, $fieldTypes, $operator = '=', $subCombine = 'AND') {
        if (empty($field) || empty($value)) {
            return;
        }
        if (isset($fieldTypes[AbstractWhere::SEARCH_TABLE]) && isset($fieldTypes[AbstractWhere::SEARCH_TABLE][$field]) && is_callable($fieldTypes[AbstractWhere::SEARCH_TABLE][$field])) {
            $this->addPredicate(
                $fieldTypes[AbstractWhere::SEARCH_TABLE][$field]($this, $field, $value, $operator),
                $combine
            );
            return;
        }
        $sqlWrap = ($this->neq) ? '(NOT (%s))' : '(%s)';
        if (!is_array($value)) {
            $value = [$value];
        }
        $sql = array_fill(0, count($value), $this->getPlainSql($docType, $field, $operator, $fieldTypes));
        $sql = implode(' ' . $subCombine . ' ', $sql);
        $sql = sprintf($sqlWrap, $sql);
        $this->addPredicate(new Expression($sql, $value), $combine);
    }

    public function selectByRange($docType, $field, &$valueStart, &$valueEnd, $combine, $fieldTypes, $startOperator = '>', $endOperator = '<')
    {
        if (isset($fieldTypes[AbstractWhere::SEARCH_TABLE]) && isset($fieldTypes[AbstractWhere::SEARCH_TABLE][$field]) && is_callable($fieldTypes[AbstractWhere::SEARCH_TABLE][$field])) {
            $this->addPredicate(
                $fieldTypes[AbstractWhere::SEARCH_TABLE][$field]($this, $field, [$valueStart, $valueEnd], [$startOperator, $endOperator]),
                $combine
            );
            return;
        }
        $sqlWrap = $this->neq ? 'NOT (%s AND %s)' : '%s AND %s';
        $sql = sprintf($sqlWrap,
            $this->getPlainSql($docType, $field, $startOperator, $fieldTypes),
            $this->getPlainSql($docType, $field, $endOperator, $fieldTypes)
        );
        $this->addPredicate(new Expression($sql, [$valueStart, $valueEnd]), $combine);
    }

    private function getPlainSql($docType, $field, $operator, $fieldTypes) {
        $type  = is_array($fieldTypes) ? (isset($fieldTypes[$field]) ? strtolower($fieldTypes[$field]) : self::TYPE_TEXT) : $fieldTypes;
        $field = str_replace('.','[*].', $field);
        switch($type) {
            case self::TYPE_ARRAY:
                $sql = 'JSON_EXISTS(data, \'$.' . $field . '[*]?(@ ' . $operator . '= $LD_VAL)\' passing ?? as "LD_VAL")';
                /*
                $sql = 'ID in (
                            SELECT "LAJI_DOCUMENT"."ID"
                                FROM "LAJI_DOCUMENT",
                                JSON_TABLE(data, \'$.' . $field . '[*]\' COLUMNS (val PATH \'$\')) jt
                                WHERE "LAJI_DOCUMENT"."TYPE" = \'' . $docType . '\' AND jt.val ' . $operator . ' ??
                        )';
                */
                break;
            case self::TYPE_ID:
                $sql = 'ID ' . $operator . ' ??';
                break;
            case self::TYPE_DATETIME:
                $sql = 'to_timestamp_tz(json_value(data, \'$.' . $field . '\'),\'' . self::DB_TIME_FORMAT . '\') ' . $operator . ' to_timestamp_tz(??,\'' . self::DB_TIME_FORMAT . '\')';
                break;
            case self::TYPE_BOOLEAN:
                $sql = 'json_value(data, \'$.' . $field . '\' DEFAULT \'false\' ON ERROR) ' . $operator . ' ??';
                break;
            default:
                $sql = 'json_value(data, \'$.' . $field . '\') ' . $operator . ' ??';
                //$sql = 'JSON_EXISTS(data, \'$.' . $field . '?(@ ' . $operator . ' $val )\', passing ?? as $val)';
        }
        return $sql;
    }
}