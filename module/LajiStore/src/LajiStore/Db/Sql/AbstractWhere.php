<?php
namespace LajiStore\Db\Sql;

use DomainException;
use UnexpectedValueException;
use Zend\Db\Sql\Where;

abstract class AbstractWhere extends Where
{
    const TYPE_ID = 'id';
    const TYPE_TEXT = 'text';
    const TYPE_ARRAY = 'array';
    const TYPE_NUMBER = 'number';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_DATETIME = 'datetime';

    const SEARCH_TABLE = 'search_table';
    const SEARCH_TABLE_GATEWAY = 'table_gateway';


    protected $query;
    protected $buffer = [];
    protected $platformName;
    protected $neq = false;
    protected $docType;

    abstract public function selectByValue($docType, $field, $value, $combine, $fieldTypes, $operator = '=', $subCombine = 'AND');
    abstract public function selectByRange($docType, $field, &$valueStart, &$valueEnd, $combine, $fieldTypes, $startOperator = '>', $endOperator = '<');

    public function parseQuery($docType, $query, $fieldTypes, &$pos = 0, $sub = false) {
        $current   = '';
        $field     = null;
        $combine   = $this->defaultCombination;
        $valueList = false;
        $inQuote   = false;
        $this->neq = false;
        $buffer    = [];
        $this->docType = $docType;
        while(isset($query[$pos])) {
            if ($inQuote && $query[$pos] !== '"') {
                $current .= $query[$pos];
                $pos++;
                continue;
            }
            switch($query[$pos]) {
                case '[':
                    $this->addRange($docType, $field, $query, $pos, $combine, $fieldTypes, '>=');
                    break;
                case '{':
                    $this->addRange($docType, $field, $query, $pos, $combine, $fieldTypes, '>');
                    break;
                case '"':
                    if ($inQuote) {
                        if ($valueList) {
                            $buffer[] = $current;
                        } else {
                            $this->selectByValue($docType, $field, $current, $combine, $fieldTypes);
                        }
                        $current = '';
                    } elseif ($field === null) {
                        throw new UnexpectedValueException("You should specify field before using quotation mark.", 400);
                    }
                    $inQuote = !$inQuote;
                    break;
                case ':':
                    if (!isset($fieldTypes[$current])) {
                        throw new DomainException("'$current' is not correct query parameter", 400);
                    }
                    $field = trim($current);
                    $current = '';
                    break;
                case '(':
                    if ($field === null) {
                        $pos++;
                        $subQuery = new static();
                        $subQuery->parseQuery($docType, $query, $fieldTypes, $pos, true);
                        $this->addPredicate($subQuery, $combine);
                    } else {
                        $buffer    = [];
                        $valueList = true;
                        $current   = '';
                    }
                    break;
                case ')':
                    if ($valueList) {
                        if (!empty($current)) {
                            $buffer[] = $current;
                        }
                        if (!empty($buffer)) {
                            $this->selectByValue($docType, $field, $buffer, $combine, $fieldTypes, '=', 'OR');
                        }
                        $buffer = [];
                        $valueList = false;
                        $field     = null;
                    } else if ($sub) {
                        $this->selectByValue($docType, $field, $current, $combine, $fieldTypes);
                        $pos++;
                        return $this;
                    } else {
                        throw new UnexpectedValueException("Query had ')', but is Missing opening parentheses.", 400);
                    }
                    break;
                case 'N':
                    if (isset($query[$pos + 3]) && $query[$pos + 1] === 'O' && $query[$pos + 2] === 'T' && $query[$pos + 3] === ' ') {
                        $this->neq = true;
                        $pos += 3;
                    } else {
                        $current .= 'N';
                    }
                    break;
                case 'A':
                    if (isset($query[$pos + 3]) && $query[$pos + 1] === 'N' && $query[$pos + 2] === 'D' && $query[$pos + 3] === ' ') {
                        $this->selectByValue($docType, $field, $current, $combine, $fieldTypes);
                        $current = '';
                        $combine = self::COMBINED_BY_AND;
                        $pos += 3;
                    } else {
                        $current .= 'A';
                    }
                    break;
                case 'O':
                    if (isset($query[$pos + 2]) && $query[$pos + 1] === 'R' && $query[$pos + 2] === ' ') {
                        $this->selectByValue($docType, $field, $current, $combine, $fieldTypes);
                        $current = '';
                        $combine = self::COMBINED_BY_OR;
                        $pos += 2;
                    } else {
                        $current .= 'O';
                    }
                    break;
                case ' ':
                    if (!$valueList) {
                        $this->selectByValue($docType, $field, $current, $combine, $fieldTypes);
                        $field = null;
                        $combine = $this->defaultCombination;
                        $this->neq = false;
                    }
                    $current = '';
                    break;
                /** @noinspection PhpMissingBreakStatementInspection */
                case '\\':
                    $pos++;
                default:
                    $current .= $query[$pos];
            }
            $pos++;
        }
        if ($sub) {
            throw new UnexpectedValueException("Closing parentheses is missing in query", 400);
        }
        if ($inQuote) {
            throw new UnexpectedValueException("Missing closing quotation mark!", 400);
        }
        $this->selectByValue($docType, $field, $current, $combine, $fieldTypes);
        return $this;
    }

    protected function addRange($docType, $field, $query, &$pos, $combine, $fieldTypes, $startComparison) {
        if (empty($field)) {
            throw new UnexpectedValueException("You should specify field before specifying range.", 400);
        }
        $pos++;
        $start = $pos;
        $endComparison = null;
        while(isset($query[$pos])) {
            if ($query[$pos] === ']') {
                $endComparison = '<=';
                break;
            } else if ($query[$pos] === '}') {
                $endComparison = '<';
                break;
            }
            $pos++;
        }
        if ($endComparison === null) {
            throw new UnexpectedValueException("Missing end of range!.", 400);
        }
        $range = substr($query, $start, ($pos - $start));
        $range = explode(' TO ', $range);
        if (count($range) != 2) {
            throw new UnexpectedValueException("Invalid range given!.", 400);
        }
        $startStar = trim($range[0]) === '*';
        $endStar   = trim($range[1]) === '*';
        if (!$startStar && !$endStar) {
            $this->selectByRange($docType, $field, $range[0], $range[1], $combine, $fieldTypes, $startComparison, $endComparison);
            return;
        }
        if (!$startStar) {
            $this->selectByValue($docType, $field, $range[0], $combine, $fieldTypes, $startComparison);
        }
        if (!$endStar) {
            $this->selectByValue($docType, $field, $range[1], $combine, $fieldTypes, $endComparison);
        }
    }
}