<?php

namespace LajiStore\TableGateway;


use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway as ZendTableGateway;

class TableGateway extends ZendTableGateway
{
    protected $useHistory = false;
    protected $originalTable;
    protected $historyTable;

    public function __construct($table, AdapterInterface $adapter, $features = null, ResultSetInterface $resultSetPrototype = null, Sql $sql = null, $historyTable = null)
    {
        $this->originalTable = $table;
        $this->historyTable  = $historyTable;
        parent::__construct($table, $adapter, $features, $resultSetPrototype, $sql);
    }

    public function getTable()
    {
        if ($this->useHistory) {
            if ($this->historyTable === null) {
                throw new Exception\InvalidArgumentException('Requested history data but no history table specified!');
            }
            return $this->historyTable;
        }
        return parent::getTable();
    }

    /**
     * @return boolean
     */
    public function isUseHistory()
    {
        return $this->useHistory;
    }

    /**
     * @param boolean $useHistory
     */
    public function setUseHistory($useHistory)
    {
        if ($useHistory) {
            $this->table = $this->historyTable;
        } else {
            $this->table = $this->originalTable;
        }
        if ($this->useHistory !== $useHistory) {
            $this->sql = new Sql($this->adapter, $this->table);
        }
        $this->useHistory = $useHistory;
    }

}