<?php

namespace LajiStore\TableGateway\Exception;

use Zend\Db\Exception;

interface ExceptionInterface extends Exception\ExceptionInterface
{
}