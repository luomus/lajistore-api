<?php

namespace LajiStore\TableGateway\Exception;

use Zend\Db\Exception;

class InvalidArgumentException extends Exception\InvalidArgumentException implements ExceptionInterface
{
}