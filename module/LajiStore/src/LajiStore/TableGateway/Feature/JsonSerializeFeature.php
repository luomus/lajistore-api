<?php

namespace LajiStore\TableGateway\Feature;

use LajiStore\Db\Driver\MetaResultInterface;
use LajiStore\Db\Driver\Oci8\MetaResult as Oci8MetaResult;
use Zend\Db\Adapter\Driver\Oci8\Result as Oci8Result;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Update;
use Zend\Db\TableGateway\Feature\AbstractFeature;

class JsonSerializeFeature extends AbstractFeature
{

    /**
     * @var string
     */
    protected $serializeField;

    public function __construct($serializeField)
    {
        $this->serializeField = $serializeField;
    }


    /**
     * Wraps the select with where clause
     *
     * @param Select $select
     * @return Select
     */
    public function preSelect(Select $select) {
        $driver = $this->tableGateway->getAdapter()->getDriver();
        if (method_exists($driver, 'getResultPrototype') && method_exists($driver, 'registerResultPrototype')) {
            $driver->registerResultPrototype(
                $this->proxyResult($driver->getResultPrototype())
            );
        }

        return $select;
    }

    /**
     * Prepares data to be inserted in json data format
     *
     * @param Insert $insert
     * @return Insert
     */
    public function preInsert(Insert $insert) {
        $data = $insert->getRawState();
        if (!isset($data['columns']) || !isset($data['values'])) {
            return $insert;
        }
        $data = array_combine($data['columns'], $data['values']);
        $insert->values(
            $this->serialize($data),
            Insert::VALUES_SET
        );

        return $insert;
    }

    /**
     * Prepares data to be inserted to json data format
     *
     * @param Update $update
     * @return Update
     */
    public function preUpdate(Update $update) {
        $data = $update->getRawState();
        if (!isset($data['set'])) {
            return $update;
        }
        $update->set(
            $this->serialize($data['set']),
            Update::VALUES_SET
        );

        return $update;
    }

    protected function serialize($data) {
        if (is_array($data) && isset($data[$this->serializeField]) && is_array($data[$this->serializeField])) {
            $data[$this->serializeField] = json_encode($data[$this->serializeField]);
        }
        return $data;
    }

    protected function proxyResult(ResultInterface $result) {
        if ($result instanceof MetaResultInterface) {
            return $result;
        } elseif ($result instanceof Oci8Result) {
            return new Oci8MetaResult($this->serializeField);
        } else {
            throw new \Exception('Could not proxy the result');
        }
    }
}
