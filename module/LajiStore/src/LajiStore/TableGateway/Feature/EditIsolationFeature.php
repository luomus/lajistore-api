<?php

namespace LajiStore\TableGateway\Feature;

use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Update;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\Feature\AbstractFeature;

class EditIsolationFeature extends AbstractFeature
{
    protected $field;
    protected $value;

    public function __construct($field, $value) {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Wraps the update with where clause
     *
     * @param Update $update
     * @return Update
     */
    public function preUpdate(Update $update) {
        $update->where($this->wrapWhere($update->where));

        return $update;
    }

    /**
     * Wraps the delete with where clause
     *
     * @param Delete $delete
     * @return Delete
     */
    public function preDelete(Delete $delete) {
        $delete->where($this->wrapWhere($delete->where));

        return $delete;
    }

    /**
     * Limits the visibility of where clause to the given source
     *
     * @param Where $subWhere
     * @return Where
     */
    protected function wrapWhere(Where $subWhere) {
        $where = new Where();
        $where->equalTo($this->field, $this->value);
        if ($subWhere->count()) {
            $where->andPredicate($subWhere);
        }
        return $where;
    }

}