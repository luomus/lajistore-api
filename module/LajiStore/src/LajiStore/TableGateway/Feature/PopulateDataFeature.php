<?php

namespace LajiStore\TableGateway\Feature;

use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Update;
use Zend\Db\TableGateway\Feature\AbstractFeature;

class PopulateDataFeature extends AbstractFeature
{

    static $cachedSequenceValue = 0;
    static $cachedSequenceID = '';

    const SUB_ID_SEPARATOR = '#';
    const SUB_ID = '__SUB_ID__';
    const EMPTY_OBJECT = '__EMPTY_OBJECT__';

    /**
     * @var string
     */
    protected $primaryKeyField;

    /**
     * @var string
     */
    protected $primaryValue;

    /**
     * @var string
     */
    protected $sequenceKeyField;

    /**
     * @var int
     */
    protected $sequenceValue = 0;

    /**
     * @var array
     */
    protected $populateInstructions = [];


    /**
     * @param string $primaryKeyField
     * @param string $sequenceKeyField
     * @param array  $populateInstructions
     */
    public function __construct($populateInstructions, $primaryKeyField = null, $sequenceKeyField = null)
    {
        $this->primaryKeyField  = $primaryKeyField;
        $this->sequenceKeyField = $sequenceKeyField;
        $this->populateInstructions = $populateInstructions;
    }

    /**
     * @param Insert $insert
     * @return Insert
     * @throws \Exception
     */
    public function preInsert(Insert $insert)
    {
        $columns = $insert->getRawState('columns');
        $values = $insert->getRawState('values');
        $raw    = array_combine($columns, $values);

        $insert->values(
            $this->prepareData($raw),
            Insert::VALUES_SET
        );

        return $insert;
    }

    /**
     * Prepares data to be inserted to json data format
     *
     * @param Update $update
     * @return Update
     */
    public function preUpdate(Update $update) {
        $data = $update->getRawState();
        if (!isset($data['set'])) {
            return $update;
        }
        $update->set(
            $this->prepareData($data['set']),
            Update::VALUES_SET
        );

        return $update;
    }

    protected function prepareData($raw) {
        if (!isset($raw['DATA'])) {
            throw new \Exception("Could not generate child IDs!");
        }
        if ($this->primaryKeyField !== null && isset($raw['DATA'][$this->primaryKeyField])) {
            $this->primaryValue = $raw['DATA'][$this->primaryKeyField];
            if ($this->sequenceKeyField !== null) {
                $this->initSequenceValue();
            }
        }

        $this->addValues($raw['DATA'], $this->parsePopulationInstructions());
        if ($this->sequenceKeyField !== null) {
            $raw[$this->sequenceKeyField] = (int)$this->sequenceValue;
        }
        if (!empty($this->primaryValue)) {
            self::$cachedSequenceID = $this->primaryValue;
            self::$cachedSequenceValue = (int)$this->sequenceValue;
        }

        return $raw;
    }

    protected function initSequenceValue() {
        if (self::$cachedSequenceID === $this->primaryValue) {
            $this->sequenceValue = self::$cachedSequenceValue;
            return;
        }
        $select = new Select();
        $select
            ->from($this->tableGateway->getTable())
            ->columns([$this->sequenceKeyField])
            ->where(['ID' => $this->primaryValue]);
        $result = $this->tableGateway->selectWith($select)->current();
        if ($result) {
            $this->sequenceValue = (int)$result->{$this->sequenceKeyField};
        }
    }

    protected function addValues(&$data, $locations) {
        foreach($locations as $key => $value) {
            if ($key == '*') {
                if (is_array($data)) {
                    foreach($data as &$child) {
                        $this->addValues($child, $value);
                    }
                }
            } elseif (is_string($value)) {
                if ($value === self::SUB_ID) {
                    if (!isset($data[$key])) {
                        $data[$key] = $this->primaryValue . self::SUB_ID_SEPARATOR . $this->nextSequenceId();
                    }
                } else {
                    $data[$key] = $value;
                }
            } elseif (isset($data[$key])) {
                $this->addValues($data[$key], $value);
            }
        }
    }

    protected function parsePopulationInstructions() {
        $childIDLocations = [];
        $query = '';
        foreach($this->populateInstructions as $location => $value) {
            $location = explode('/', $location);
            $query .= array_shift($location);
            $query .= empty($location) ?
                '=' . $value :
                '[' . implode('][', $location) . ']=' . $value;

            $query .= '&';
        }
        parse_str($query, $childIDLocations);
        return $childIDLocations;
    }

    /**
     * Generate a new value from the specified sequence in the database, and return it.
     * @return int
     */
    public function nextSequenceId()
    {
        $this->sequenceValue++;
        return $this->sequenceValue;
    }

    /**
     * Return the most recent value from the specified sequence
     * @return int
     */
    public function lastSequenceId()
    {
        return $this->sequenceValue;
    }
}
