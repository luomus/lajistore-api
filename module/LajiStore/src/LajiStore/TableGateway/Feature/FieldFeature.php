<?php

namespace LajiStore\TableGateway\Feature;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\Adapter\Driver\StatementInterface;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\Feature\AbstractFeature;

class FieldFeature extends AbstractFeature
{
    const KEY_FINAL = '_final_';
    protected $fields;
    protected $keepValues = [];
    protected $keepKey;
    protected $instructions;
    protected $isAmbiguous = false;

    public function __construct($fields, array $instructions)
    {
        $this->fields = explode(',', $fields);
        $this->instructions = $instructions;
        foreach ($this->fields as $field) {
            if (!array_key_exists($field, $this->instructions)) {
                throw new \Exception('Cannot select by field "'. $field .'"');
            }
            if ($this->instructions[$field] === false) {
                $this->isAmbiguous = true;
                break;
            }
        }
    }

    public function preSelect(Select $select) {
        $columns = [
            'ID'
        ];
        foreach ($this->fields as $key => $field) {
            if ($field === 'id') {
                continue;
            }
            // When this https://stackoverflow.com/questions/38294462/json-query-path-expression-based-on-variables
            // can be elegantly handled. The line bellow can be replaced with this:
            //$columns[$field] = new Expression("json_query(data, ? WITH ARRAY WRAPPER)", "$." . $field);
            $columns[$field] = new Expression("json_query(data, '$." . $field . "' RETURNING CLOB WITH ARRAY WRAPPER)");
        }
        $select->columns($columns);
    }

    public function postSelect(StatementInterface $statement, ResultInterface $result, ResultSetInterface $resultSet)
    {
        if (empty($this->fields)) {
            return $resultSet;
        }
        $data = [];
        foreach($result as $row) {
            $data[] = $this->selectFields(['id' => $row['ID']], $row);
        }
        $resultSet->initialize($data);
        return $resultSet;
    }

    protected function selectFields($data, $rawData)
    {
        foreach ($rawData as $rawPath => $value) {
            if ($rawPath === 'ID' || $rawPath === 'id') {
                continue;
            }
            if ($this->isAmbiguous) {
                $data[$rawPath] = $this->instructions[$rawPath] ? json_decode($value)[0] : json_decode($value);
                continue;
            }
            $path = explode('.', $rawPath);
            $ref = &$data;
            foreach ($path as $part) {
                $last = substr($part, -1);
                if ($last === ']') {
                    $pos = strpos($part, '[');
                    if ($pos === false) {
                        continue 2;
                    }
                    $parent = substr($part, 0, $pos);
                    $num = (int)substr($part, $pos + 1, -1);
                    if (!isset($ref[$parent])) {
                        $ref[$parent] = [];
                    }
                    for($i = 0; $i < $num; $i++) {
                        if (!isset($ref[$i])) {
                            $ref[$parent][$i] = null;
                        }
                    }
                    if (isset($ref[$parent][$num]) && $ref[$parent][$num] === null) {
                        $ref[$parent][$num] = [];
                    }
                    $ref = &$ref[$parent][$num];
                } else {
                    if (!isset($ref[$part])) {
                        $ref[$part] = [];
                    }
                    $ref = &$ref[$part];
                }
            }
            $ref = json_decode($value)[0];
        }
        return $data;
    }
}