<?php

namespace LajiStore\TableGateway\Feature;

use Zend\Db\Sql\Select;

class FullIsolationFeature extends EditIsolationFeature
{

    /**
     * Wraps the select with where clause
     *
     * @param Select $select
     * @return Select
     */
    public function preSelect(Select $select) {
        $select->where($this->wrapWhere($select->where));

        return $select;
    }
}