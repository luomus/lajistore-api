<?php

namespace LajiStore\TableGateway\Feature;

use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\TableGateway\Feature\AbstractFeature;


class MetadataFeature extends SourceMetadataFeature
{
    protected $apiVersion;
    protected $type;

    public function __construct($source, $type, $apiVersion = 1) {
        parent::__construct($source);
        $this->type       = $type;
        $this->apiVersion = (int) $apiVersion;
    }

    /**
     * Converts the date to a format that database uses
     *
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data) {
        $raw = [];
        if (isset($data['id'])) {
            $raw['ID'] = $data['id'];
        }
        $raw['DATA']        = $data;
        $raw['API_VERSION'] = $this->apiVersion;
        $raw['SOURCE']      = $this->source;
        $raw['TYPE']        = $this->type;

        return $raw;
    }
}