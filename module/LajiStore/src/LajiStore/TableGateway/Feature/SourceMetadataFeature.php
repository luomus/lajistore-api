<?php

namespace LajiStore\TableGateway\Feature;

use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Update;
use Zend\Db\TableGateway\Feature\AbstractFeature;


class SourceMetadataFeature extends AbstractFeature
{
    protected $source;

    public function __construct($source) {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * Prepares data to be inserted in json data format
     *
     * @param Insert $insert
     * @return Insert
     */
    public function preInsert(Insert $insert) {
        $data = $insert->getRawState();
        if (!isset($data['columns']) || !isset($data['values'])) {
            return $insert;
        }
        $data = array_combine($data['columns'], $data['values']);
        $insert->values(
            $this->prepareData($data),
            Insert::VALUES_SET
        );

        return $insert;
    }

    /**
     * Prepares data to be inserted to json data format
     *
     * @param Update $update
     * @return Update
     */
    public function preUpdate(Update $update) {
        $data = $update->getRawState();
        if (!isset($data['set'])) {
            return $update;
        }
        $update->set(
            $this->prepareData($data['set']),
            Update::VALUES_SET
        );

        return $update;
    }

    /**
     * Adds source to value
     *
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data) {
        $data['SOURCE'] = $this->source;

        return $data;
    }
}