<?php

namespace LajiStore\TableGateway\Feature;

use HttpInvalidParamException;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\Adapter\Driver\StatementInterface;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Db\TableGateway\Feature\AbstractFeature;

class FilterFeature extends AbstractFeature
{
    const KEY_FINAL = '_final_';
    const KEY_FILTERED = '_filtered';
    protected $filterBy;
    protected $keepValues = [];
    protected $keepKey;

    public function __construct($filterBy)
    {
        $this->filterBy = $filterBy;
    }

    public function postSelect(StatementInterface $statement, ResultInterface $result, ResultSetInterface $resultSet)
    {
        if (empty($this->filterBy)) {
            return $resultSet;
        }
        $filters = $this->parseFilterBy($this->filterBy);
        $data = [];
        foreach($result as $row) {
            $this->filter($row, $filters);
            $data[] = $row;
        }
        $resultSet->initialize($data);
        return $resultSet;
    }

    protected function filter(&$data, $filters)
    {
        foreach($filters as $key => $filter) {
            $final = isset($filter[self::KEY_FINAL]);
            if (isset($data[$key])) {
                return $this->_filter($data, $filter, $key, $final);
            }
            $data = array_values(array_filter($data, function (&$array) use ($filter, $key, $final) {
                return $this->_filter($array, $filter, $key, $final);
            }));
            return !empty($data);
        }
        return true;
    }

    protected function _filter(&$data, $filter, $key, $final) {
        if ($final) {
            if (is_array($data[$key])) {
                return !empty(array_intersect(array_flip($filter), $data[$key]));
            } elseif (isset($filter[$data[$key]])) {
                return true;
            }
            return false;
        }
        return $this->filter($data[$key], $filter);
    }

    protected function parseFilterBy($filterBy)
    {
        $filters = [];
        $filtersStr = explode(' ', $filterBy);
        foreach($filtersStr as $filter) {
            $parts = explode(':', $filter, 2);
            if (count($parts) < 2) {
                if ($parts[0] === 'OR') {
                    throw new \Exception('OR filtering is not supported!');
                }
                continue;
            }
            $value = $parts[1];
            $keys = explode('.', $parts[0]);

            $reference =& $filters;
            $final = count($keys) - 1;

            foreach ($keys as $key => $part) {
                if (!isset($reference[$part])) {
                    $reference[$part] = array();
                }
                if ($final != $key) {
                    $reference =& $reference[$part];
                } else {
                    $reference[$part][self::KEY_FINAL] = 1;
                    $reference[$part][$value] = 1;
                }
            }
        }
        return $filters;
    }
}