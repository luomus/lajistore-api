<?php

namespace LajiStore\TableGateway\Feature;


use Zend\Db\TableGateway\Feature\SequenceFeature;

class SequencePrefixFeature extends SequenceFeature
{
    protected $prefix;

    public function __construct($primaryKeyField, $sequenceName, $prefix)
    {
        parent::__construct($primaryKeyField, $sequenceName);
        $this->prefix = $prefix;
    }

    public function nextSequenceId() {
        return ($this->prefix . parent::nextSequenceId());
    }
}