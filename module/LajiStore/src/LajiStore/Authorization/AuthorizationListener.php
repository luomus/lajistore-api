<?php

namespace LajiStore\Authorization;


use ZF\MvcAuth\MvcAuthEvent;

class AuthorizationListener
{

    public function __invoke(MvcAuthEvent $mvcAuthEvent)
    {
        /** @var \ZF\MvcAuth\Authorization\AclAuthorization $authorization */
        $authorization = $mvcAuthEvent->getAuthorizationService();
        $allUsedMethod = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];

        $authorization->addResource('Application\Controller\Index::index');
        $authorization->addResource('ZF\Apigility\Documentation\Controller::show');
        $authorization->addResource('ZF\Apigility\Documentation\Swagger\SwaggerUi::show');
        $authorization->addResource('ZF\Apigility\Documentation\Swagger\SwaggerUi::list');
        $authorization->addResource('LajiStore\V1\Rpc\Ping\Controller::ping');

        $authorization->deny('guest', null, $allUsedMethod);
        $authorization->allow('guest', 'Application\Controller\Index::index', 'GET');
        $authorization->allow('guest', 'ZF\Apigility\Documentation\Controller::show', 'GET');
        $authorization->allow('guest', 'ZF\Apigility\Documentation\Swagger\SwaggerUi::show', 'GET');
        $authorization->allow('guest', 'ZF\Apigility\Documentation\Swagger\SwaggerUi::list', 'GET');
        $authorization->allow('guest', 'LajiStore\V1\Rpc\Ping\Controller::ping', 'GET');
        $authorization->allow(null, null, $allUsedMethod);
    }

}