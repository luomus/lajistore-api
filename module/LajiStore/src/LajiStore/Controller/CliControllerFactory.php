<?php
namespace LajiStore\Controller;

use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\Feature\FullIsolationFeature;
use LajiStore\TableGateway\Feature\SourceMetadataFeature;
use LajiStore\TableGateway\TableGateway;
use LajiStore\V1\Rest\Annotation\AnnotationMapper;
use LajiStore\V1\Rest\Document\DocumentMapper;
use LajiStore\V1\Rest\NamedPlace\NamedPlaceMapper;
use LajiStore\Validator\LajistoreValidatorSpecProvider;
use Zend\Http\Client;
use Zend\Mvc\Controller\ControllerManager;
use Luomus\InputFilter\Generator;

class CliControllerFactory
{
    public function __invoke(ControllerManager $controllers)
    {
        $container       = $controllers->getServiceLocator();
        $queueManager    = $container->get('SlmQueue\Queue\QueuePluginManager');
        $queue           = $queueManager->get('lajistore');
        $tableGW         = new TableGateway('LAJI_DOCUMENT', $container->get('laji-db'));
        $esService       = $container->get(ElasticService::class);

        $om              = $container->get('Triplestore\ObjectManager');
        $config          = $container->get('Config');
        $generatorConfig = isset($config['luomus']['input_filter']) ? $config['luomus']['input_filter'] : [];
        $metadataService = $om->getMetadataService();

        return new CliController(
            $tableGW,
            new DocumentMapper(
                $esService,
                '',
                $tableGW,
                $queue,
                new TableGateway(
                    'DOCUMENT_SEARCH',
                    $container->get('laji-db'),
                    [
                        new SourceMetadataFeature(''),
                        new FullIsolationFeature('SOURCE','')
                    ]
                )
            ),
            new AnnotationMapper(
                $esService,
                '',
                $tableGW,
                $queue
            ),
            new NamedPlaceMapper(
                $esService,
                '',
                $tableGW,
                $queue,
                new TableGateway(
                    'NAMEDPLACE_SEARCH',
                    $container->get('laji-db'),
                    [
                        new SourceMetadataFeature(''),
                        new FullIsolationFeature('SOURCE','')
                    ]
                )
            ),
            $container->get('Triplestore\ObjectManager'),
            $queue,
            $container->get(ElasticService::class),
            new Generator(
                $metadataService,
                $generatorConfig,
                new LajistoreValidatorSpecProvider($metadataService)
            ),
            $config
        );
    }
}
