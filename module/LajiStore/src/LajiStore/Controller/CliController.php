<?php
namespace LajiStore\Controller;

use LajiStore\Db\Sql\Predicate\Expression;
use LajiStore\Job\Warehouse;
use LajiStore\Job\WarehouseAbstract;
use LajiStore\Job\WarehouseAnnotation;
use LajiStore\Service\ElasticService;
use LajiStore\TableGateway\Feature\FullIsolationFeature;
use LajiStore\TableGateway\Feature\SourceMetadataFeature;
use LajiStore\TableGateway\TableGateway;
use LajiStore\V1\Rest\Annotation\AnnotationMapper;
use LajiStore\V1\Rest\Document\DocumentMapper;
use LajiStore\V1\Rest\Form\FormMapper;
use LajiStore\V1\Rest\NamedPlace\NamedPlaceMapper;
use SlmQueue\Queue\QueueInterface;
use Triplestore\Db\Oracle;
use Triplestore\Service\ObjectManager;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Select;
use Zend\Http\Client;
use Zend\Mvc\Controller\AbstractConsoleController;
use Zend\ProgressBar\Adapter\Console;
use Zend\ProgressBar\ProgressBar;
use Luomus\InputFilter\Generator;

const KEY_FEATURE = 'features';
const KEY_COLLECTION = 'collectionID';
const DOC_PUBLIC = 'MZ.publicityRestrictionsPublic';

class CliController extends AbstractConsoleController
{
    private $documentTable;
    private $documentMapper;
    private $namedPlaceMapper;
    private $annotationMapper;
    private $om;
    private $queue;
    private $elasticService;
    private $generator;
    private $config;

    public function __construct(
        TableGateway $documentTable,
        DocumentMapper $documentMapper,
        AnnotationMapper $annotationMapper,
        NamedPlaceMapper $namedPlaceMapper,
        ObjectManager $om,
        QueueInterface $queue,
        ElasticService $elasticService,
        Generator $generator,
        $config
    )
    {
        $this->documentTable = $documentTable;
        $this->documentMapper = $documentMapper;
        $this->annotationMapper = $annotationMapper;
        $this->namedPlaceMapper = $namedPlaceMapper;
        $this->om = $om;
        $this->queue = $queue;
        $this->elasticService = $elasticService;
        $this->generator = $generator;
        $this->config = $config;
    }

    public function generateFiltersAction() {
        $this->generator->generateValidators();
        echo "Classes generated\n";
    }

    public function copyToElasticAction() {
        $id = $this->params('id');
        while($this->elasticService->copyToElastic($id !== null ? explode(',', $id) : null, $id !== null) > 0) {}
    }

    public function checkElasticContentAction() {
        $select = new Select();
        $select->from('LAJI_DOCUMENT');
        $select->columns(['ID', 'SOURCE', 'TYPE']);
        $rowSet = $this->documentTable->selectWith($select);
        foreach ($rowSet as $row) {
            if (!isset($row['ID'])) {
                continue;
            }
            if (!$this->elasticService->hasDocument($row['SOURCE'], $row['TYPE'], $row['ID'])) {
                echo "Trying to add " . $row['ID'] . "\n";
                $this->elasticService->copyToElastic([$row['ID']], true);
            }
        }
    }

    public function upgradeAnnotationsAction() {
        $select = new Select();
        $select->from('LAJI_DOCUMENT');
        $select->columns(['ID', 'SOURCE', 'DATA']);
        $pred = new Predicate();
        $pred->equalTo('TYPE', AnnotationMapper::TYPE)
            ->like('DATA', '%"type"%');
        $select->where($pred);
        $rowSet = $this->documentTable->selectWith($select);


        $httpClient = new Client(
            $this->config['api_laji_fi']['url'] . $this->config['api_laji_fi']['endpoints']['annotationsConvert']
        );
        $apiHeaders = [
            'Authorization' => $this->config['api_laji_fi']['token'] ,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];
        $httpClient->setHeaders($apiHeaders);

        foreach ($rowSet as $row) {
            if (!isset($row['ID'])) {
                continue;
            }
            $data = $row['DATA']->load();
            $httpClient->setMethod('POST');
            $httpClient->setRawBody($data);
            $res = $httpClient->send();

            if (!$res->isOk()) {
                echo "FAILED!!!\n.{$res->getBody()}\n";
                exit();
            }
            $data = json_decode($res->getBody());
            if (!$data) {
                echo "FAILED!!!\n.{$res->getBody()}\n";
                exit();
            }

            echo $row['ID'] . "\n";
            $this->purgeToWarehouseAnnotationQueue($row['ID']);
            $this->documentTable->update(['DATA' => json_encode($data)], ['ID' => $row['ID'], 'SOURCE' => $row['SOURCE']]);
        }
        echo "DONE\n";
    }

    public function resetNamedPlaceCoordinatesAction() {
        $id = $this->params('id');
        $source = $this->params('source');
        $version = $this->params('version', '1');
        $dryRun = $this->params('dry-run');
        $geometry = false;

        // 1. Fetch the namedPlace with the given id
        $select = new Select();
        $select->from('LAJI_DOCUMENT');
        $pred = new Predicate();
        $pred
            ->equalTo('TYPE', NamedPlaceMapper::TYPE)
            ->equalTo('SOURCE', $source)
            ->equalTo('ID', $id);
        $select->where($pred);
        $rowSet = $this->documentTable->selectWith($select);

        foreach($rowSet as $row) {
            $origNS = json_decode($row['DATA']->load(), true);

            $select = new Select();
            $select->from('LAJI_DOCUMENT_HISTORY');
            $pred = new Predicate();
            $pred->equalTo('TYPE', NamedPlaceMapper::TYPE)
                ->equalTo('SOURCE', $source)
                ->equalTo('ID', $id)
                ->equalTo('VERSION', $version);
            $select->where($pred);
            $nsSet = $this->documentTable->selectWith($select);
            foreach ($nsSet as $n) {
                $ns = json_decode($n['DATA']->load(), true);
                if (
                    isset($ns['prepopulatedDocument']) &&
                    isset($ns['prepopulatedDocument']['gatherings']) &&
                    isset($ns['prepopulatedDocument']['gatherings'][0]) &&
                    isset($ns['prepopulatedDocument']['gatherings'][0]['geometry'])
                ) {
                    $geometry = $ns['prepopulatedDocument']['gatherings'][0]['geometry'];
                }
            }

            if (
                $geometry &&
                isset($origNS['prepopulatedDocument']) &&
                isset($origNS['prepopulatedDocument']['gatherings']) &&
                isset($origNS['prepopulatedDocument']['gatherings'][0]) &&
                isset($origNS['prepopulatedDocument']['gatherings'][0]['geometry'])
            ) {
                $origNS['prepopulatedDocument']['gatherings'][0]['geometry'] = $geometry;
            }

            if ($dryRun) {
                echo json_encode($origNS, JSON_PRETTY_PRINT);
                echo "\n";
                exit();
            } else {
                $this->documentTable->update(['DATA' => json_encode($origNS)], ['ID' => $row['ID'], 'SOURCE' => $row['SOURCE']]);
            }

        }
        echo "Done\n";
    }

    public function updateEditedDateAction() {
        $dryRun = $this->params('dry-run');
        $source = $this->params('source');
        $select = new Select();
        $select
            ->from('LAJI_DOCUMENT')
            ->where("type = '" . DocumentMapper::TYPE . "' AND 
             id like 'KE.8_%' AND
             edited between to_date('2019-01-25 07', 'YYYY-MM-DD HH24') AND to_date('2019-01-25 10', 'YYYY-MM-DD HH24') AND
             source = '" . $source . "'");
        $rowSet = $this->documentTable->selectWith($select);
        echo "Running\n";
        foreach($rowSet as $row) {
            echo ".";
            $data = json_decode($row['DATA']->load(), true);
            //var_dump($data);exit();
            if (!isset($data['dateEdited'])) {
                continue;
            }
            $dateTime = new \DateTime($data['dateEdited']);
            if ($dryRun) {
                echo json_encode($data, JSON_PRETTY_PRINT);
                exit();
            } else {
                $this->documentTable->update([
                    'EDITED' => $dateTime->format('d.m.Y')
                ], ['ID' => $row['ID'], 'SOURCE' => $row['SOURCE']]);
            }
        }
    }

    public function fixAction() {
        /*
        $select = new Select();
        $select->from('DOCUMENT_SEARCH')
            ->columns(['ID'])
            ->where("COLLECTIONID = 'HR.39'  and DATESTART > 20170900 and DATESTART < 20180900 and id like 'KE.%'");
        $docsSet = $this->documentTable->selectWith($select);

        $possible = [];
        foreach ($docsSet as $set) {
            $possible[$set['ID']] = true;
        }
        file_put_contents('docs.json', json_encode($possible));
        */
        $possible = json_decode(file_get_contents('docs.json'), true);
        $select = new Select();
        $select->from('NAMEDPLACE_SEARCH')
            ->columns(['ID'])
            ->where("COLLECTIONID = 'HR.39'");
        $docsSet = $this->documentTable->selectWith($select);

        foreach ($docsSet as $set) {
            $select = new Select();
            $select->from('LAJI_DOCUMENT')
                ->where("ID = '" . $set['ID'] . "'");
            $nsSet = $this->documentTable->selectWith($select);

            foreach ($nsSet as $rowNs) {
                if ($rowNs['VERSION'] <= 3) {
                    continue;
                }
                $ns = json_decode($rowNs['DATA']->load(), true);
                if (
                    isset($ns['prepopulatedDocument']) &&
                    isset($ns['prepopulatedDocument']['id']) &&
                    isset($possible[$ns['prepopulatedDocument']['id']])
                ) {
                    $geo = @$ns['prepopulatedDocument']['gatherings'][0]['geometry'];
                    if (isset($geo) && isset($geo['geometries']) && count($geo['geometries']) > 1) {
                        echo "OK ${ns['id']}\n";
                        continue;
                    }

                    $select = new Select();
                    $select->from('LAJI_DOCUMENT_HISTORY');
                    $pred = new Predicate();
                    $pred->equalTo('TYPE', NamedPlaceMapper::TYPE)
                        ->equalTo('SOURCE', $rowNs['SOURCE'])
                        ->equalTo('ID', $ns['id'])
                        ->equalTo('VERSION', 1);
                    $select->where($pred);
                    $nsSet = $this->documentTable->selectWith($select);
                    foreach ($nsSet as $n) {
                        $geo = @$n['prepopulatedDocument']['gatherings'][0]['geometry'];
                        if (isset($geo) && isset($geo['geometries']) && count($geo['geometries']) > 1) {
                            echo "new for {$ns['id']}\n";
                            continue;
                        } else {
                            echo "NOTHING FOR {$ns['id']}\n";
                        }
                    }
                }
            }
        }
    }

    public function integrateAction() {

        echo "Running\n";
        $source = $this->params('source');

        // 1. Select all forms and pick collection id of the forms that have copy feature enabled
        $select = new Select();
        $select->from('LAJI_DOCUMENT')->where("TYPE = '" . FormMapper::TYPE . "'");
        $rowSet = $this->documentTable->selectWith($select);
        $collections = [];
        foreach($rowSet as $row) {
            $form = json_decode($row['DATA']->load(), true);
            if (isset($form['options']) && isset($form['options']['namedPlaceOptions']) && isset($form['options']['namedPlaceOptions']['copyLatestDocumentToNamedPlace']) && $form['options']['namedPlaceOptions']['copyLatestDocumentToNamedPlace']) {
                $collections[$form[KEY_COLLECTION]] = true;
            }
        }
        $collections = array_keys($collections);

        // 2. Fetch all namedPlaces with the given document
        $select = new Select();
        $select->from('LAJI_DOCUMENT');
        $pred = new Predicate();
        $pred
            ->equalTo('TYPE', NamedPlaceMapper::TYPE)
            ->equalTo('SOURCE', $source)
            ->in(new Expression("json_value(DATA, '$.collectionID')"), $collections);
        $select->where($pred);
        $rowSet = $this->documentTable->selectWith($select);
        foreach($rowSet as $row) {
            $namedPlace = json_decode($row['DATA']->load(), true);
            $hasChanges = false;
            $currentID = isset($namedPlace['prepopulatedDocument']) && isset($namedPlace['prepopulatedDocument']['id']) ?
                $namedPlace['prepopulatedDocument']['id'] : null;

            // 3. fetch the newest document that was found from the db
            $select = new Select();
            $select->from('DOCUMENT_SEARCH')
                ->where("NAMEDPLACEID = '" . $namedPlace['id'] . "' AND PUBLICITYRESTRICTIONS = 'MZ.publicityRestrictionsPublic' AND DATEEND > 20180901 AND SOURCE = '" . $source . "'") // dateend because we don't want to lift earlier documents for prepopulatedDocument
                ->order('DATEEND desc, ID');
            $docsSet = $this->documentTable->selectWith($select);

            // 4. check if we need to change to the newest document found
            $shouldChangeTo = false;
            $lastEnd = '';
            foreach ($docsSet as $idx => $doc) {
                $end = $doc['DATEEND'];
                if ($idx === 0) {
                    $lastEnd = $end;
                }
                if ($doc['ID'] === $currentID && $lastEnd === $end) {
                    continue 2; // all good move to next namedPlace
                }
                if ($lastEnd !== $end) {
                    break;
                }
                $shouldChangeTo = $doc['ID'];
            }

            if ($shouldChangeTo) {
                $document = $this->getDocument($shouldChangeTo, $source);
                if (isset($document['publicityRestrictions']) && $document['publicityRestrictions'] === DOC_PUBLIC) {
                    $hasChanges = true;
                    $namedPlace['prepopulatedDocument'] = $document;
                    //$namedPlace['prepopulatedDocument']['gatherings'][0]['geometry'] = $document['gatherings'][0]['geometry'];
                }
            } else {
                $document = $this->getDocumentFromNSHistory($row['ID'], $source, $currentID);
                if ($document !== null) {
                    $hasChanges = true;
                    $namedPlace['prepopulatedDocument'] = $document;
                }
            }
            if ($hasChanges) {
                $this->documentTable->update(['DATA' => json_encode($namedPlace)], ['ID' => $row['ID'], 'SOURCE' => $row['SOURCE']]);
            }
        }

    }

    private function getDocumentFromNSHistory($id, $source, $differentFrom) {
        if ($differentFrom === null) {
            return null;
        }
        $select = new Select();
        $select->from('LAJI_DOCUMENT_HISTORY');
        $pred = new Predicate();
        $pred->equalTo('TYPE', NamedPlaceMapper::TYPE)
            ->equalTo('SOURCE', $source)
            ->equalTo('ID', $id);
        $select->where($pred);
        $select->order('VERSION DESC');
        $docSet = $this->documentTable->selectWith($select);
        foreach ($docSet as $d) {
            $ns = json_decode($d['DATA']->load(), true);
            if (isset($ns['prepopulatedDocument'])) {
                if (!isset($ns['prepopulatedDocument']['id'])) {
                    return $ns['prepopulatedDocument'];
                } else if ($ns['prepopulatedDocument']['id'] !== $differentFrom && $this->getDocument($ns['prepopulatedDocument']['id'], $source) !== null) {
                    return $ns['prepopulatedDocument'];
                }
            }
        }
        return null;
    }

    private function getDocument($id, $source) {
        $select = new Select();
        $select->from('LAJI_DOCUMENT');
        $pred = new Predicate();
        $pred->equalTo('TYPE', DocumentMapper::TYPE)
            ->equalTo('SOURCE', $source)
            ->equalTo('ID', $id);
        $select->where($pred);
        $docSet = $this->documentTable->selectWith($select);
        foreach ($docSet as $d) {
            return json_decode($d['DATA']->load(), true);
        }
        return null;
    }

    public function rebuildDocumentSearchIndexAction() {
        $source = $this->params('source');
        $select = new Select();
        $select
            ->from($this->documentTable->table)
            ->columns(['ID'])
            ->where("type = '" . DocumentMapper::TYPE . "' AND SOURCE = '" . $source . "'");
        $rowSet = $this->documentTable->selectWith($select);
        $searchGW = $this->documentMapper->getSearchTableGateway();
        $feature = $searchGW->getFeatureSet()->getFeatureByClassName(SourceMetadataFeature::class);
        if ($feature instanceof SourceMetadataFeature) {
            $feature->setSource($source);
        }
        $feature = $searchGW->getFeatureSet()->getFeatureByClassName(FullIsolationFeature::class);
        if ($feature instanceof FullIsolationFeature && $feature->getField() === 'SOURCE') {
            $feature->setValue($source);
        }
        echo "Running\n";
        foreach($rowSet as $row) {
            $select = new Select();
            $select
                ->from($this->documentTable->table)
                ->columns(['DATA'])
                ->where("TYPE = '" . DocumentMapper::TYPE . "' AND SOURCE = '" . $source . "' AND ID = '" . $row['ID'] . "'");
            $res = $this->documentTable->selectWith($select);
            foreach ($res as $r) {
                $data = json_decode($r['DATA']->load(), true);
                if (!isset($data['id'])) {
                    var_dump($data);
                    exit();
                }
                $this->documentMapper->deleteSearchRow($data['id']);
                $this->documentMapper->addSearchRow($data['id'], $data);
                echo ".";
            }
        }
        echo "\nDone\n";
    }

    public function rebuildNamedPlaceSearchIndexAction() {
        $source = $this->params('source');
        $select = new Select();
        $select
            ->from($this->documentTable->table)
            ->where("type = '" . NamedPlaceMapper::TYPE . "' AND SOURCE = '" . $source . "'");
        $rowSet = $this->documentTable->selectWith($select);
        $searchGW = $this->namedPlaceMapper->getSearchTableGateway();
        $feature = $searchGW->getFeatureSet()->getFeatureByClassName(SourceMetadataFeature::class);
        if ($feature instanceof SourceMetadataFeature) {
            $feature->setSource($source);
        }
        $feature = $searchGW->getFeatureSet()->getFeatureByClassName(FullIsolationFeature::class);
        if ($feature instanceof FullIsolationFeature && $feature->getField() === 'SOURCE') {
            $feature->setValue($source);
        }
        echo "Running\n";
        foreach($rowSet as $row) {
            $data = json_decode($row['DATA']->load(), true);
            if (!isset($data['id'])) {
                var_dump($data);exit();
            }
            try {
                $this->namedPlaceMapper->deleteSearchRow($data['id']);
            } catch (\Exception $e) { }
            $this->namedPlaceMapper->addSearchRow($data['id'], $data);
            echo ".";
        }
        echo "\nDone\n";
    }

    public function updateNSAction()
    {
        $dryRun = $this->params('dry-run');
        $source = $this->params('source');
        $select = new Select();
        $select
            ->from('LAJI_DOCUMENT')
            ->where("type = '" . NamedPlaceMapper::TYPE . "' AND id in (
                select id from namedplace_search where collectionID = 'HR.61'
            )");
        $rowSet = $this->documentTable->selectWith($select);
        echo "Running\n";
        foreach($rowSet as $row) {
            $doc = json_decode($row['DATA']->load(), true);
            $routeNro = $doc['alternativeIDs'][0];

            $tags = [];
            // Nothing needs updating
            continue;

            $data = $this->doNSChanges($doc, $tags);


            if ($data === false) {
                echo ".";
                continue;
            }
            if ($dryRun) {
                echo json_encode($data, JSON_PRETTY_PRINT);
                exit();
            } else {
                echo "#";
                $this->documentTable->update([
                    'DATA' => json_encode($data)
                ], ['ID' => $row['ID'], 'SOURCE' => $row['SOURCE']]);
            }
        }
        echo "\nDone\n";
    }

    public function updateAction()
    {
        $dryRun = $this->params('dry-run');
        $source = $this->params('source');
        $select = new Select();
        $select
            ->from('LAJI_DOCUMENT')
            ->where("type = '" . DocumentMapper::TYPE . "' AND data like '%\/\"%'");
        $rowSet = $this->documentTable->selectWith($select);
        echo "Running\n";
        foreach($rowSet as $row) {
            echo ".";
            $data = $this->doChanges(json_decode($row['DATA']->load(), true));
            if ($data === false) {
                continue;
            }
            if ($dryRun) {
                echo json_encode($data, JSON_PRETTY_PRINT);
                exit();
            } else {
                $this->documentTable->update([
                    'DATA' => json_encode($data)
                ], ['ID' => $row['ID'], 'SOURCE' => $row['SOURCE']]);
                // $this->addToWarehouseJob($row['ID'], Warehouse::ACTION_DELETE);
            }
        }
        echo "\nDone\n";
    }

    private function doChanges($data) {
        $change = false;
        if (isset($data['gatherings'])) {
            foreach ($data['gatherings'] as &$gathering) {
                if (isset($gathering['geometry'])) {
                    $change = true;
                }
                if (isset($gathering['units'])) {
                    foreach ($gathering['units'] as &$unit) {
                        $check = false;
                        /*
                        if (isset($unit['shortHandText']) && in_array($unit['shortHandText'], $this->ch)) {
                            $json = file_get_contents('https://laji.fi/api/autocomplete/unit?formID=MHL.1&q=' . urlencode($unit['shortHandText']));
                            $obj = json_decode($json, true);
                            if (isset($obj['payload'])
                                && isset($obj['payload']['interpretedFrom'])
                                && isset($obj['payload']['interpretedFrom']['type'])
                                && in_array($obj['payload']['interpretedFrom']['type'], ['PARI', 'O'])
                            ) {
                                $t1 = '';
                                $t2 = '';
                                if ($unit['pairCount'] !== $obj['payload']['unit']['pairCount']) {
                                    $t1 = 'pair: ' . $unit['pairCount'] . ' => ' . $obj['payload']['unit']['pairCount'] . "\n";
                                    $unit['pairCount'] = $obj['payload']['unit']['pairCount'];
                                    $change = true;
                                    $check = true;
                                }
                                if (!isset($unit['individualCount'])) {
                                    $unit['individualCount'] = -1;
                                }
                                if (isset($obj['payload']['unit']['individualCount']) && $unit['individualCount'] !== $obj['payload']['unit']['individualCount']) {
                                    $t2 = 'idv: ' . $unit['individualCount'] . ' => ' . $obj['payload']['unit']['individualCount'] . "\n";
                                    $unit['individualCount'] = $obj['payload']['unit']['individualCount'];
                                    $change = true;
                                    $check = true;
                                }
                                if ($unit['individualCount'] === -1) {
                                    unset($unit['individualCount']);
                                }
                                if ($check) {
                                    echo $unit['shortHandText'] . "\n";
                                    echo $t1;
                                    echo $t2;
                                }
                            }
                        }
                        */
                        /*
                        if (isset($unit['movingStatus']) && !is_array($unit['movingStatus'])) {
                            $change = true;
                            $unit['movingStatus'] = [$unit['movingStatus']];
                        }
                        */
                        /*
                        if (isset($unit['identifications']) && isset($unit['identifications'][0]) && isset($unit['identifications'][0]['taxonID'])) {
                            if (!isset($unit['unitFact'])) {
                                $unit['unitFact'] = [];
                            }
                            $unit['unitFact']['autocompleteSelectedTaxonID'] = $unit['identifications'][0]['taxonID'];
                            unset($unit['identifications'][0]['taxonID']);
                        }
                        */
                        /*
                        if (isset($unit['unitGathering']) && isset($unit['unitGathering']['substrate'])) {
                            $unit['substrateNotes'] = $unit['unitGathering']['substrate'];
                            unset($unit['unitGathering']['substrate']);
                        }
                        */
                        /*
                        if (isset($unit['countOfOccurrences']) && !isset($unit['count'])) {
                            $unit['count'] = '' . $unit['countOfOccurrences'];
                            unset($unit['countOfOccurrences']);
                        }
                        */
                    }
                }
            }
        }
        return $change ? $data : false;
    }

    private function doNSChanges($data, $tags = []) {
        $change = false;
        if (!isset($data['tags'])) {
            $data['tags'] = [];
        }
        foreach ($tags as $tag) {
            if (!in_array($tag, $data['tags'])) {
                $change = true;
                array_push($data['tags'], $tag);
            }
        }
        return $change ? $data : false;
    }

    public function autoLinkUsersAction()
    {
        // This can be used after first run
        $back = date('d.m.Y', strtotime('-7 days'));
        //$back = '30.10.2010';
        /** @var Oracle $connection */
        $connection = $this->om->getConnection();
        $dbName = $connection->getDatabase();
        $sql = "SELECT
                    subjectname AS personID,
                    predicatename,
                    resourceliteral AS original,
                    CASE WHEN predicatename = 'MA.lintuvaaraLoginName' THEN 'lintuvaara:'||resourceliteral
                       WHEN predicatename = 'MA.hatikkaLoginName' THEN 'vanhahatikka:'||resourceliteral
                       WHEN predicatename = 'MA.fieldjournalLoginName' THEN 'hatikka:'||resourceliteral
                       WHEN predicatename = 'MA.insectDatabaseLoginName' THEN 'virtala:'||trim(replace(substr(resourceliteral, instr(resourceliteral, '(')+1), ')', ''))
                    END AS dwuserid
                    FROM $dbName.rdf_statementview
                    WHERE predicatename IN ('MA.lintuvaaraLoginName', 'MA.fieldjournalLoginName', 'MA.hatikkaLoginName', 'MA.insectDatabaseLoginName')
                    And created > TO_DATE('$back', 'DD.MM.YYYY')";

        $results = $connection->executeSelectSql($sql);
        $adapter = new Console();
        $progress = new ProgressBar($adapter, 0, count($results));
        foreach ($results as $result) {
            $target = $result['PERSONID'];
            $source = $result['DWUSERID'];
            //echo "$source -> $target\n";
            $this->linkUser($source, $target, false);
            if (strpos($source, '@hatikka.fi') !== false) {
                $source = $result['ORIGINAL'];
                //echo "$source -> $target\n";
                $this->linkUser($source, $target, false);
            }
            $progress->next();
        }
        $progress->finish();
    }

    public function linkUserAction()
    {
        $source  = $this->params('source');
        $target  = $this->params('target');
        $dryRun  = $this->params('dry-run');
        $legTo   = $this->params('leg-to', false);
        $legFrom = $this->params('leg-from', false);
        $this->linkUser($source, $target, $dryRun, $legTo, $legFrom);

        echo "\nDone\n";
    }

    private function linkUser($source, $target, $dryRun, $legTo = false, $legFrom = false) {
        if ($legTo) {
            $legTo = trim($legTo);
            if ($legFrom === false) {
                $legFrom = $source;
            }
        }
        $select = new Select('LAJI_DOCUMENT');
        $pred = new Predicate();
        $pred->equalTo('TYPE',  DocumentMapper::TYPE );
        $pred->expression('ID in (SELECT ID FROM DOCUMENT_SEARCH WHERE OWNERS = ?)', $source);
        $select->where($pred);
        $rowSet = $this->documentTable->selectWith($select);
        $searchGW = $this->documentMapper->getSearchTableGateway();
        foreach($rowSet as $row) {
            // if ($dryRun) {
            //     file_put_contents('docs/doc_' . $row['ID'] . '_original.json', json_encode(json_decode($row['DATA']->load(), true), JSON_PRETTY_PRINT));
            // }
            $data = $this->linkUserInDocument(json_decode($row['DATA']->load(), true), $source, $target, $legTo, $legFrom);
            if ($data === false) {
                continue;
            }
            if ($dryRun) {
                echo json_encode($data, JSON_PRETTY_PRINT);
                // file_put_contents('docs/doc_' . $row['ID'] . '_converted.json', json_encode($data, JSON_PRETTY_PRINT));
            } else {
                $this->documentTable->update([
                    'DATA' => json_encode($data)
                ], ['ID' => $row['ID'], 'SOURCE' => $row['SOURCE']]);
                $this->addToWarehouseJob($row['ID'], Warehouse::ACTION_UPDATE);

                $feature = $searchGW->getFeatureSet()->getFeatureByClassName(SourceMetadataFeature::class);
                if ($feature instanceof SourceMetadataFeature) {
                    $feature->setSource($row['SOURCE']);
                }
                $feature = $searchGW->getFeatureSet()->getFeatureByClassName(FullIsolationFeature::class);
                if ($feature instanceof FullIsolationFeature && $feature->getField() === 'SOURCE') {
                    $feature->setValue($row['SOURCE']);
                }

                $this->documentMapper->deleteSearchRow($data['id']);
                $this->documentMapper->addSearchRow($data['id'], $data);
            }
        }
    }

    private function linkUserInDocument($data, $source, $target, $legTo = false, $legFrom = false) {
        $change = false;
        $docFields = ['creator', 'editor', 'editors'];
        foreach ($docFields as $field) {
            if (isset($data[$field])) {
                $data[$field] = $this->linkValue($data[$field], $source, $target, $change);
            }
        }
        if ($legTo !== false) {
            if (isset($data['gatheringEvent'])) {
                if (isset($data['gatheringEvent']['leg'])) {
                    $data['gatheringEvent']['leg'] = $this->linkValue($data['gatheringEvent']['leg'], $legFrom, $legTo, $change);
                }
                if (isset($data['gatheringEvent']['legUserID'])) {
                    $data['gatheringEvent']['legUserID'] = $this->linkValue($data['gatheringEvent']['legUserID'], $source, $target, $change);
                }
            }
            if (isset($data['gatherings'])) {
                foreach ($data['gatherings'] as &$gathering) {
                    if (isset($gathering['leg'])) {
                        $gathering['leg'] = $this->linkValue($gathering['leg'], $legFrom, $legTo, $change);
                    }
                    if (isset($gathering['legUserID'])) {
                        $gathering['legUserID'] = $this->linkValue($gathering['legUserID'], $source, $target, $change);
                    }
                }
            }
        }
        return $change ? $data : false;
    }

    private function linkValue($value, $source, $target, &$hasChanges) {
        if (is_array($value)) {
            $result = [];
            foreach ($value as $val) {
                $result[] = $this->linkValue($val, $source, $target, $hasChanges);
            }
            return array_filter($result);
        }
        if ($value === $source) {
            $hasChanges = true;
            return $target;
        }
        return $value;
    }

    public function addMissingSourceAction()
    {
        $dryRun = $this->params('dry-run');
        $source = $this->params('source');
        $select = new Select();
        $select
            ->from('LAJI_DOCUMENT')
            ->where("TYPE = 'document' AND SOURCE = '" . $source . "'");
        $rowSet = $this->documentTable->selectWith($select);
        echo "Running";
        $cnt = 0;
        foreach($rowSet as $row) {
            $data = json_decode($row['DATA']->load(), true);
            if (!isset($data['sourceID'])) {
                $cnt++;
                if ($dryRun) {
                    echo "adding to ${row['ID']}\n";
                }
                $data['sourceID'] = 'KE.389';
            }
            if (!$dryRun) {
                $this->documentTable->update([
                    'DATA' => json_encode($data)
                ], ['ID' => $row['ID']]);
                echo ".";
            }
        }
        echo "\nDone added $cnt missing sources\n";
    }

    public function deleteDuplicateAnnotationsAction()
    {
        $dryRun = $this->params('dry-run');
        $same = [];
        $select = new Select();
        $select
            ->from('LAJI_DOCUMENT')
            ->where("TYPE = 'annotation'");
        $rowSet = $this->documentTable->selectWith($select);
        $skip = [
            'id',
            '@type',
            '@context',
            'created',
        ];
        echo "Running";
        foreach($rowSet as $row) {
            $data = json_decode($row['DATA']->load(), true);
            $dataKey = '';
            foreach ($data as $key => $value) {
                if (in_array($key, $skip)) {
                    continue;
                }
                $dataKey .= ';' . $key . ':' . $data[$key];
            }
            if (!isset($same[$dataKey])) {
                $same[$dataKey] = $row['ID'];
                continue;
            }
            if ($dryRun) {
                echo  "\n". $row['ID']. ' same as ' . $same[$dataKey];
            } else {
                echo  "\nDeleting ". $row['ID'];
                $this->annotationMapper->delete($row['ID']);
            }
        }
        echo "\nDone\n";
    }

    public function deleteFromWarehouse()
    {
        $idsRaw = $this->params('id');
        $ids = $idsRaw ? explode(',', $idsRaw) : [];
        foreach ($ids as $id) {
            $this->addToWarehouseJob($id, WarehouseAbstract::ACTION_DELETE);
        }
    }

    public function sendToWarehouseAction()
    {
        $id     = $this->params('id');
        $where  = $id ? ['ID' => explode(',', $id)] : [];
        $where['TYPE'] = DocumentMapper::TYPE;
        $rowSet = $this->documentTable->select($where);
        foreach($rowSet as $row) {
            $this->addToWarehouseJob($row['ID']);
        }
        echo "done\n";
    }

    public function sendAnnotationToWarehouseAction() {
        $id     = $this->params('id');
        $where  = $id ? ['ID' => explode(',', $id)] : [];
        $where['TYPE'] = AnnotationMapper::TYPE;
        $rowSet = $this->documentTable->select($where);
        foreach($rowSet as $row) {
            $this->purgeToWarehouseAnnotationQueue($row['ID']);
        }
        echo "done\n";
    }

    private function purgeToWarehouseAnnotationQueue($id) {
        $job = new WarehouseAnnotation();
        $job->setContent([
            'ids' => [$id],
            'action' => WarehouseAbstract::ACTION_UPDATE
        ]);
        $this->queue->push($job);
    }

    private function addToWarehouseJob($id, $action = WarehouseAbstract::ACTION_UPDATE) {
        $job = new Warehouse();
        $job->setContent([
            'ids' => $id,
            'action' => $action
        ]);
        $this->queue->push($job);
    }

}
