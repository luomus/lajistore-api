<?php

namespace LajiStore\Validator;


use Common\Service\IdService;
use Luomus\InputFilter\Validator\ObjectExistsCheckerInterface;
use Triplestore\Model\Predicate;
use Triplestore\Service\ObjectManager;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\View\Helper\Url;

class ObjectChecker implements ObjectExistsCheckerInterface
{
    private $lajiStoreTableGateway;
    private $tripleStore;
    private $tablePrefix;

    private $config;
    private $rests;
    private $urlHelper;

    public function __construct(TableGateway $table, ObjectManager $triplesStore, $config, Url $urlHelper)
    {
        $this->tripleStore = $triplesStore;
        $this->config = $config;
        if (isset($config['laji-store']['table-prefix'])) {
            $this->tablePrefix = $config['laji-store']['table-prefix'];
        }
        $this->lajiStoreTableGateway = $table;
        $this->urlHelper = $urlHelper;
    }

    public function exists($value, $type)
    {
        // TODO: Disabled check if exists
        return true;
        if ($type === null) {
            return false;
        }
        $dotLessType = IdService::getDotLess($type);
        try {
            if ($this->inLajiStore($dotLessType)) {
                $valid = $this->existsInLajiStore($value, $dotLessType);
            } else {
                $valid = $this->existsInTriplestore($value, $type);
            }
        } catch (\Exception $exception) {
            // TODO: add error logging here
            return false;
        }

        return $valid;
    }

    private function inLajiStore($dotLessType) {
        if ($this->rests === null) {
            $this->init();
        }
        return isset($this->rests[$dotLessType]);
    }

    private function init() {
        $services = isset($this->config['zf-rest']) ? $this->config['zf-rest'] : [];
        $rests = [];
        foreach($services as $key => $value) {
            $name = $this->nthLastName($key, 2);
            if ($name === 'Controller') {
                continue;
            }
            $rests[lcfirst($name)] = true;
        }
        $this->rests = $rests;
    }

    private function nthLastName($value, $target = 1, $cnt = 1)
    {
        $pos = strrpos($value, '\\');
        if ($pos) {
            if ($target > $cnt) {
                $cnt++;
                return $this->nthLastName(substr($value, 0, $pos), $target, $cnt);
            }
            $value = substr($value, $pos + 1);
        }
        return $value;
    }

    protected function existsInLajiStore($id, $dotLessType) {
        $gateway = $this->lajiStoreTableGateway;
        $select = new Select($gateway->getTable());
        $qname = $this->getQName($id, $dotLessType);
        $select->where(['ID' => $qname, 'TYPE' => $dotLessType])->columns(['ID']);
        return $gateway->selectWith($select)->current() !== null;
    }

    private function getQName($id, $dotLessType) {
        $url = $this->urlHelper->__invoke('laji-store.rest.' . $dotLessType, [],['force_canonical' => true]);
        $hashPos = strpos($id, '#');
        if ($hashPos !== false) {
            $id = substr($id, 0 ,$hashPos  - 1);
        }
        return str_replace($url . '/', '', $id);
    }

    protected function existsInTriplestore($subject, $type) {
        $typePredicates = [
            ObjectManager::PREDICATE_TYPE,
            ObjectManager::PREDICATE_SUBCLASS
        ];
        foreach($typePredicates as $predicate) {
            $predicate = $this->tripleStore->getPredicate($subject, $predicate);
            if ($predicate instanceof Predicate) {
                return ($predicate->getFirst()->getValue() === $type);
            }
        }
        return false;
    }
}