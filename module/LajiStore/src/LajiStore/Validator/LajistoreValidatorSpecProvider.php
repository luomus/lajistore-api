<?php
namespace LajiStore\Validator;


use Luomus\InputFilter\ValidatorSpecProvider;

class LajistoreValidatorSpecProvider extends ValidatorSpecProvider
{

    public function getEmbeddedObjectSpec($className, $property, $hasMany, $isRequired)
    {
       if ($hasMany) {
           return $this->getMultipleEmbeddedObjectSpec($className, $property, $isRequired);
       }
        return $this->getSingleEmbeddedObjectSpec($className, $property, $isRequired);
    }

    protected function getMultipleEmbeddedObjectSpec($className, $property, $isRequired) {
        return '
    if (!$this->has(\'' . $property . '\')) {
        $col = new CollectionInputFilter();
        $col->setInputFilter($this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get(\'InputFilterManager\')
            ->get(\'Luomus\V1\GeneratedInputFilter\\'. $className . '\')
            ->setRequired(' . ($isRequired ? 'true' : 'false') . '));
        $this->add($col,
            \''.$property.'\'
        );
    }';
    }

    protected function getSingleEmbeddedObjectSpec($className, $property, $isRequired) {
        return '
    if (!$this->has(\'' . $property . '\')) {
        $this->add($this
            ->getServiceLocator()
            ->getServiceLocator()
            ->get(\'InputFilterManager\')
            ->get(\'Luomus\V1\GeneratedInputFilter\\'. $className . '\')
            ->setRequired(' . ($isRequired ? 'true' : 'false') . '),
            \''.$property.'\'
        );
    }';
    }
}