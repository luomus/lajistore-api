<?php

namespace LajiStore\Validator;

use Interop\Container\ContainerInterface;
use LajiStore\TableGateway\Feature\FullIsolationFeature;
use LajiStore\TableGateway\Feature\MetadataFeature;
use LajiStore\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ObjectCheckerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        return $this($services, ObjectChecker::class);
    }

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $apiIdentity = $container->get('api-identity');

        $featureSet = new FeatureSet([
            new MetadataFeature($apiIdentity->getName(),1),
            new FullIsolationFeature('SOURCE', $apiIdentity->getName())
        ]);
        return new ObjectChecker(
            new TableGateway(
                'LAJI_DOCUMENT',
                $container->get('laji-db'),
                $featureSet
            ),
            $container->get('TripleStore\ObjectManager'),
            $container->get('Config'),
            $container->get('ViewHelperManager')->get('Url')
        );
    }
}