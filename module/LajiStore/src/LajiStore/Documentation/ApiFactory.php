<?php

namespace LajiStore\Documentation;

use Luomus\InputFilter\Validator\MultiLanguage;
use Triplestore\Service\MetadataService;
use Zend\InputFilter\BaseInputFilter;
use Zend\InputFilter\CollectionInputFilter;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterPluginManager;
use Zend\ModuleManager\ModuleManager;
use ZF\Apigility\Documentation\Api;
use ZF\Apigility\Documentation\ApiFactory as DocumentationApiFactory;
use ZF\Apigility\Documentation\Operation;
use ZF\Apigility\Documentation\Service;
use ZF\Configuration\ModuleUtils as ConfigModuleUtils;

class ApiFactory extends DocumentationApiFactory
{

    const OBJECT_DELIMITER = '/';

    protected $embedded = [
        'gatherings',
        'gatheringEvent',
        'unitGathering',
        'units',
        'identifications',
        'typeSpecimens',
        'gatherings',
    ];

    protected $descripedObject = [];

    /**
     * @var InputFilterPluginManager
     */
    protected $inputFilterManager;

    /**
     * @var MetadataService
     */
    protected $metadataService;

    public function __construct(
        ModuleManager $moduleManager,
        array $config,
        ConfigModuleUtils $configModuleUtils,
        InputFilterPluginManager $inputFilterManger,
        MetadataService $metadataService
    )
    {
        $this->metadataService = $metadataService;
        $this->inputFilterManager = $inputFilterManger;
        parent::__construct($moduleManager, $config, $configModuleUtils);
    }

    /**
     * Create documentation details for a given service in a given version of
     * an API module
     *
     * @param string $apiName
     * @param int|string $apiVersion
     * @param string $serviceName
     * @return Service
     */
    public function createService(Api $api, $serviceName)
    {
        $service = new Service();
        $service->setApi($api);

        $serviceData = null;
        $isRest      = false;
        $isRpc       = false;
        $hasSegments = false;
        $hasFields   = false;
        $config      = $this->config;

        foreach ($config['zf-rest'] as $serviceClassName => $restConfig) {
            if ((strpos($serviceClassName, $api->getName() . '\\') === 0)
                && isset($restConfig['service_name'])
                && ($restConfig['service_name'] === $serviceName)
                && (strstr($serviceClassName, '\\V' . $api->getVersion() . '\\') !== false)
            ) {
                if (isset($restConfig['hide_doc']) && $restConfig['hide_doc'] === true) {
                    return false;
                }
                $serviceData = $restConfig;
                $isRest = true;
                $hasSegments = true;
                break;
            }
        }

        if (!$serviceData) {
            foreach ($config['zf-rpc'] as $serviceClassName => $rpcConfig) {
                if ((strpos($serviceClassName, $api->getName() . '\\') === 0)
                    && isset($rpcConfig['service_name'])
                    && ($rpcConfig['service_name'] === $serviceName)
                    && (strstr($serviceClassName, '\\V' . $api->getVersion() . '\\') !== false)
                ) {
                    if (isset($rpcConfig['hide_doc']) && $rpcConfig['hide_doc'] === true) {
                        return false;
                    }
                    $serviceData = $rpcConfig;
                    $serviceData['action'] = $this->marshalActionFromRouteConfig(
                        $serviceName,
                        $serviceClassName,
                        $rpcConfig
                    );
                    $isRpc = true;
                    break;
                }
            }
        }

        if (!$serviceData || !isset($serviceClassName)) {
            return false;
        }

        $authorizations = $this->getAuthorizations($serviceClassName);

        $docsArray = $this->getDocumentationConfig($api->getName());

        $service->setName($serviceData['service_name']);
        if (isset($docsArray[$serviceClassName]['description'])) {
            $service->setDescription($docsArray[$serviceClassName]['description']);
        }

        $route = $config['router']['routes'][$serviceData['route_name']]['options']['route'];
        $service->setRoute(str_replace('[/v:version]', '', $route)); // remove internal version prefix, hacky
        if ($isRpc) {
            $hasSegments = $this->hasOptionalSegments($route);
        }

        if (isset($serviceData['route_identifier_name'])) {
            $service->setRouteIdentifierName($serviceData['route_identifier_name']);
        }

        $inputFilterManger = $this->inputFilterManager;

        $fields = [];
        if (isset($config['zf-content-validation'][$serviceClassName]['input_filter'])) {
            $validatorName = $config['zf-content-validation'][$serviceClassName]['input_filter'];
            if (isset($config['input_filter_specs'][$validatorName])) {
                foreach ($this->mapFields($config['input_filter_specs'][$validatorName]) as $fieldData) {
                    $fields['input_filter'][] = $this->getField($fieldData);
                }
                $hasFields = true;
            } elseif ($inputFilterManger->has($validatorName)) {
                $inputFilter = $inputFilterManger->get($validatorName);
                if ($inputFilter instanceof BaseInputFilter) {
                    $this->descripedObject = [];
                    foreach($this->mapInputFields($inputFilter)  as $fieldData) {
                        $fields['input_filter'][] = $this->getField($fieldData);
                    }
                    $hasFields = true;
                }
            }
        }

        $baseOperationData = (isset($serviceData['collection_http_methods']))
            ? $serviceData['collection_http_methods']
            : $serviceData['http_methods'];

        $ops = [];
        foreach ($baseOperationData as $httpMethod) {
            $op = new Operation();
            $op->setHttpMethod($httpMethod);

            if (isset($config['zf-content-validation'][$serviceClassName][$httpMethod])) {
                $validatorName = $config['zf-content-validation'][$serviceClassName][$httpMethod];
                if (isset($config['input_filter_specs'][$validatorName])) {
                    foreach ($config['input_filter_specs'][$validatorName] as $fieldData) {
                        $fields[$httpMethod][] = $field = new Field();
                        $field->setName($fieldData['name']);
                        if (isset($fieldData['description'])) {
                            $field->setDescription($fieldData['description']);
                        }
                        $field->setRequired($fieldData['required']);
                    }
                    $hasFields = true;
                }
            }

            if ($isRest) {
                $description = isset($docsArray[$serviceClassName]['collection'][$httpMethod]['description'])
                    ? $docsArray[$serviceClassName]['collection'][$httpMethod]['description']
                    : '';
                $op->setDescription($description);

                $requestDescription = isset($docsArray[$serviceClassName]['collection'][$httpMethod]['request'])
                    ? $docsArray[$serviceClassName]['collection'][$httpMethod]['request']
                    : '';
                $op->setRequestDescription($requestDescription);

                $responseDescription = isset($docsArray[$serviceClassName]['collection'][$httpMethod]['response'])
                    ? $docsArray[$serviceClassName]['collection'][$httpMethod]['response']
                    : '';

                $op->setResponseDescription($responseDescription);
                $op->setRequiresAuthorization(
                    isset($authorizations['collection'][$httpMethod])
                        ? $authorizations['collection'][$httpMethod]
                        : false
                );

                $op->setResponseStatusCodes($this->getStatusCodes(
                    $httpMethod,
                    false,
                    $hasFields,
                    $op->requiresAuthorization()
                ));
            }

            if ($isRpc) {
                $description = isset($docsArray[$serviceClassName][$httpMethod]['description'])
                    ? $docsArray[$serviceClassName][$httpMethod]['description']
                    : '';
                $op->setDescription($description);

                $requestDescription = isset($docsArray[$serviceClassName][$httpMethod]['request'])
                    ? $docsArray[$serviceClassName][$httpMethod]['request']
                    : '';
                $op->setRequestDescription($requestDescription);

                $responseDescription = isset($docsArray[$serviceClassName][$httpMethod]['response'])
                    ? $docsArray[$serviceClassName][$httpMethod]['response']
                    : '';
                $op->setResponseDescription($responseDescription);

                $op->setRequiresAuthorization(
                    isset($authorizations['actions'][$serviceData['action']][$httpMethod])
                        ? $authorizations['actions'][$serviceData['action']][$httpMethod]
                        : false
                );
                $op->setResponseStatusCodes($this->getStatusCodes(
                    $httpMethod,
                    $hasSegments,
                    $hasFields,
                    $op->requiresAuthorization()
                ));
            }

            $ops[] = $op;
        }

        $service->setFields($fields);
        $service->setOperations($ops);

        if (isset($serviceData['entity_http_methods'])) {
            $ops = [];
            foreach ($serviceData['entity_http_methods'] as $httpMethod) {
                $op = new Operation();
                $op->setHttpMethod($httpMethod);

                $description = isset($docsArray[$serviceClassName]['entity'][$httpMethod]['description'])
                    ? $docsArray[$serviceClassName]['entity'][$httpMethod]['description']
                    : '';
                $op->setDescription($description);

                $requestDescription = isset($docsArray[$serviceClassName]['entity'][$httpMethod]['request'])
                    ? $docsArray[$serviceClassName]['entity'][$httpMethod]['request']
                    : '';
                $op->setRequestDescription($requestDescription);

                $responseDescription = isset($docsArray[$serviceClassName]['entity'][$httpMethod]['response'])
                    ? $docsArray[$serviceClassName]['entity'][$httpMethod]['response']
                    : '';
                $op->setResponseDescription($responseDescription);

                $op->setRequiresAuthorization(
                    isset($authorizations['entity'][$httpMethod])
                        ? $authorizations['entity'][$httpMethod]
                        : false
                );
                $op->setResponseStatusCodes($this->getStatusCodes(
                    $httpMethod,
                    true,
                    $hasFields,
                    $op->requiresAuthorization()
                ));
                $ops[] = $op;
            }
            $service->setEntityOperations($ops);
        }

        if (isset($config['zf-content-negotiation']['accept_whitelist'][$serviceClassName])) {
            $service->setRequestAcceptTypes(
                $config['zf-content-negotiation']['accept_whitelist'][$serviceClassName]
            );
        }

        if (isset($config['zf-content-negotiation']['content_type_whitelist'][$serviceClassName])) {
            $service->setRequestContentTypes(
                $config['zf-content-negotiation']['content_type_whitelist'][$serviceClassName]
            );
        }

        return $service;
    }

    /**
     * @param $field
     * @param string $prefix
     * @return array
     */
    private function mapInputFields($field, $prefix = '')
    {
        $flatFields = [];
        if ($field instanceof InputFilter) {
            foreach($field->getInputs() as $name => $input) {
                if ($input instanceof Input) {
                    if ($this->analyzeMultiLanguage($input, $prefix, $flatFields)) {
                        continue;
                    }
                    $flatFields[] = $this->getFieldFromInput($input, $prefix, $field);
                } elseif ($input instanceof CollectionInputFilter) {
                    $fullindex  = $prefix ? sprintf('%s'.self::OBJECT_DELIMITER.'%s', $prefix, $name) : $name;
                    $flatFields[] = [
                        'name' => $fullindex,
                        'required' => $input->getIsRequired(),
                        'type' => '[' . $this->getType($name, $input) . ']',
                        'description' => 'Array of ' . $name,
                        'embedded' => true
                    ];
                    if (in_array($name, $this->descripedObject)) {
                        continue;
                    }
                    array_push($this->descripedObject, $name);
                    $flatFields = array_merge($flatFields, $this->mapInputFields($input->getInputFilter(), $name));
                } elseif ($input instanceof InputFilter) {
                    if ($input instanceof MultiLanguage) {
                        $fullindex  = $prefix ? sprintf('%s'.self::OBJECT_DELIMITER.'%s', $prefix, $name) : $name;
                        $flatFields[] = [
                            'name' => $fullindex,
                            'required' => method_exists($input, 'isRequired') ? $input->isRequired() : false,
                            'type' => 'MultiLanguage',
                            'description' => 'Instance of MultiLanguage',
                            'embedded' => true
                        ];
                        $flatFields = array_merge($flatFields, $this->mapInputFields($input, 'MultiLanguage'));
                        continue;
                    }
                    $fullindex  = $prefix ? sprintf('%s'.self::OBJECT_DELIMITER.'%s', $prefix, $name) : $name;
                    $flatFields[] = [
                        'name' => $fullindex,
                        'required' => method_exists($input, 'isRequired') ? $input->isRequired() : false,
                        'type' => $this->getType($name, $input),
                        'description' => 'instance of ' . $name,
                        'embedded' => true
                    ];
                    $fullindex  = $prefix ? sprintf('%s'.self::OBJECT_DELIMITER.'%s', $prefix, $name) : $name;
                    $flatFields = array_merge($flatFields, $this->mapInputFields($input, $fullindex));
                }
            }
        }
        return $flatFields;
    }

    private function getType($name, $input) {
        return $name;
    }

    private function getNamePath($prefix, $name) {
        if ($prefix) {
            if (strpos($prefix, self::OBJECT_DELIMITER) !== false) {
                $parts = explode(self::OBJECT_DELIMITER, $prefix);
                $prefix = array_pop($parts);
            }
            $name = sprintf('%s'.self::OBJECT_DELIMITER.'%s',$prefix, $name);

        }
        return $name;
    }

    private function analyzeMultiLanguage(Input $input, $prefix, &$flat) {
        $chain = $input->getValidatorChain();
        foreach($chain->getValidators() as $validator) {
            if (isset($validator['instance']) && $validator['instance'] instanceof MultiLanguage) {
                $name = $input->getName();
                $flat[] = [
                    'name' => $this->getNamePath($prefix, $name),
                    'required' => $input->isRequired(),
                    'type' => 'MultiLanguage',
                    'description' => 'instance of MultiLanguage'
                ];
                $langs = $validator['instance']->getLanguages();
                foreach($langs as $lang) {
                    $flat[] = [
                        'name' => 'MultiLanguage' . self::OBJECT_DELIMITER . $lang,
                        'required' => $input->isRequired(),
                        'type' => 'string',
                    ];
                }
                return true;
            }
        }
        return false;
    }

    /**
     * @param Input $input
     * @param string $prefix
     * @param InputFilter $inputFilter
     * @return array
     */
    private function getFieldFromInput(Input $input, $prefix = '', InputFilter $inputFilter)
    {
        $name          = $input->getName();
        $documentation = [];
        if ($inputFilter instanceof DocumentedInputFilter) {
            $documentation = $inputFilter->getDocumentation($name);
        }

        return array_merge([
            'name' => $this->getNamePath($prefix, $name),
            'required' => $input->isRequired(),
        ], $documentation);
    }

    /**
     * @param array $fields
     * @param string $prefix To unwind nesting of fields
     * @return array
     */
    private function mapFields(array $fields, $prefix = '')
    {
        if (isset($fields['name'])) {
            /// detect usage of "name" as a field group name
            if (is_array($fields['name']) && isset($fields['name']['name'])) {
                return $this->mapFields($fields['name'], 'name');
            }

            if ($prefix) {
                $fields['name'] = $this->getNamePath($prefix, $fields['name']);
            }
            return [$fields];
        }

        $flatFields = [];

        foreach ($fields as $idx => $field) {
            if (isset($field['type']) && is_subclass_of($field['type'], 'Zend\InputFilter\InputFilterInterface')) {
                $filteredFields = array_diff_key($field, ['type' => 0]);
                $fullindex = $prefix ? sprintf('%s'.self::OBJECT_DELIMITER.'%s', $prefix, $idx) : $idx;
                $flatFields = array_merge($flatFields, $this->mapFields($filteredFields, $fullindex));
                continue;
            }

            $flatFields = array_merge($flatFields, $this->mapFields($field, $prefix));
        }

        return $flatFields;
    }

    /**
     * @param array $fieldData
     * @return Field
     */
    private function getField(array $fieldData)
    {
        $field = new Field();

        $field->setName($fieldData['name']);
        if (isset($fieldData['type'])) {
            $isMultiple = strpos($fieldData['type'], '[') === 0;
            $type = trim($fieldData['type'], '[]');
             if ($this->metadataService->altExists($type)) {
                $newType = 'string';
                $enum = $this->metadataService->getAlt($type);
                $field->setEnum(array_keys($enum));
            } else if ($this->metadataService->classExists($type)) {
                $newType = 'string';
                $this->addToDescription($fieldData, 'QName for ' . $type);
            } else {
                $newType = str_replace('xsd:','', $type);
                switch ($type) {
                    case 'xsd:anyUri':
                        $newType = 'string';
                        $this->addToDescription($fieldData, 'Any uri');
                        break;
                    case 'xsd:date':
                        $newType = 'string';
                        $this->addToDescription($fieldData, 'date string using ISO8601 format');
                        break;
                    case 'xsd:dateTime':
                        $newType = 'string';
                        $this->addToDescription($fieldData, 'dateTime string using ISO8601 format');
                        break;
                    case 'xsd:nonNegativeInteger':
                        $newType = 'integer';
                        $this->addToDescription($fieldData, 'Non-negative integer');
                        break;
                    case 'xsd:decimal':
                        $newType = 'number';
                        $this->addToDescription($fieldData, 'Decimal number');
                        break;
                    case 'rdfs:Resource':
                        $newType = 'string';
                        $this->addToDescription($fieldData, 'QName for resource');
                }
            }
            $field->setType($isMultiple ? '[' . $newType . ']' : $newType);
        }
        if (isset($fieldData['description'])) {
            $field->setDescription($fieldData['description']);
        }

        $required = isset($fieldData['required']) ? (bool) $fieldData['required'] : false;
        $field->setRequired($required);

        return $field;
    }

    private function addToDescription(&$data, $addition) {
        if (isset($data['description']) && empty(trim($data['description']))) {
            $data['description'] = $addition;
        } else {
            $data['description'] = trim($data['description'] . '. ' . $addition );
        }
    }

    protected function getStatusCodes($httpMethod, $hasOptionalSegments, $hasValidation, $requiresAuthorization)
    {
        $statusCodes = [
            ['code' => '406', 'message' => 'Not Acceptable', 'responseModel' => 'ErrorDocument'],
            ['code' => '415', 'message' => 'Unsupported Media Type', 'responseModel' => 'ErrorDocument'],
        ];

        switch ($httpMethod) {
            case 'GET':
                array_push($statusCodes, ['code' => '200', 'message' => 'OK', 'responseModel' => 'UserDocument']);
                if ($hasOptionalSegments) {
                    array_push($statusCodes, ['code' => '404', 'message' => 'Not Found', 'responseModel' => 'ErrorDocument']);
                }
                break;
            case 'DELETE':
                array_push($statusCodes, ['code' => '204', 'message' => 'No Content', 'responseModel' => 'UserDocument']);
                if ($hasOptionalSegments) {
                    array_push($statusCodes, ['code' => '404', 'message' => 'Not Found', 'responseModel' => 'ErrorDocument']);
                }
                break;
            case 'POST':
                array_push($statusCodes, ['code' => '201', 'message' => 'Created', 'responseModel' => 'UserDocument']);
                if ($hasOptionalSegments) {
                    array_push($statusCodes, ['code' => '404', 'message' => 'Not Found', 'responseModel' => 'ErrorDocument']);
                }
                if ($hasValidation) {
                    array_push($statusCodes, ['code' => '400', 'message' => 'Client Error', 'responseModel' => 'ErrorDocument']);
                    array_push($statusCodes, ['code' => '422', 'message' => 'Unprocessable Entity', 'responseModel' => 'ErrorDocument']);
                }
                break;
            case 'PATCH':
            case 'PUT':
                array_push($statusCodes, ['code' => '200', 'message' => 'OK', 'responseModel' => 'UserDocument']);
                if ($hasOptionalSegments) {
                    array_push($statusCodes, ['code' => '404', 'message' => 'Not Found', 'responseModel' => 'ErrorDocument']);
                }
                if ($hasValidation) {
                    array_push($statusCodes, ['code' => '400', 'message' => 'Client Error', 'responseModel' => 'ErrorDocument']);
                    array_push($statusCodes, ['code' => '422', 'message' => 'Unprocessable Entity', 'responseModel' => 'ErrorDocument']);
                }
                break;
        }

        if ($requiresAuthorization) {
            array_push($statusCodes, ['code' => '401', 'message' => 'Unauthorized', 'responseModel' => 'ErrorDocument']);
            array_push($statusCodes, ['code' => '403', 'message' => 'Forbidden', 'responseModel' => 'ErrorDocument']);
        }
        usort($statusCodes, function($a, $b) {
            return $a['code'] > $b['code'];
        });

        return $statusCodes;
    }
}
