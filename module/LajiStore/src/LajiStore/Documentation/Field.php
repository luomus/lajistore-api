<?php
/**
 * Created by PhpStorm.
 * User: Localadmin_vixriihi
 * Date: 5.4.2016
 * Time: 12:52
 */

namespace LajiStore\Documentation;


class Field extends \ZF\Apigility\Documentation\Field
{
    /**
     * @var array
     */
    protected $enum;

    /**
     * @return array
     */
    public function getEnum()
    {
        return $this->enum;
    }

    /**
     * @param array $enum
     */
    public function setEnum(array $enum)
    {
        $this->enum = $enum;
    }

    /**
     * Cast object to array
     *
     * @return array
     */
    public function toArray()
    {
        $data = [
            'description' => $this->description,
            'required' => $this->required,
            'type' => $this->type
        ];
        if ($this->enum) {
            $data['enum'] = $this->enum;
        }
        return $data;
    }


}