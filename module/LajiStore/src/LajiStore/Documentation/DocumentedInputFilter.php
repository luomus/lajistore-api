<?php

namespace LajiStore\Documentation;


interface DocumentedInputFilter
{
    /**
     * Return true if the field has description or type
     *
     * @param $field
     * @return boolean
     */
    public function hasDocumentation($field);

    /**
     * Returns the documentation for the given field
     *
     * @param $field
     * @return array
     */
    public function getDocumentation($field);

    /**
     * Returns the field type
     *
     * @param $field
     * @return string
     */
    public function getInputType($field);

    /**
     * Sets the fields type
     *
     * @param $field
     * @param $type
     * @return mixed
     */
    public function setInputType($field, $type);

    /**
     * Returns the field description
     *
     * @param $field
     * @return string
     */
    public function getInputDescription($field);

    /**
     * Sets the fields description
     *
     * @param $field
     * @param $description
     * @return mixed
     */
    public function setInputDescription($field, $description);
}