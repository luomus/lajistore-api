<?php

namespace LajiStore\Documentation;


trait DocumentedInputFilterTrait
{
    protected $documentation = [];

    public function hasDocumentation($field) {
        return isset($this->documentation[$field]);
    }

    public function getDocumentation($field) {
        return isset($this->documentation[$field]) ? $this->documentation[$field] : [];
    }

    public function getInputType($field) {
        return isset($this->documentation[$field]) && isset($this->documentation[$field]['type']) ?
            $this->documentation[$field]['type'] :
            null;
    }

    public function setInputType($field, $type) {
        $this->documentation[$field]['type'] = $type;
    }

    public function getInputDescription($field) {
        return isset($this->documentation[$field]) && isset($this->documentation[$field]['description']) ?
            $this->documentation[$field]['description'] :
            null;
    }

    public function setInputDescription($field, $description) {
        $this->documentation[$field]['description'] = $description;
    }

    public function add($input, $name = NULL) {
        if (is_array($input)) {
            $field = isset($input['name']) ? $input['name'] : null;
            if (isset($input['inputType'])) {
                $this->setInputType($field, $input['inputType']);
            }
            if (isset($input['description'])) {
                $this->setInputDescription($field, $input['description']);
            }
        }
        parent::add($input, $name);
    }
}