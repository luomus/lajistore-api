<?php

namespace LajiStore\Documentation;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ApiFactoryFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $services)
    {
        return $this($services, ApiFactory::class);
    }

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $container->get('Triplestore\ObjectManager');
        return new ApiFactory(
            $container->get('ModuleManager'),
            $container->get('Config'),
            $container->get('ZF\Configuration\ModuleUtils'),
            $container->get('InputFilterManager'),
            $om->getMetadataService()
        );
    }
}