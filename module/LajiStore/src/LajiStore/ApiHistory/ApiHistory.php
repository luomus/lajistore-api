<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace LajiStore\ApiHistory;

/**
 * Object describing an API-History payload
 */
class ApiHistory
{

    /**
     * Title of the error.
     *
     * @var string
     */
    protected $title;

    protected $versionList;

    protected $src;

    protected $dst;

    /**
     * Constructor
     *
     * Create an instance using the provided information.
     *
     * @param array  $versionList
     * @param array $src
     * @param array $dst
     */
    public function __construct(array $versionList = null, array $src = null, array $dst = null)
    {

        $this->versionList = $versionList;
        $this->src         = $src;
        $this->dst         = $dst;
    }

    /**
     * @return array
     */
    public function getVersionList()
    {
        return $this->versionList;
    }

    /**
     * @param array $versionList
     */
    public function setVersionList($versionList)
    {
        $this->versionList = $versionList;
    }

    /**
     * @return array
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @param array $src
     */
    public function setSrc($src)
    {
        $this->src = $src;
    }

    /**
     * @return array
     */
    public function getDst()
    {
        return $this->dst;
    }

    /**
     * @param array $dst
     */
    public function setDst($dst)
    {
        $this->dst = $dst;
    }

    public function hasVersionList() {
        return is_array($this->versionList);
    }

    /**
     * Cast to an array
     *
     * @return array
     */
    public function toArray()
    {
        if ($this->hasVersionList()) {
            return [
                'versions' => $this->versionList
            ];
        }
        return [
            'from' => $this->src,
            'to' => $this->dst
        ];
    }

}
