<?php

namespace LajiStore\ApiHistory;

use mikemccabe\JsonPatch\JsonPatch;
use Zend\Http\Response;

class ApiHistoryResponse extends Response
{
    /**
     * @var ApiHistory
     */
    protected $apiHistory;

    /**
     * Flags to use with json_encode
     *
     * @var int
     */
    protected $jsonFlags = 0;

    /**
     * @param ApiHistory $apiHistory
     */
    public function __construct(ApiHistory $apiHistory)
    {
        $this->apiHistory = $apiHistory;

        if (defined('JSON_UNESCAPED_SLASHES')) {
            $this->jsonFlags = constant('JSON_UNESCAPED_SLASHES');
        }
    }

    /**
     * @return ApiHistory
     */
    public function getApiHistory()
    {
        return $this->apiHistory;
    }

    /**
     * Retrieve the content
     *
     * Serializes the composed ApiHistory instance to JSON.
     *
     * @return string
     */
    public function getContent()
    {
        if ($this->apiHistory->hasVersionList()) {
            return json_encode($this->apiHistory->toArray(), $this->jsonFlags);
        }
        $diff = JsonPatch::diff(
            $this->apiHistory->getSrc(),
            $this->apiHistory->getDst()
        );
        return json_encode($diff, $this->jsonFlags);
    }

    /**
     * Retrieve headers
     *
     * Proxies to parent class, but then checks if we have an content-type
     * header; if not, sets it, with a value of "application/problem+json".
     *
     * @return \Zend\Http\Headers
     */
    public function getHeaders()
    {
        $headers = parent::getHeaders();
        if (!$headers->has('content-type')) {
            if ($this->apiHistory->hasVersionList()) {
                $headers->addHeaderLine('content-type', 'application/json');
            } else {
                $headers->addHeaderLine('content-type', 'application/json-patch+json');
            }
        }
        return $headers;
    }
}
