<?php

namespace LajiStore\Mapper;


/**
 * Interface for StatusLib mappers
 */
interface MapperInterface
{
    /**
     * @param array|\Traversable|\stdClass $data
     * @return mixed
     */
    public function create($data);

    /**
     * @param string $id
     * @return mixed
     */
    public function fetch($id);

    /**
     * @param array $params
     * @return mixed
     */
    public function fetchAll($params);

    /**
     * @param string $id
     * @param array|\Traversable|\stdClass $data
     * @return mixed
     */
    public function update($id, $data);

    /**
     * @param string $id
     * @return bool
     */
    public function delete($id);

    /**
     * @param mixed $data
     * @return bool
     */
    public function deleteAll($data = null);
}