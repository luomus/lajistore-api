<?php
return array(
    'LajiStore\\V1\\Rpc\\Ping\\Controller' => array(
        'GET' => array(
            'response' => '{
   "ack": "Acknowledges request by returning current timestamp"
}',
        ),
        'description' => 'Heartbeat service to check that the server is alive',
    ),
    'LajiStore\\V1\\Rpc\\Status\\Controller' => array(
        'GET' => array(
            'response' => '{
   "status": "Current status of the system"
}',
        ),
        'description' => 'Heartbeat service to check that the server is working as expected',
    ),
    'LajiStore\\V1\\Rest\\News\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns news and the needed metadata to go through the paged collection.',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new news.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes all the newses. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns single news with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates the whole single news with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes the news with the specified id.',
            ),
        ),
        'description' => 'REST service to deal with news',
    ),
    'LajiStore\\V1\\Rest\\FormPermissionSingle\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns form permissions and the needed metadata to go through the paged collection.',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new form permission single.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes all the form permission singles. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns form permission with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates the whole form permission with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes the form permission with the specified id.',
            ),
        ),
        'description' => 'REST service to deal with form permissions',
    ),
    'LajiStore\\V1\\Rest\\Notification\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns notifications and the needed metadata to go through the paged collection.',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new notification.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes all the notifications. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns notification with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates the whole notification with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes the notification with the specified id.',
            ),
        ),
        'description' => 'REST service to deal with form permissions',
    ),
    'LajiStore\\V1\\Rest\\Annotation\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns annotation and the needed metadata to go through the paged collection.',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new annotation.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes all the annotations. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns single annotation with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates the whole single annotation with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes the annotation with the specified id.',
            ),
        ),
        'description' => 'REST service to deal with annotations',
    ),
    'LajiStore\\V1\\Rest\\NamedPlace\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns named place and the needed metadata to go through the paged collection.',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new named place.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes all the named place. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns single named place with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates the whole single named place with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes the named place with the specified id.',
            ),
        ),
        'description' => 'REST service to deal with named places',
    ),
    'LajiStore\\V1\\Rest\\Device\\Controller' => array(
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns devices and the needed metadata to go through the paged collection.',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new device.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes all the devices. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns single device with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates the whole single device with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes the device with the specified id.',
            ),
        ),
        'description' => 'REST service to deal with satellite tracking devices',
    ),
    'LajiStore\\V1\\Rest\\Document\\Controller' => array(
        'description' => 'REST service to deal with documents',
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns documents and the needed metadata to go through the paged collection.',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new document.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes all the documents. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns single document with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates the whole single document with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes the document with the specified id.',
            ),
        ),
    ),
    'LajiStore\\V1\\Rest\\Individual\\Controller' => array(
        'description' => 'REST service to deal with individuals',
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns individuals and the needed metadata to go through the paged collection.',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new individual.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes all the individuals. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns single individual with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates the whole single individual with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes the individual with the specified id.',
            ),
        ),
    ),
    'LajiStore\\V1\\Rest\\DeviceIndividual\\Controller' => array(
        'description' => 'REST service to deal with linking devices and individuals',
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns all linking object and the needed metadata to go through the paged collection.',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new link.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes all the links. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns single link with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates the whole single link with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes the link with the specified id.',
            ),
        ),
    ),
    'LajiStore\\V1\\Rest\\Form\\Controller' => array(
        'description' => 'REST service to deal with forms',
        'collection' => array(
            'GET' => array(
                'request' => '',
                'description' => 'Returns all forms and the needed metadata to go through the paged collection.
                Please note that this is just partly isolated resource. Meaning that the you\'ll be able to
                see other systems forms but you wont be able to modify or delete them',
            ),
            'POST' => array(
                'request' => '',
                'description' => 'Adds a new form.',
                'response' => '',
            ),
            'DELETE' => array(
                'description' => 'Deletes systems all forms. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => '',
                'description' => 'Returns single form with the specified id.',
            ),
            'PUT' => array(
                'description' => 'Updates systems whole form with the specified id.',
                'request' => '',
            ),
            'description' => '',
            'DELETE' => array(
                'description' => 'Deletes systems form with the specified id.',
            ),
        ),
    ),
    'LajiStore\\V1\\Rest\\Profile\\Controller' => array(
        'description' => 'REST service to deal with profiles',
        'collection' => array(
            'GET' => array(
                'response' => 'Profile',
                'description' => 'Returns all profiles and the needed metadata to go through the paged collection.'
            ),
            'POST' => array(
                'request' => 'Profile',
                'description' => 'Adds a new profile.',
                'response' => 'Profile',
            ),
            'DELETE' => array(
                'description' => 'Deletes systems all profiles. Only usable for testing users',
            ),
        ),
        'entity' => array(
            'GET' => array(
                'response' => 'Profile',
                'description' => 'Returns single profile with the specified id.',
            ),
            'PUT' => array(
                'response' => 'Profile',
                'description' => 'Updates whole profile with the specified id.',
                'request' => 'Profile',
            ),
            'DELETE' => array(
                'description' => 'Deletes profile with the specified id.',
            ),
        ),
    ),
);
