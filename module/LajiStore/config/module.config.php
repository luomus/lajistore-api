<?php
return array(
    'controllers' => array(
        'factories' => array(
            'LajiStore\\V1\\Rpc\\Ping\\Controller' => 'LajiStore\\V1\\Rpc\\Ping\\PingControllerFactory',
            'LajiStore\\V1\\Rpc\\Status\\Controller' => 'LajiStore\\V1\\Rpc\\Status\\StatusControllerFactory',
            'LajiStore\\V1\\Rpc\\UpdateFilters\\Controller' => 'LajiStore\\V1\\Rpc\\UpdateFilters\\UpdateFiltersControllerFactory',
            LajiStore\Controller\CliController::class => LajiStore\Controller\CliControllerFactory::class,
        ),
    ),
    'console'   => array(
        'router' => array(
            'routes' => array(
                'generate-filters' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'generate-filters',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'generateFilters'
                        ),
                    ),
                ),
                'delete-from-warehouse' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'delete-from-warehouse [<id>]',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'deleteFromWarehouse'
                        ),
                    ),
                ),
                'copy-to-elastic' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'copy-to-elastic [<id>]',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'copyToElastic'
                        ),
                    ),
                ),
                'check-elastic-content' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'check-elastic-content',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'checkElasticContent'
                        ),
                    ),
                ),
                'upgrade-annotations' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'upgrade-annotations',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'upgradeAnnotations'
                        ),
                    ),
                ),
                'send-to-warehouse' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'send-to-warehouse [<id>]',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'sendToWarehouse'
                        ),
                    ),
                ),
                'send-annotation-to-warehouse' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'send-annotation-to-warehouse [<id>]',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'sendAnnotationToWarehouse'
                        ),
                    ),
                ),
                'rebuildSearch' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'rebuild document search <source>',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'rebuildDocumentSearchIndex'
                        ),
                    ),
                ),
                'editedTime' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'update dateEdited <source>',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'updateEditedDate'
                        ),
                    ),
                ),
                'rebuildNPSearch' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'rebuild namedPlace search <source>',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'rebuildNamedPlaceSearchIndex'
                        ),
                    ),
                ),
                'removeDuplicateAnnotations' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'delete-duplicate-annotations [--dry-run]',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'deleteDuplicateAnnotations'
                        ),
                    ),
                ),
                'addMissingSource' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'add-missing-source [--dry-run] <source>',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'addMissingSource'
                        ),
                    ),
                ),
                'resetNsCoordinates' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'reset-named-place-coordinates [--dry-run] <source> <id> [<version>]',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'resetNamedPlaceCoordinates'
                        ),
                    ),
                ),
                'fix' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'fix',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'fix'
                        ),
                    ),
                ),
                'integrate' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'integrate <source>',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'integrate'
                        ),
                    ),
                ),
                'autoLinkUsers' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'auto-link-users',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'autoLinkUsers'
                        ),
                    ),
                ),
                'linkUsers' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'link user <source> <target> [--dry-run] [--leg-to=] [--leg-from=]',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'linkUser'
                        ),
                    ),
                ),
                'update' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'update [--dry-run] <source>',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'update'
                        ),
                    ),
                ),
                'updateNS' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'updateNamedPlace [--dry-run]',
                        'defaults' => array(
                            'controller' => LajiStore\Controller\CliController::class,
                            'action'     => 'updateNS'
                        ),
                    ),
                ),
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            'zf-apigility' => array(
                'type' => 'Zend\\Mvc\\Router\\Http\\Literal',
                'options' => array(
                    'route' => '/documentation',
                ),
                'may_terminate' => false,
                'child_routes' => array(
                    'documentation' => array(
                        'type' => 'Zend\\Mvc\\Router\\Http\\Segment',
                        'options' => array(
                            'route' => '/[:api[-v:version][/:service]]',
                            'constraints' => array(
                                'api' => '[a-zA-Z][a-zA-Z0-9_]+',
                            ),
                            'defaults' => array(
                                'controller' => 'ZF\\Apigility\\Documentation\\Controller',
                                'action' => 'show',
                            ),
                        ),
                    ),
                ),
            ),
            'laji-store.rpc.status' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/lajistore-status',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rpc\\Status\\Controller',
                        'action' => 'status',
                    ),
                ),
            ),
            'laji-store.rpc.ping' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/ping',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rpc\\Ping\\Controller',
                        'action' => 'ping',
                    ),
                ),
            ),
            'laji-store.rest.news' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/news[/:news_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\News\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.namedPlace' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/namedPlaces[/:namedPlace_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\NamedPlace\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.annotation' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/annotations[/:annotation_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\Annotation\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.formPermissionSingle' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/formPermissions[/:permission_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\FormPermissionSingle\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.notification' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/notifications[/:notification_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\Notification\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.device' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/devices[/:device_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\Device\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.deviceIndividual' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/deviceIndividuals[/:device_individual_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\DeviceIndividual\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.document' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/documents[/:document_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\Document\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.individual' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/individuals[/:individual_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\Individual\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.form' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/forms[/:form_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\Form\\Controller',
                    ),
                ),
            ),
            'laji-store.rest.profile' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/profiles[/:profile_id]',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rest\\Profile\\Controller',
                    ),
                ),
            ),
            'laji-store.rpc.update-filters' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/update-filters',
                    'defaults' => array(
                        'controller' => 'LajiStore\\V1\\Rpc\\UpdateFilters\\Controller',
                        'action' => 'updateFilters',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'laji-store.rpc.status',
            1 => 'laji-store.rpc.ping',
            2 => 'laji-store.rest.device',
            3 => 'laji-store.rest.document',
            4 => 'laji-store.rest.individual',
            5 => 'laji-store.rest.form',
            6 => 'laji-store.rpc.update-filters',
            7 => 'laji-store.rpc.deviceIndividual',
            8 => 'laji-store.rpc.news',
            9 => 'laji-store.rest.profile',
            10 => 'laji-store.rest.namedPlace',
            11 => 'laji-store.rest.annotation',
            12 => 'laji-store.rest.notification',
        ),
        'default_version' => 1,
    ),
    'zf-rpc' => array(
        'LajiStore\\V1\\Rpc\\Ping\\Controller' => array(
            'service_name' => 'Ping',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'laji-store.rpc.ping',
        ),
        'LajiStore\\V1\\Rpc\\Status\\Controller' => array(
            'service_name' => 'Status',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'laji-store.rpc.status',
        ),
        'LajiStore\\V1\\Rpc\\UpdateFilters\\Controller' => array(
            'service_name' => 'UpdateFilters',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'laji-store.rpc.update-filters',
            'hide_doc' => true,
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'LajiStore\\V1\\Rpc\\Ping\\Controller' => 'Json',
            'LajiStore\\V1\\Rpc\\Status\\Controller' => 'Json',
            'LajiStore\\V1\\Rest\\News\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rest\\NamedPlace\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rest\\Annotation\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rest\\FormPermissionSingle\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rest\\Notification\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rest\\Device\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rest\\Document\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rest\\Individual\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rest\\Form\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rest\\Profile\\Controller' => 'JsonLD',
            'LajiStore\\V1\\Rpc\\UpdateFilters\\Controller' => 'Json',
        ),
        'accept_whitelist' => array(
            'LajiStore\\V1\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'LajiStore\\V1\\Rpc\\Status\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'LajiStore\\V1\\Rest\\News\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\NamedPlace\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\Annotation\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\FormPermissionSingle\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\Notification\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\Device\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\DeviceIndividual\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\Document\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\Individual\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\Form\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rest\\Profile\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/ld+json',
            ),
            'LajiStore\\V1\\Rpc\\UpdateFilters\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
        ),
        'content_type_whitelist' => array(
            'LajiStore\\V1\\Rpc\\Ping\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rpc\\Status\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\DeviceIndividual\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\News\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\NamedPlace\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\Annotation\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\FormPermissionSingle\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\Notification\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\Device\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\Document\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\Individual\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\Form\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rest\\Profile\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
            'LajiStore\\V1\\Rpc\\UpdateFilters\\Controller' => array(
                0 => 'application/vnd.laji-store.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-content-validation' => array(
        'LajiStore\\V1\\Rpc\\Ping\\Controller' => array(
            'input_filter' => 'LajiStore\\V1\\Rpc\\Ping\\Validator',
        ),
        'LajiStore\\V1\\Rpc\\Status\\Controller' => array(
            'input_filter' => 'LajiStore\\V1\\Rpc\\Status\\Validator',
        ),
        'LajiStore\\V1\\Rest\\News\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\News',
        ),
        'LajiStore\\V1\\Rest\\DeviceIndividual\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\DeviceIndividual',
        ),
        'LajiStore\\V1\\Rest\\NamedPlace\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\NamedPlaceBase',
        ),
        'LajiStore\\V1\\Rest\\Annotation\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\AnnotationBase',
        ),
        'LajiStore\\V1\\Rest\\FormPermissionSingle\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\FormPermissionSingleBase',
        ),
        'LajiStore\\V1\\Rest\\Notification\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\NotificationBase',
        ),
        'LajiStore\\V1\\Rest\\Device\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\DeviceBase',
        ),
        'LajiStore\\V1\\Rest\\Document\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\DocumentBase',
        ),
        'LajiStore\\V1\\Rest\\Individual\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\Individual',
        ),
        'LajiStore\\V1\\Rest\\Form\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\Form',
        ),
        'LajiStore\\V1\\Rest\\Profile\\Controller' => array(
            'allows_only_fields_in_filter' => true,
            'input_filter' => 'LajiStore\\V1\\InputFilter\\Profile',
        ),
    ),
    'input_filter_specs' => array(
        'LajiStore\\V1\\Rpc\\Ping\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'ack',
                'description' => 'Acknowledges request by returning current timestamp',
            ),
        ),
        'LajiStore\\V1\\Rest\\Individual\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'individualId',
                'description' => 'Id for the individual',
            ),
            1 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'taxon',
                'description' => 'Name or MX code for the individual',
            ),
        ),
        'LajiStore\\V1\\Rpc\\Truncate\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'truncated',
                'description' => 'List of tables truncated',
            ),
        ),
    ),
    'zf-rest' => array(
        'LajiStore\\V1\\Rest\\News\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\News\\NewsResource',
            'route_name' => 'laji-store.rest.news',
            'route_identifier_name' => 'news_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'page_size',
                1 => 'q',
            ),
            'page_size' => 20,
            'max_page_size' => 1000,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\News\\NewsEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\News\\NewsCollection',
            'service_name' => 'News',
        ),
        'LajiStore\\V1\\Rest\\NamedPlace\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\NamedPlace\\NamedPlaceResource',
            'route_name' => 'laji-store.rest.namedPlace',
            'route_identifier_name' => 'namedPlace_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'page_size',
                1 => 'q',
            ),
            'page_size' => 20,
            'max_page_size' => 10000,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\NamedPlace\\NamedPlaceEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\NamedPlace\\NamedPlaceCollection',
            'service_name' => 'NamedPlace',
        ),
        'LajiStore\\V1\\Rest\\Annotation\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\Annotation\\AnnotationResource',
            'route_name' => 'laji-store.rest.annotation',
            'route_identifier_name' => 'annotation_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'page_size',
                1 => 'q',
            ),
            'page_size' => 20,
            'max_page_size' => 100,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\Annotation\\AnnotationEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\Annotation\\AnnotationCollection',
            'service_name' => 'Annotation',
        ),
        'LajiStore\\V1\\Rest\\FormPermissionSingle\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\FormPermissionSingle\\FormPermissionSingleResource',
            'route_name' => 'laji-store.rest.formPermissionSingle',
            'route_identifier_name' => 'permission_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'page_size',
                1 => 'q',
            ),
            'page_size' => 20,
            'max_page_size' => 1000,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\FormPermissionSingle\\FormPermissionSingleEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\FormPermissionSingle\\FormPermissionSingleCollection',
            'service_name' => 'FormPermission',
        ),
        'LajiStore\\V1\\Rest\\Notification\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\Notification\\NotificationResource',
            'route_name' => 'laji-store.rest.notification',
            'route_identifier_name' => 'notification_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'page_size',
                1 => 'q',
            ),
            'page_size' => 20,
            'max_page_size' => 100,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\Notification\\NotificationEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\Notification\\NotificationCollection',
            'service_name' => 'Notification',
        ),
        'LajiStore\\V1\\Rest\\Device\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\Device\\DeviceResource',
            'route_name' => 'laji-store.rest.device',
            'route_identifier_name' => 'device_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'page_size',
                1 => 'q',
            ),
            'page_size' => 20,
            'max_page_size' => 100,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\Device\\DeviceEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\Device\\DeviceCollection',
            'service_name' => 'Device',
        ),
        'LajiStore\\V1\\Rest\\DeviceIndividual\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\DeviceIndividual\\DeviceIndividualResource',
            'route_name' => 'laji-store.rest.deviceIndividual',
            'route_identifier_name' => 'device_individual_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'page_size',
                1 => 'q',
            ),
            'page_size' => 20,
            'max_page_size' => 100,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\DeviceIndividual\\DeviceIndividualEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\DeviceIndividual\\DeviceIndividualCollection',
            'service_name' => 'DeviceIndividual',
        ),
        'LajiStore\\V1\\Rest\\Document\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\Document\\DocumentResource',
            'route_name' => 'laji-store.rest.document',
            'route_identifier_name' => 'document_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'page_size',
                1 => 'q',
                2 => 'filter',
            ),
            'page_size' => 1000,
            'max_page_size' => 10000,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\Document\\DocumentEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\Document\\DocumentCollection',
            'service_name' => 'Document',
        ),
        'LajiStore\\V1\\Rest\\Individual\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\Individual\\IndividualResource',
            'route_name' => 'laji-store.rest.individual',
            'route_identifier_name' => 'individual_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(
                0 => 'page_size',
                1 => 'q',
            ),
            'page_size' => 25,
            'max_page_size' => 1000,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\Individual\\IndividualEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\Individual\\IndividualCollection',
            'service_name' => 'Individual',
        ),
        'LajiStore\\V1\\Rest\\Form\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\Form\\FormResource',
            'route_name' => 'laji-store.rest.form',
            'route_identifier_name' => 'form_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'max_page_size' => 100,
            'page_size_param' => 'page_size',
            'entity_class' => 'LajiStore\\V1\\Rest\\Form\\FormEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\Form\\FormCollection',
            'service_name' => 'Form',
        ),
        'LajiStore\\V1\\Rest\\Profile\\Controller' => array(
            'listener' => 'LajiStore\\V1\\Rest\\Profile\\ProfileResource',
            'route_name' => 'laji-store.rest.profile',
            'route_identifier_name' => 'profile_id',
            'controller_class' => 'ZF\\JsonLD\\RestController',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PUT',
                2 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
                2 => 'DELETE',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'max_page_size' => 100,
            'page_size_param' => null,
            'entity_class' => 'LajiStore\\V1\\Rest\\Profile\\ProfileEntity',
            'collection_class' => 'LajiStore\\V1\\Rest\\Profile\\ProfileCollection',
            'service_name' => 'Profile',
        ),
    ),
    'zf-jsonld' => array(
        'metadata_map' => array(
            'LajiStore\\V1\\Rest\\News\\NewsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.news',
                'route_identifier_name' => 'news_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/news.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\NamedPlace\\NamedPlaceEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.namedPlace',
                'route_identifier_name' => 'namedPlace_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/namedPlace.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\Annotation\\AnnotationEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.annotation',
                'route_identifier_name' => 'annotation_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/annotation.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\FormPermissionSingle\\FormPermissionSingleEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.formPermissionSingle',
                'route_identifier_name' => 'permission_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/formPermissionSingle.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\Notification\\NotificationEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.notification',
                'route_identifier_name' => 'notification_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/notification.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\Device\\DeviceEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.device',
                'route_identifier_name' => 'device_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/device.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\DeviceIndividual\\DeviceIndividualEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.deviceIndividual',
                'route_identifier_name' => 'device_individual_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/deviceIndividual.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\NamedPlace\\NamedPlaceCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.namedPlace',
                'route_identifier_name' => 'namedPlace_id',
                'is_collection' => true,
            ),
            'LajiStore\\V1\\Rest\\Annotation\\AnnotationCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.annotation',
                'route_identifier_name' => 'annotation_id',
                'is_collection' => true,
            ),
            'LajiStore\\V1\\Rest\\FormPermissionSingle\\FormPermissionSingleCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.formPermissionSingle',
                'route_identifier_name' => 'permission_id',
                'is_collection' => true,
            ),
            'LajiStore\\V1\\Rest\\Notification\\NotificationCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.notification',
                'route_identifier_name' => 'notification_id',
                'is_collection' => true,
            ),
            'LajiStore\\V1\\Rest\\Device\\DeviceCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.device',
                'route_identifier_name' => 'device_id',
                'is_collection' => true,
            ),
            'LajiStore\\V1\\Rest\\Document\\DocumentEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.document',
                'route_identifier_name' => 'document_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/document.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\Document\\DocumentCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.document',
                'route_identifier_name' => 'document_id',
                'is_collection' => true,
            ),
            'LajiStore\\V1\\Rest\\Individual\\IndividualEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.individual',
                'route_identifier_name' => 'individual_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/individual.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\Individual\\IndividualCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.individual',
                'route_identifier_name' => 'individual_id',
                'is_collection' => true,
            ),
            'LajiStore\\V1\\Rest\\Form\\FormEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.form',
                'route_identifier_name' => 'form_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/form.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\Form\\FormCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.form',
                'route_identifier_name' => 'form_id',
                'is_collection' => true,
            ),
            'LajiStore\\V1\\Rest\\Profile\\ProfileEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.profile',
                'route_identifier_name' => 'profile_id',
                'hydrator' => 'Zend\\Hydrator\\ObjectProperty',
                'properties' => array(
                    0 => array(
                        'key' => '@context',
                        'value' => 'http://schema.laji.fi/context/profile.jsonld',
                    ),
                ),
            ),
            'LajiStore\\V1\\Rest\\Profile\\ProfileCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'laji-store.rest.profile',
                'route_identifier_name' => 'profile_id',
                'is_collection' => true,
            ),
        ),
    ),
    'service_manager' => array(
        'invokables' => array(),
        'factories' => array(
            'LajiStore\\ApiHistory\\Listener\\SendApiHistoryResponseListener' => 'LajiStore\\ApiHistory\\Factory\\SendApiHistoryResponseListenerFactory',
            'LajiStore\\V1\\Rest\\News\\NewsResource' => 'LajiStore\\V1\\Rest\\News\\NewsResourceFactory',
            'LajiStore\\V1\\Rest\\News\\NewsMapper' => 'LajiStore\\V1\\Rest\\News\\NewsMapperFactory',
            'LajiStore\\V1\\Rest\\NamedPlace\\NamedPlaceResource' => 'LajiStore\\V1\\Rest\\NamedPlace\\NamedPlaceResourceFactory',
            'LajiStore\\V1\\Rest\\NamedPlace\\NamedPlaceMapper' => 'LajiStore\\V1\\Rest\\NamedPlace\\NamedPlaceMapperFactory',
            'LajiStore\\V1\\Rest\\Annotation\\AnnotationResource' => 'LajiStore\\V1\\Rest\\Annotation\\AnnotationResourceFactory',
            'LajiStore\\V1\\Rest\\FormPermissionSingle\\FormPermissionSingleResource' => 'LajiStore\\V1\\Rest\\FormPermissionSingle\\FormPermissionSingleResourceFactory',
            'LajiStore\\V1\\Rest\\Notification\\NotificationResource' => 'LajiStore\\V1\\Rest\\Notification\\NotificationResourceFactory',
            'LajiStore\\V1\\Rest\\Annotation\\AnnotationMapper' => 'LajiStore\\V1\\Rest\\Annotation\\AnnotationMapperFactory',
            'LajiStore\\V1\\Rest\\FormPermissionSingle\\FormPermissionSingleMapper' => 'LajiStore\\V1\\Rest\\FormPermissionSingle\\FormPermissionSingleMapperFactory',
            'LajiStore\\V1\\Rest\\Notification\\NotificationMapper' => 'LajiStore\\V1\\Rest\\Notification\\NotificationMapperFactory',
            'LajiStore\\V1\\Rest\\Device\\DeviceResource' => 'LajiStore\\V1\\Rest\\Device\\DeviceResourceFactory',
            'LajiStore\\V1\\Rest\\Device\\DeviceMapper' => 'LajiStore\\V1\\Rest\\Device\\DeviceMapperFactory',
            'LajiStore\\V1\\Rest\\DeviceIndividual\\DeviceIndividualResource' => 'LajiStore\\V1\\Rest\\DeviceIndividual\\DeviceIndividualResourceFactory',
            'LajiStore\\V1\\Rest\\DeviceIndividual\\DeviceIndividualMapper' => 'LajiStore\\V1\\Rest\\DeviceIndividual\\DeviceIndividualMapperFactory',
            'LajiStore\\V1\\Rest\\Document\\DocumentResource' => 'LajiStore\\V1\\Rest\\Document\\DocumentResourceFactory',
            'LajiStore\\V1\\Rest\\Document\\DocumentMapper' => 'LajiStore\\V1\\Rest\\Document\\DocumentMapperFactory',
            'LajiStore\\V1\\Rest\\Individual\\IndividualResource' => 'LajiStore\\V1\\Rest\\Individual\\IndividualResourceFactory',
            'LajiStore\\V1\\Rest\\Individual\\IndividualMapper' => 'LajiStore\\V1\\Rest\\Individual\\IndividualMapperFactory',
            'LajiStore\\V1\\Rest\\Form\\FormResource' => 'LajiStore\\V1\\Rest\\Form\\FormResourceFactory',
            'LajiStore\\V1\\Rest\\Form\\FormMapper' => 'LajiStore\\V1\\Rest\\Form\\FormMapperFactory',
            'LajiStore\\V1\\Rest\\Profile\\ProfileResource' => 'LajiStore\\V1\\Rest\\Profile\\ProfileResourceFactory',
            'LajiStore\\V1\\Rest\\Profile\\ProfileMapper' => 'LajiStore\\V1\\Rest\\Profile\\ProfileMapperFactory',
            \LajiStore\Service\ElasticService::class => \LajiStore\Service\ElasticServiceFactory::class,
            'ZF\\Apigility\\Documentation\\ApiFactory' => 'LajiStore\\Documentation\\ApiFactoryFactory',
            'ObjectExistsChecker' => 'LajiStore\\Validator\\ObjectCheckerFactory',
            'logger' => 'LajiStore\\Service\\LoggerFactory'
        ),
        'abstract_factories' => array(
            'Zend\\Db\\Adapter\\Adapter' => 'Zend\\Db\\Adapter\\AdapterAbstractServiceFactory',
        ),
    ),
    'input_filters' => array(
        'invokables' => array(
            'LajiStore\\V1\\InputFilter\\News' => 'LajiStore\\V1\\InputFilter\\News',
            'LajiStore\\V1\\InputFilter\\Identification' => 'LajiStore\\V1\\InputFilter\\Identification',
            'LajiStore\\V1\\InputFilter\\TypeSpecimen' => 'LajiStore\\V1\\InputFilter\\TypeSpecimen',
            'LajiStore\\V1\\InputFilter\\Unit' => 'LajiStore\\V1\\InputFilter\\Unit',
            'LajiStore\\V1\\InputFilter\\UnitGathering' => 'LajiStore\\V1\\InputFilter\\UnitGathering',
            'LajiStore\\V1\\InputFilter\\Gathering' => 'LajiStore\\V1\\InputFilter\\Gathering',
            'LajiStore\\V1\\InputFilter\\GatheringEvent' => 'LajiStore\\V1\\InputFilter\\GatheringEvent',
            'LajiStore\\V1\\InputFilter\\DocumentBase' => 'LajiStore\\V1\\InputFilter\\DocumentBase',
            'LajiStore\\V1\\InputFilter\\NamedPlaceBase' => 'LajiStore\\V1\\InputFilter\\NamedPlaceBase',
            'LajiStore\\V1\\InputFilter\\AnnotationBase' => 'LajiStore\\V1\\InputFilter\\AnnotationBase',
            'LajiStore\\V1\\InputFilter\\FormPermissionSingleBase' => 'LajiStore\\V1\\InputFilter\\FormPermissionSingleBase',
            'LajiStore\\V1\\InputFilter\\NotificationBase' => 'LajiStore\\V1\\InputFilter\\NotificationBase',
            'LajiStore\\V1\\InputFilter\\DeviceBase' => 'LajiStore\\V1\\InputFilter\\DeviceBase',
            'LajiStore\\V1\\InputFilter\\DeviceIndividual' => 'LajiStore\\V1\\InputFilter\\DeviceIndividual',
            'LajiStore\\V1\\InputFilter\\Fact' => 'LajiStore\\V1\\InputFilter\\Fact',
            'LajiStore\\V1\\InputFilter\\Individual' => 'LajiStore\\V1\\InputFilter\\Individual',
            'LajiStore\\V1\\InputFilter\\Form' => 'LajiStore\\V1\\InputFilter\\Form',
            'LajiStore\\V1\\InputFilter\\Profile' => 'LajiStore\\V1\\InputFilter\\Profile',
            'LajiStore\\V1\\InputFilter\\Field' => 'LajiStore\\V1\\InputFilter\\Field',
        ),
    ),
    'filters' => array(
        'invokables' => array(
            'LajiStore\\Filter\\CurrentTime' => 'LajiStore\\Filter\\CurrentTime',
            'LajiStore\\Service\\InputFilterGenerator' => 'LajiStore\\Service\\InputFilterGeneratorFactory',
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'LajiStore\\V1\\Rest\\Document\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\News\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\NamedPlace\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\Annotation\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\FormPermissionSingle\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\Notification\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\Device\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\DeviceIndividual\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\Form\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\Profile\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
            'LajiStore\\V1\\Rest\\Individual\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
                'entity' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => true,
                ),
            ),
        ),
    ),
);
