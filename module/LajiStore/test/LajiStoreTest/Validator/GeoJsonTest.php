<?php

namespace LajiStoreTest\Validator;

use Luomus\InputFilter\Validator\Geometry;

class GeoJsonTest extends  \PHPUnit_Framework_TestCase
{
    /** @var Geometry */
    private $validator;

    public function setUp()
    {
        $this->validator = new Geometry();
    }

    public function testMissingType()
    {
        $data = [
            'coordinates' => [10,10]
        ];
        $this->assertFalse($this->validator->isValid($data));
        $this->assertRegExp('/type/', implode(' ',$this->validator->getMessages()));
    }

    public function testMissingCoordinates()
    {
        $data = [
            'type' => 'Point'
        ];
        $this->assertFalse($this->validator->isValid($data));
        $this->assertRegExp('/coordinates/', implode(' ',$this->validator->getMessages()));
    }

    public function testInvalidKeys()
    {
        $data = [
            'type' => 'Point',
            'coordinates' => [10,10],
            'test' => '',
        ];
        $this->assertFalse($this->validator->isValid($data));
        $this->assertRegExp('/test/', implode(' ',$this->validator->getMessages()));
    }

    public function testTooManyKeys()
    {
        $data = [
            'type' => 'Point',
            'coordinates' => [10,10],
            'nokey',
        ];
        $this->assertFalse($this->validator->isValid($data));
        $this->assertRegExp('/0/', implode(' ',$this->validator->getMessages()));
    }

    public function testThirdCoordinate()
    {
        $data = [
            'type' => 'Point',
            'coordinates' => [30,40, 20],
        ];
        $this->assertTrue($this->validator->isValid($data));
    }

    public function testNegativeCoordinates()
    {
        $data = [
            'type' => 'Point',
            'coordinates' => [-30,-40,-20],
        ];
        $this->assertTrue($this->validator->isValid($data));
    }

    public function testCoordinateVerbatim()
    {
        $data = [
            'type' => 'Point',
            'coordinateVerbatim' => '668:336',
            'coordinates' => [30,30],
        ];
        $this->assertTrue($this->validator->isValid($data));
    }

    public function testPolygon()
    {
        $data = [
            'type' => 'Polygon',
            'coordinates' => [[[10,10], [20,10], [20,20], [10,10]]],
        ];
        $this->assertTrue($this->validator->isValid($data));
    }

    public function testPolygonWithHole()
    {
        $data = [
            'type' => 'Polygon',
            'coordinates' => [
                [[10,10], [20,10], [20,20], [10,10]],
                [[12,12], [18,12], [18,18], [12,12]]
            ],
        ];
        $this->assertTrue($this->validator->isValid($data));
    }

    public function testPolygonWithManyHoles()
    {
        $data = [
            'type' => 'Polygon',
            'coordinates' => [
                [[10,10], [20,10], [20,20], [10,10]],
                [[12,12], [18,12], [18,18], [12,12]],
                [[10,10], [11,10], [11,11], [10,10]]
            ],
        ];
        $this->assertTrue($this->validator->isValid($data));
    }

    public function testPolygonWithInvalidData()
    {
        $data = [
            'type' => 'Polygon',
            'coordinates' => [
                [[10,10], [20,10], [20],    [10,10]],
                [[12,12], [18,12], [18,18], [12,12]]
            ],
        ];
        $this->assertFalse($this->validator->isValid($data));
    }

    public function testInvalidPolygon()
    {
        $data = [
            'type' => 'Polygon',
            'coordinates' => [10,10],
        ];
        $this->assertFalse($this->validator->isValid($data));
    }
}