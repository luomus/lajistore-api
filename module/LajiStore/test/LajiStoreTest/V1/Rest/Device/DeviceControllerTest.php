<?php

namespace LajiStoreTest\V1\Rest\Device;

use Zend\Json\Json;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use ZF\ContentNegotiation\Request;

class DeviceControllerTest extends AbstractHttpControllerTestCase
{

    private $accessToken1;
    private $accessToken2;

    public function setUp()
    {
        $this->setApplicationConfig(
            include __DIR__ . '/../../../../../../../config/application.config.php'
        );
        $this->accessToken1 = 'S0UuMDE3LXRlc3Rpbmc6cGFzc3dvcmQ=';
        $this->accessToken2 = 'S0UuMDI3LXRlc3Rpbmc6cGFzc3dvcmQ=';
        parent::setUp();
    }

    public function testClearingDevices() {
        $this->getPreparedRequest();
        $this->dispatch('/devices', 'DELETE');
        $this->assertResponseStatusCode(204);
    }

    public function testClearingDevicesOnAnotherPerson() {
        $this->getPreparedRequest(true);
        $this->dispatch('/devices', 'DELETE');
        $this->assertResponseStatusCode(204);
    }

    public function testDeviceGuestAccess()
    {
        $request = $this->getRequest();
        $request->setMethod('GET');
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(403);
    }

    public function testDeviceWithInvalidCredentials()
    {
        $request = $this->getRequest();
        $request->setMethod('GET');
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $headers->addHeaderLine('Authorization', 'Basic ' . $this->accessToken1 . 'KJE');
        $this->dispatch('/devices');
        $this->assertResponseStatusCode(401);
    }

    public function testDeviceActionInvalidContentType()
    {
        $request = $this->getRequest();
        $request->setMethod('GET');
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Authorization', 'Basic ' . $this->accessToken1);
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(406);
    }

    public function testDeviceActionWithCorrectUserAuthorization() {
        $this->getPreparedRequest();
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $body = $this->getResponse()->getContent();
        $data = Json::decode($body, Json::TYPE_ARRAY);
        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('member', $data);
        $this->assertArrayHasKey('@context', $data);
        $this->assertArrayHasKey('@type', $data);
        $this->assertArrayHasKey('totalItems', $data);
        $this->assertEquals(0, $data['totalItems']);
        $this->assertEquals('Collection', $data['@type']);
        $this->assertInternalType('int', $data['totalItems']);
    }

    /**
     * @dataProvider invalidCollectionVerbProvider
     */
    public function testDeviceActionInvalidVerb($verb)
    {
        $request = $this->getRequest();
        $request->setMethod($verb);
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Authorization', 'Basic ' . $this->accessToken1);
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(405);
    }

    public function invalidCollectionVerbProvider() {
        return [
            ['PUT'],
            ['PATCH']
        ];
    }

    public function testAddingInvalidDateTimeDevice() {
        $request = $this->getPreparedRequest();
        $request->setMethod('POST');
        $data = $this->getDevice([
            'deviceId' => 'MY.3',
            'createdAt' => '2015-01-01 01:01:00+03:00'
        ], false);
        $request->setContent(json_encode($data));
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(422);
        $result = json_decode($this->getResponse()->getContent(), true);
        $this->assertArrayHasKey('detail', $result);
        $this->assertArrayHasKey('validation_messages', $result);
        $this->assertEquals('Failed Validation',$result['detail']);
        return $result;
    }

    public function testAddingWithInvalidKeyDevice() {
        $request = $this->getPreparedRequest();
        $request->setMethod('POST');
        $data = $this->getDevice([
            'deviceManufacturerID' => 'MY.1',
            'nonExisting' => 'fail'
        ], false);
        $request->setContent(json_encode($data));
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(422);
        $result = json_decode($this->getResponse()->getContent(), true);
        $this->assertArrayHasKey('title', $result);
        $this->assertArrayHasKey('detail', $result);
        $this->assertEquals('Unprocessable Entity',$result['title']);
        $this->assertContains('nonExisting', $result['detail']);
    }

    public function testAddingNewDevice() {
        $request = $this->getPreparedRequest();
        $request->setMethod('POST');
        $data = $this->getDevice([
            'deviceManufacturerID' => 'MY.1',
        ], false);
        $request->setContent(json_encode($data));
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(201);
        $result = $resultData = json_decode($this->getResponse()->getContent(), true);
        $this->assertArrayHasKey('id', $resultData);
        $this->assertArrayHasKey('@context', $resultData);
        unset($resultData['id']);
        unset($resultData['@context']);
        $this->assertEquals($data, $resultData);
        return $result;
    }

    /**
     * @depends testAddingNewDevice
     */
    public function testAddingNewDeviceWithAnotherPerson() {
        $request = $this->getPreparedRequest(true);
        $request->setMethod('POST');
        $data = $this->getDevice([
            'deviceManufacturerID' => 'MY.2',
        ], false);
        $request->setContent(json_encode($data));
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(201);
        $resultData = json_decode($this->getResponse()->getContent(), true);
        $this->assertArrayHasKey('id', $resultData);
        $this->assertArrayHasKey('@context', $resultData);
        unset($resultData['id']);
        unset($resultData['@context']);
        $this->assertEquals($data, $resultData);
    }

    private function getIdFormUrl($url) {
        $pos = strrpos($url, '/');
        if ($pos !== false) {
            $url = substr($url, $pos + 1);
        }
        return $url;
    }

    /**
     * @depends testAddingNewDevice
     * @param array $data of the added device
     */
    public function testGettingHistory($data) {
        $this->getPreparedRequest();
        $id = $this->getIdFormUrl($data['id']);
        $this->dispatch('/devices/' . $id . '?version=');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->assertArrayHasKey('versions', $data);
        $this->assertCount(0, $data['versions']);
    }

    /**
     * @depends testAddingNewDevice
     * @param array $data of the added device
     */
    public function testUpdatingDeviceWithAnotherUser($data) {
        $request = $this->getPreparedRequest(true);
        $request->setMethod('PUT');
        $data['deviceManufacturerID'] = 'MY.9';
        $id = $this->getIdFormUrl($data['id']);
        unset($data['id']);
        unset($data['@context']);
        $request->setContent(json_encode($data));
        $this->dispatch('/devices/' . $id);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(404);
    }

    /**
     * @depends testAddingNewDevice
     * @param array $data of the added device
     */
    public function testTryingToAccessOthersDataEntity($data) {
        $request = $this->getPreparedRequest(true);
        $request->setContent(json_encode($data));
        $this->dispatch('/devices/' . $this->getIdFormUrl($data['id']));
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(404);
    }

    /**
     * @depends testAddingNewDevice
     * @param array $data of the added device
     */
    public function testTryingToAccessOthersDataCollection($data) {
        $this->getPreparedRequest(true);
        $query = 'id:(1 2 ' . $this->getIdFormUrl($data['id']) . ' 4 5)';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->assertArrayHasKey('member', $data);
        $this->assertInternalType('array', $data['member']);
        $this->assertCount(0, $data['member']);
    }

    /**
     * @depends testAddingNewDeviceWithAnotherPerson
     */
    public function testDeletingAllOtherPersonsDevices() {
        $this->getPreparedRequest(true);
        $this->dispatch('/devices', 'DELETE');
        $this->assertResponseStatusCode(204);
    }

    /**
     * @depends testDeletingAllOtherPersonsDevices
     */
    public function testAccessingEmptyDataCollectionWithAnotherUser() {
        $this->getPreparedRequest(true);
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 0);
    }

    private function collectionHasCorrectNumberOfMembers($data, $count) {
        $this->assertArrayHasKey('totalItems', $data);
        $this->assertEquals($count, $data['totalItems']);
        $this->assertArrayHasKey('member', $data);
        $this->assertInternalType('array', $data['member']);
        $this->assertCount($count, $data['member']);
    }

    /**
     * @depends testAddingNewDevice
     * @param array $data of the added device
     *
     * @return array
     */
    public function testAccessingDataCollection($data) {
        $this->getPreparedRequest();
        $query = 'id:(1 2 ' . $this->getIdFormUrl($data['id']) . ' 4 5)';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 1);
    }

    /**
     * @depends testAddingNewDevice
     * @param array $data of the added device
     *
     * @return array
     */
    public function testAccessingDataCollectionNotValueList($data) {
        $this->getPreparedRequest();
        $query = 'NOT id:(1 2 ' . $this->getIdFormUrl($data['id']) . ' 4 5)';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 0);
    }

    /**
     * @depends testAddingNewDevice
     * @param array $data of the added device
     *
     * @return array
     */
    public function testAccessingDataCollectionRange($data) {
        $this->markTestSkipped(
            'ID cannot be used for range query anymore.'
        );
        $this->getPreparedRequest();
        $id = $this->getIdFormUrl($data['id']);
        $query = 'id:[' . $id . ' TO ' . ($id + 4) . ']';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 1);

        $this->reset();
        $this->getPreparedRequest();
        $query = 'id:{' . $id . ' TO ' . ($id + 4) . ']';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 0);

        $this->reset();
        $this->getPreparedRequest();
        $query = 'id:[' . $id . ' TO *]';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 1);

        $this->reset();
        $this->getPreparedRequest();
        $query = 'id:[* TO ' . $id . '}';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 0);
    }

    /**
     * @depends testAddingNewDevice
     * @param array $data of the added device
     *
     * @return array
     */
    public function testAccessingDataCollectionRangeWithNot($data) {
        $this->markTestSkipped(
            'ID cannot be used for range query anymore.'
        );
        $this->getPreparedRequest();
        $id = $this->getIdFormUrl($data['id']);
        $query = 'NOT id:[' . $id . ' TO ' . ($id + 4) . ']';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 0);

        $this->reset();
        $this->getPreparedRequest();
        $query = 'NOT id:{' . $id . ' TO ' . ($id + 4) . ']';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 1);

        $this->reset();
        $this->getPreparedRequest();
        $query = 'NOT id:[' . $id . ' TO *]';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 0);

        $this->reset();
        $this->getPreparedRequest();
        $query = 'NOT id:[* TO ' . $id . '}';
        $this->dispatch('/devices?q=' . $query);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data, 1);
    }



    /**
     * @depends testAddingNewDevice
     * @param array $data of the added device
     *
     * @return array
     */
    public function testAccessingDataEntity($data) {
        $request = $this->getPreparedRequest();
        $request->setContent(json_encode($data));
        $this->dispatch('/devices/' . $this->getIdFormUrl($data['id']));
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $result = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->assertEquals($data, $result);
        return $data;
    }

    /**
     * @depends testAccessingDataEntity
     * @param array $data of the added device
     *
     * @return array
     */
    public function testUpdatingDevice($data) {
        $request = $this->getPreparedRequest();
        $request->setMethod('PUT');
        $data['deviceManufacturerID'] = 'MY.2';
        $id = $this->getIdFormUrl($data['id']);
        $expectedResult = $data;
        unset($data['id']);
        unset($data['@context']);
        $request->setContent(json_encode($data));
        $this->dispatch('/devices/' . $id);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $result = json_decode($this->getResponse()->getContent(), true);
        $this->assertEquals($expectedResult, $result);
        return $result;
    }

    /**
     * @depends testUpdatingDevice
     * @param array $data of the added device
     */
    public function testGettingUpdatedItemFromHistory($data) {
        $this->getPreparedRequest();
        $id = $this->getIdFormUrl($data['id']);
        $this->dispatch('/devices/' . $id . '?version=');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->assertArrayHasKey('versions', $data);
        $this->assertCount(1, $data['versions']);

        $this->reset();
        $this->getPreparedRequest();
        $this->dispatch('/devices/' . $id . '?version=1&diffTo=');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->assertCount(1, $data);
        $patch = array_pop($data);
        $this->assertArrayHasKey('op', $patch);
        $this->assertArrayHasKey('path', $patch);
        $this->assertArrayHasKey('value', $patch);

        $this->reset();
        $this->getPreparedRequest();
        $this->dispatch('/devices/' . $id . '?diffTo=1');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->assertCount(1, $data);
        $patch = array_pop($data);
        $this->assertArrayHasKey('op', $patch);
        $this->assertArrayHasKey('path', $patch);
        $this->assertArrayHasKey('value', $patch);
    }

    /**
     * @depends testUpdatingDevice
     * @param $data
     * @return mixed
     */
    public function testQueryingWithTwoParams($data)
    {
        $request = $this->getPreparedRequest();
        $request->setMethod('POST');
        $data2 = $this->getDevice([
            'deviceManufacturerID' => $data['deviceManufacturerID'],
        ], false);
        $request->setContent(json_encode($data2));
        $this->dispatch('/devices');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(201);

        $this->reset();
        $query = 'id:' . $this->getIdFormUrl($data['id']) . ' AND deviceManufacturerID:' . $data['deviceManufacturerID'];
        $this->getPreparedRequest();
        $this->dispatch('/devices?q=' . $query);
        $data3 = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->collectionHasCorrectNumberOfMembers($data3, 1);

    }

    /**
     * @depends testUpdatingDevice
     * @param $data
     * @return mixed
     */
    public function testDeletingDeviceWithAnotherUser($data) {
        $request = $this->getPreparedRequest(true);
        $request->setMethod('DELETE');
        $this->dispatch('/devices/' . $this->getIdFormUrl($data['id']));
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(422);
        return $data;
    }

    /**
     * @depends testDeletingDeviceWithAnotherUser
     * @param $data
     * @return mixed
     */
    public function testDeletingDevice($data) {
        $request = $this->getPreparedRequest();
        $request->setMethod('DELETE');
        $this->dispatch('/devices/' . $this->getIdFormUrl($data['id']));
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(204);
        $this->assertEmpty($this->getResponse()->getContent());
        return $data;
    }

    /**
     * @depends testDeletingDevice
     * @param array $data of the added device
     */
    public function testGettingDeletedItemFromHistory($data) {
        $this->getPreparedRequest();
        $id = $this->getIdFormUrl($data['id']);
        $this->dispatch('/devices/' . $id . '?version=');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $versions = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->assertArrayHasKey('versions', $versions);
        $this->assertCount(2, $versions['versions']);

        $this->reset();
        $this->getPreparedRequest();
        $this->dispatch('/devices/' . $id . '?version=2');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $result = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
        $this->assertEquals($data, $result);

        $this->reset();
        $this->getPreparedRequest();
        $this->dispatch('/devices/' . $id . '?version=3');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(404);
    }

    /**
     * @depends testDeletingDevice
     * @param $data
     */
    public function testGettingDeletedDevice($data) {
        $request = $this->getPreparedRequest();
        $request->setMethod('GET');
        $this->dispatch('/devices/' . $this->getIdFormUrl($data['id']));
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(404);
    }

    private function getPreparedRequest($useSecondPersonAuth = false) {
        /** @var Request $request */
        $request = $this->getRequest();
        $request->setMethod('GET');
        $headers = $request->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $headers->addHeaderLine('Content-Type', 'application/json');
        if ($useSecondPersonAuth) {
            $headers->addHeaderLine('Authorization', 'Basic ' . $this->accessToken2);
        } else {
            $headers->addHeaderLine('Authorization', 'Basic ' . $this->accessToken1);
        }

        return $request;
    }

    private function getDevice($base, $asJson = true) {
        $now = new \DateTime();
        $data = [
            'deviceManufacturerID' => '',
            'deviceType' => 'GMS',
            'deviceManufacturer' => 'Exotone',
            'dateCreated' => $now->format('c'),
            'creator' => 'MA.97',
            'dateEdited' => $now->format('c'),
            'editor' => 'MA.97',
            '@type' => 'MXC.device'
        ];
        $data = ArrayUtils::merge($data, $base);
        if ($asJson) {
            $data = json_encode($data);
        }
        return $data;
    }
}