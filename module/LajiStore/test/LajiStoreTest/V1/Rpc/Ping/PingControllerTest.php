<?php

namespace LajiStoreTest\V1\Rpc\Ping;

use Zend\Json\Json;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class PingControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        $this->setApplicationConfig(
            include __DIR__ . '/../../../../../../../config/application.config.php'
        );
        parent::setUp();
    }
    public function testPingActionInvalidContentType()
    {
        $request = $this->getRequest();
        $request->setMethod('GET');
        $this->dispatch('/ping');
        $this->assertModuleName('LajiStore');
        $this->assertControllerClass('PingController');
        $this->assertResponseStatusCode(406);
    }
    public function testPingActionOk()
    {
        $request = $this->getRequest();
        $request->setMethod('GET');
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $this->dispatch('/ping');
        $this->assertModuleName('LajiStore');
        $this->assertControllerClass('PingController');
        $this->assertResponseStatusCode(200);
        $body = $this->getResponse()->getContent();
        $data = Json::decode($body, Json::TYPE_ARRAY);
        $this->assertArrayHasKey('ack', $data);
        $this->assertInternalType('int', $data['ack']);
    }
}