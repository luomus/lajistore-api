<?php

namespace LajiStoreTest\V1\Rest;


use Zend\Http\Request;
use Zend\Json\Json;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class GenericRestServiceTest extends AbstractHttpControllerTestCase
{
    const SERVER = 'https://data.local/';
    private $accessToken1;
    private $accessToken2;

    private $service = [
        'devices',
        'individuals',
        'documents',
        'news',
        'deviceIndividuals',
        'namedPlaces',
        'annotations'
    ];

    public static function setUpBeforeClass() {
        $_SERVER['SERVER_NAME'] = 'data.local';
        $_SERVER['SERVER_PORT'] = '80';
        $_SERVER['HTTP_X_FORWARDED_PROTO'] = 'https';
    }

    public function setUp()
    {
        $this->application = null;
        $this->setApplicationConfig(
            include __DIR__ . '/../../../../../config/application.config.php'
        );
        $this->accessToken1 = 'S0UuMy10ZXN0aW5nOnBhc3N3b3Jk';
        $this->accessToken2 = 'S0UuMC10ZXN0aW5nOnBhc3N3b3Jk';
        parent::setUp();
    }

    public function serviceProvider() {
        $result = [];
        foreach($this->service as $service) {
            $result[] = [$service];
        }
        return $result;
    }

    public function testClearingServices() {
        $this->getPreparedRequest();
        foreach($this->service as $service) {
            $this->dispatch(self::SERVER . $service, 'DELETE');
            $this->assertResponseStatusCode(204);
        }
    }

    public function testClearingServicesOnAnotherPerson() {
        $this->getPreparedRequest(true);
        foreach($this->service as $service) {
            $this->dispatch(self::SERVER . $service, 'DELETE');
            $this->assertResponseStatusCode(204);
        }
    }

    /**
     * @dataProvider serviceProvider
     * @param $service
     */
    public function testCollectionGuestAccess($service)
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $request->setMethod('GET');
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $this->dispatch(self::SERVER . $service);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(403);

        $request->setMethod('POST');
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $this->dispatch(self::SERVER . $service);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(403);

        $request->setMethod('GET');
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $this->dispatch(self::SERVER . $service . '/1');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(403);

        $request->setMethod('PUT');
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $this->dispatch(self::SERVER . $service . '/1');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(403);

        $request->setMethod('DELETE');
        $headers = $this->getRequest()->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $this->dispatch(self::SERVER . $service . '/1');
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(403);
    }

    /**
     * @dataProvider serviceProvider
     * @param $service
     */
    public function testCollectionWithCorrectUserAuthorization($service) {
        $this->getPreparedRequest();
        $this->dispatch(self::SERVER . $service);
        $this->assertControllerClass('RestController');
        $this->assertResponseStatusCode(200);
        $body = $this->getResponse()->getContent();
        $data = Json::decode($body, Json::TYPE_ARRAY);
        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('@context', $data);
        $this->assertArrayHasKey('@type', $data);
        $this->assertEquals('Collection', $data['@type']);
        $this->collectionHasCorrectNumberOfMembers($data, 0);
    }

    /**
     * @return array
     */
    public function testAddingNewEntry() {
        $results = [];
        $now = new \DateTime();
        $inserts = [
            'devices' => [
                [
                    'deviceManufacturerID' => 'ManuID',
                    'deviceType' => 'GMS',
                    'deviceManufacturer' => 'Exotone',
                    'dateCreated' => $now->format('c'),
                    'creator' => 'MA.97',
                    'dateEdited' => $now->format('c'),
                    'editor' => 'MA.97',
                ]
            ],
            'individuals' => [
                [
                    'deleted'=> false,
                    'ringID' => 'Ring ID',
                    'intellectualRights' => 'MZ.intellectualRightsCC-BY-SA-4.0',
                    'description' => [
                        'en' => 'Description EN',
                        'fi' => 'Kuvaus fi',
                    ]
                ]
            ],
            'documents' => [
                [
                    'inMustikka' => true,
                    'publicityRestrictions' => 'MZ.publicityRestrictionsPublic',
                    'dateCreated' => '2016-01-01T00:00:00+00:00',
                    'collectionID' => 'HR.128',
                    'gatherings' => [
                        [
                            'country' => 'FI',
                            'higherGeography' => 'Earth',
                            'areaDetail' => 'test',
                            'units' => [
                                [
                                    'individualCount' => 50,
                                    'recordBasis' => 'MY.recordBasisLivingSpecimen',
                                    '@type' => 'MY.unit'
                                ]
                            ],
                            '@type' => 'MY.gathering'
                        ]
                    ]
                ]
            ],
            'news' => [
                [
                    'publishDate' => $now->format('c'),
                    'eventDate' => $now->format('c'),
                    'language' => 'en',
                    'title' => 'Title',
                    'content' => 'Content',
                    'targets' => [
                        'http://id.luomus.fi/JA.1'
                    ],
                    'targetIndividualIds' => [
                        'JA.1'
                    ]
                ]
            ],
            'deviceIndividuals' => [
                [
                    'attached' => $now->format('c'),
                ]
            ],
            'namedPlaces' => [
                [
                    'name' => 'foobar',
                    'geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            26.539537815439,
                            66.510561078836
                        ]
                    ]
                ]
            ],
            'annotations' => [
                [
                    'notes' => 'foobar'
                ]
            ]
        ];
        foreach($this->service as $service) {
            $request = $this->getPreparedRequest();
            $request->setMethod('POST');
            if (!isset($inserts[$service]) || !is_array($inserts[$service])) {
                continue;
            }
            $saveService = str_replace('/', '', $service);
            foreach($inserts[$service] as $key => $data) {
                $method = 'prepare_'. $saveService;
                if (method_exists($this, $method)) {
                    $data = $this->$method($data, $results);
                }
                $request->setContent(json_encode($data));
                $this->dispatch(self::SERVER . $service);
                $this->assertControllerClass('RestController');
                $this->assertResponseStatusCode(201);
                $fullResult = $result = json_decode($this->getResponse()->getContent(), true);
                $this->assertArrayHasKey('id', $result);
                $this->assertArrayHasKey('@context', $result);
                $this->removeIDKeys($result);
                $resultCompare = array_intersect_key($result, $data);
                $this->assertEquals($data, $resultCompare);
                $results[$service][$key] = $fullResult;
            }
        }
        return $results;
    }

    private function removeIDKeys(&$data) {
        foreach($data as $key => &$value) {
            if ($key === 'id') {
                unset($data[$key]);
            } elseif (is_array($value)) {
                $this->removeIDKeys($value);
            }
        }
    }

    private function prepare_deviceIndividuals($data, $allData) {
        $data['deviceID'] = $allData['devices'][0]['id'];
        $data['individualID'] = $allData['individuals'][0]['id'];

        return $data;
    }

    private function getIdFormUrl($url) {
        $pos = strrpos($url, '/');
        if ($pos !== false) {
            $url = substr($url, $pos + 1);
        }
        return $url;
    }

    private function collectionHasCorrectNumberOfMembers($data, $count) {
        $this->assertArrayHasKey('totalItems', $data);
        $this->assertInternalType('int', $data['totalItems']);
        $this->assertEquals($count, $data['totalItems']);
        $this->assertArrayHasKey('member', $data);
        $this->assertInternalType('array', $data['member']);
        $this->assertCount($count, $data['member']);
    }

    /**
     * @depends testAddingNewEntry
     * @param array $results of data [service => entry]
     *
     * @return array
     */
    public function testAccessingDataCollection($results) {
        foreach($results as $service => $collection) {
            foreach($collection as $data) {
                $this->getPreparedRequest();
                $query = 'id:(1 2 ' . $this->getIdFormUrl($data['id']) . ' 4 5)';
                $this->dispatch(self::SERVER . $service . '?q=' . $query);
                $this->assertControllerClass('RestController');
                $this->assertResponseStatusCode(200);
                $data = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
                $this->collectionHasCorrectNumberOfMembers($data, 1);
            }
        }
    }

    /**
     * @depends testAddingNewEntry
     * @param array $results of the added entities
     *
     * @return array
     */
    public function testAccessingDataEntity($results) {
        foreach($results as $service => $collection) {
            foreach($collection as $data) {
                $request = $this->getPreparedRequest();
                $request->setContent(json_encode($data));
                $this->dispatch(self::SERVER . $service . '/' . $this->getIdFormUrl($data['id']));
                $this->assertControllerClass('RestController');
                $this->assertResponseStatusCode(200);
                $result = Json::decode($this->getResponse()->getContent(), Json::TYPE_ARRAY);
                $this->assertEquals($data, $result);
            }
        }
        return $results;
    }

    /**
     * @depends testAccessingDataEntity
     * @param array $results of the added entities
     *
     * @return array
     */
    public function testUpdatingDevice($results) {
        $update = [
            'devices' => [
                [
                    'deviceManufacturerID' => 'NewID',
                    'editor' => 'MA.1',
                ]
            ],
            'individuals' => [
                [
                    'deleted'=> true,
                ]
            ],
            'documents' => [
                [
                    'inMustikka' => false,
                    'publicityRestrictions' => 'MZ.publicityRestrictionsPrivate',
                    'gatherings' => [
                        [
                            'units' => [
                                [
                                    'individualCount' => 0,
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'news' => [
                [
                    'content' => 'New contend',
                ]
            ],
            'deviceIndividuals' => [
                [
                    'attached' => '2015-01-01T09:33:30+0300',
                ]
            ],
            'namedPlaces' => [
                [
                    'name' => 'New name',
                    'geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            26.539537815439,
                            66.510561078836
                        ]
                    ]
                ]
            ],
            'annotations' => [
                [
                    'notes' => 'New notes'
                ]
            ]
        ];
        foreach($results as $service => $collection) {
            foreach($collection as $key => $data) {
                if (!isset($update[$service]) || !isset($update[$service][$key])) {
                    continue;
                }
                $request = $this->getPreparedRequest();
                $request->setMethod('PUT');
                $id = $this->getIdFormUrl($data['id']);
                $data = array_replace_recursive($data, $update[$service][$key]);
                $expectedResult = $data;
                unset($data['id']);
                unset($data['@context']);
                $request->setContent(json_encode($data));
                $this->dispatch(self::SERVER . $service . '/' . $id);
                $this->assertControllerClass('RestController');
                $this->assertResponseStatusCode(200);
                $result = json_decode($this->getResponse()->getContent(), true);
                $this->assertEquals($expectedResult, $result);
            }
        }
        return $results;
    }

    /**
     * @depends testUpdatingDevice
     * @param $results
     * @return mixed
     */
    public function testDeletingDevice($results) {
        foreach($results as $service => $collection) {
            foreach($collection as $data) {
                $request = $this->getPreparedRequest();
                $request->setMethod('DELETE');
                $this->dispatch(self::SERVER . $service . '/' . $this->getIdFormUrl($data['id']));
                $this->assertControllerClass('RestController');
                $this->assertResponseStatusCode(204);
                $this->assertEmpty($this->getResponse()->getContent());
            }
        }
    }

    /**
     * @depends testDeletingDevice
     */
    public function testItemsHaveBeenDeleted() {
        foreach($this->service as $service) {
            $this->getPreparedRequest();
            $this->dispatch(self::SERVER . $service);
            $this->assertControllerClass('RestController');
            $this->assertResponseStatusCode(200);
            $body = $this->getResponse()->getContent();
            $data = Json::decode($body, Json::TYPE_ARRAY);
            $this->assertArrayHasKey('id', $data);
            $this->assertArrayHasKey('@context', $data);
            $this->assertArrayHasKey('@type', $data);
            $this->assertEquals('Collection', $data['@type']);
            $this->collectionHasCorrectNumberOfMembers($data, 0);
        }
    }

    private function getPreparedRequest($useSecondPersonAuth = false) {
        /** @var Request $request */
        $request = $this->getRequest();
        $request->setUri('https://data.local/');
        $request->setMethod('GET');
        $headers = $request->getHeaders();
        $headers->addHeaderLine('Accept', 'application/json');
        $headers->addHeaderLine('Content-Type', 'application/json');
        if ($useSecondPersonAuth) {
            $headers->addHeaderLine('Authorization', 'Basic ' . $this->accessToken2);
        } else {
            $headers->addHeaderLine('Authorization', 'Basic ' . $this->accessToken1);
        }

        return $request;
    }
}