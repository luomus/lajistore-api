<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        if (class_exists('\ZF\Apigility\Admin\Module', false)) {
          return $this->redirect()->toRoute('zf-apigility/ui');
        }
        return $this->redirect()->toRoute('zf-apigility/swagger');
    }
}
