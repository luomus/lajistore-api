<?php

return [
    // Provides application-wide services.
    // We recommend using fully-qualified class names whenever possible as
    // service names.
    'service_manager' => [
        // Use 'invokables' for constructor-less services, or services that do
        // not require arguments to the constructor. Map a service name to the
        // class name.
        'aliases' => [
            'Triplestore\Service' => 'Triplestore\Oracle',
        ],
        // Use 'factories' for services provided by callbacks/factory classes.
        'factories' => [
            'Triplestore\Oracle' => 'Triplestore\Factory\OracleFactory',
            'Triplestore\Options\ModuleOptions' => 'Triplestore\Factory\ModuleOptionsFactory',
            'Triplestore\Logger' => 'Triplestore\Factory\LoggerFactory',
            'Triplestore\Service\GeneratorService' => 'Triplestore\Factory\GeneratorFactory',
            'Triplestore\ObjectManager' => 'Triplestore\Factory\ObjectManagerFactory',
            'Triplestore\Db\Adapter\Array' => 'Triplestore\Db\Adapter\Factory\Oci8ArrayAdapterFactory',
            'Triplestore\Db\Adapter\Model' => 'Triplestore\Db\Adapter\Factory\Oci8ModelAdapterFactory',
        ],
    ],
    'triplestore' => [
        'database' => [
            'host' => getenv('TRIPLESTORE_HOST'),
            'database' => getenv('TRIPLESTORE_DB'),
            'username' => getenv('TRIPLESTORE_USERNAME'),
            'password' => getenv('TRIPLESTORE_PASSWORD'),
            'charset' => 'AL32UTF8'
        ],
        'classNS' => '',
        'repositoryNS' => ''

    ],
    'cache' => [
        'adapter' => [
            'name' => 'memory',
            'options' => null
        ],
    ]
];
