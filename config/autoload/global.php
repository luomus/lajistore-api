<?php

$esHost = getenv('ELASTIC_HOST');
$esHost = empty($esHost) ? [] : explode(',', $esHost);

return array(
    'host' => 'https://data.laji.fi',
    'db' => [
        'adapters' => [
            'laji-db' => [
                'database' => getenv('LAJISTORE_DB'),
                'username' => getenv('LAJISTORE_USERNAME'),
                'password' => getenv('LAJISTORE_PASSWORD'),
                'hostname' => getenv('LAJISTORE_HOST'),
                'driver' => 'Oci8',
                'charset' => 'AL32UTF8',
                'persistent' => true
            ],
            'laji-api' => [
                'database' => getenv('LAJIAPI_DB'),
                'username' => getenv('LAJIAPI_USERNAME'),
                'password' => getenv('LAJIAPI_PASSWORD'),
                'hostname' => getenv('LAJIAPI_HOST'),
                'driver' => 'Oci8',
                'charset' => 'AL32UTF8',
                'persistent' => true,
            ],
        ],
    ],
    'api_laji_fi' => [
        'url' => getenv('LAJIAPI_URL'),
        'token' => getenv('LAJIAPI_TOKEN'),
        'defaultSystem' => getenv('LAJIAPI_DEFAULT_SYSTEM'),
        'send' => explode(',', getenv('LAJIAPI_SEND_SYSTEMS')),
        'endpoints' => [
            'warehousePush' => 'warehouse/push',
            'allForms' => 'forms',
            'annotationsConvert' => 'annotations/convert'
        ],
        'httpOptions' => [
            'adapter' => 'Zend\Http\Client\Adapter\Curl'
        ]
    ],
    'elastic' => [
        'client' => [
            'sniffOnStart' => false,
            'hosts' => $esHost,
            'SSLVerification' => false
        ]
    ],
    'zf-mvc-auth' => [
        'authentication' => [
            'adapters' => [
                'basic' => [
                    'adapter' => 'ZF\\MvcAuth\\Authentication\\HttpAdapter',
                    'options' => [
                        'accept_schemes' => [
                            'basic',
                        ],
                        'realm' => 'lajistore-api',
                        'htpasswd' => getenv('HTPASS_FILE'),
                    ],
                ],
            ],
            'map' => [
                'LajiStore' => 'basic',
                'Documents' => 'basic',
                'Document' => 'basic',
                'LajiStore\\V1' => 'basic',
            ],
        ],
    ],
    'view_manager' => [
        'display_exceptions' => getenv('DISPLAY_EXCEPTIONS') === 'true',
    ],
);
