<?php
use Luomus\InputFilter\Generator;

return [
    'validators' => array(
        'invokables' => array(
            'Integer' => 'Luomus\InputFilter\Validator\Integer',
            'Type' => 'Luomus\InputFilter\Validator\Type',
        )
    ),
    'luomus' => [
        'input_filter' => [
            'namespace' => 'Luomus\V1\GeneratedInputFilter',
            'default_extend' => '\LajiStore\V1\InputFilter\Generic',
            'extend' => [
                //'class name' => 'class to be extended'
            ],
            'style' => Generator::STYLE_SHORT,
            'location' => './data/generated-inputfilter-v1',
            'skipped_properties' => [
                'MY.hasGathering',
                'MY.gathering',
                'MY.hasUnit',
                'MY.unit',
                'MY.hasSubUnit',
                'MY.hasIdentification',
                'MY.identification'
            ]
        ],
    ]
];
